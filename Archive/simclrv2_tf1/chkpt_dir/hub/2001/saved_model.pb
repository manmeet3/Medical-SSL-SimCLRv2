ů
čĆ
:
Add
x"T
y"T
z"T"
Ttype:
2	
A
AddV2
x"T
y"T
z"T"
Ttype:
2	
B
AssignVariableOp
resource
value"dtype"
dtypetype
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype

Conv2D

input"T
filter"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

Ä
FusedBatchNormV3
x"T

scale"U
offset"U	
mean"U
variance"U
y"T

batch_mean"U
batch_variance"U
reserve_space_1"U
reserve_space_2"U
reserve_space_3"U"
Ttype:
2"
Utype:
2"
epsilonfloat%ˇŃ8"-
data_formatstringNHWC:
NHWCNCHW"
is_trainingbool(
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
Ô
MaxPool

input"T
output"T"
Ttype0:
2	"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID":
data_formatstringNHWC:
NHWCNCHWNCHW_VECT_C

Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
=
Mul
x"T
y"T
z"T"
Ttype:
2	
_
Pad

input"T
paddings"	Tpaddings
output"T"	
Ttype"
	Tpaddingstype0:
2	
C
Placeholder
output"dtype"
dtypetype"
shapeshape:

RandomStandardNormal

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
@
ReadVariableOp
resource
value"dtype"
dtypetype
E
Relu
features"T
activations"T"
Ttype:
2	
2
StopGradient

input"T
output"T"	
Ttype

TruncatedNormal

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape
9
VarIsInitializedOp
resource
is_initialized
*1.15.42v1.15.3-68-gdf8c55c8ůĘ

˘
PlaceholderPlaceholder*6
shape-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
dtype0

base_model/Pad/paddingsConst*
_output_shapes

:*9
value0B."                             *
dtype0

base_model/PadPadPlaceholderbase_model/Pad/paddings*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Á
;base_model/conv2d/kernel/Initializer/truncated_normal/shapeConst*+
_class!
loc:@base_model/conv2d/kernel*
dtype0*%
valueB"         @   *
_output_shapes
:
Ź
:base_model/conv2d/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *
valueB
 *    *
dtype0*+
_class!
loc:@base_model/conv2d/kernel
Ž
<base_model/conv2d/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
valueB
 *+Ŕ=*
dtype0*+
_class!
loc:@base_model/conv2d/kernel

Ebase_model/conv2d/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal;base_model/conv2d/kernel/Initializer/truncated_normal/shape*&
_output_shapes
:@*
T0*
dtype0*+
_class!
loc:@base_model/conv2d/kernel
Ł
9base_model/conv2d/kernel/Initializer/truncated_normal/mulMulEbase_model/conv2d/kernel/Initializer/truncated_normal/TruncatedNormal<base_model/conv2d/kernel/Initializer/truncated_normal/stddev*
T0*+
_class!
loc:@base_model/conv2d/kernel*&
_output_shapes
:@

5base_model/conv2d/kernel/Initializer/truncated_normalAdd9base_model/conv2d/kernel/Initializer/truncated_normal/mul:base_model/conv2d/kernel/Initializer/truncated_normal/mean*
T0*+
_class!
loc:@base_model/conv2d/kernel*&
_output_shapes
:@
Á
base_model/conv2d/kernelVarHandleOp*)
shared_namebase_model/conv2d/kernel*
shape:@*
dtype0*
_output_shapes
: *+
_class!
loc:@base_model/conv2d/kernel

9base_model/conv2d/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d/kernel*
_output_shapes
: 

base_model/conv2d/kernel/AssignAssignVariableOpbase_model/conv2d/kernel5base_model/conv2d/kernel/Initializer/truncated_normal*
dtype0

,base_model/conv2d/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d/kernel*&
_output_shapes
:@*
dtype0
p
base_model/conv2d/dilation_rateConst*
valueB"      *
dtype0*
_output_shapes
:

'base_model/conv2d/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d/kernel*
dtype0*&
_output_shapes
:@
Đ
base_model/conv2d/Conv2DConv2Dbase_model/Pad'base_model/conv2d/Conv2D/ReadVariableOp*
paddingVALID*
strides
*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0

base_model/initial_convIdentitybase_model/conv2d/Conv2D*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
ť
5base_model/batch_normalization/gamma/Initializer/onesConst*
dtype0*
valueB@*  ?*7
_class-
+)loc:@base_model/batch_normalization/gamma*
_output_shapes
:@
Ů
$base_model/batch_normalization/gammaVarHandleOp*
shape:@*
_output_shapes
: *7
_class-
+)loc:@base_model/batch_normalization/gamma*
dtype0*5
shared_name&$base_model/batch_normalization/gamma

Ebase_model/batch_normalization/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp$base_model/batch_normalization/gamma*
_output_shapes
: 
Š
+base_model/batch_normalization/gamma/AssignAssignVariableOp$base_model/batch_normalization/gamma5base_model/batch_normalization/gamma/Initializer/ones*
dtype0

8base_model/batch_normalization/gamma/Read/ReadVariableOpReadVariableOp$base_model/batch_normalization/gamma*
dtype0*
_output_shapes
:@
ş
5base_model/batch_normalization/beta/Initializer/zerosConst*
valueB@*    *
dtype0*6
_class,
*(loc:@base_model/batch_normalization/beta*
_output_shapes
:@
Ö
#base_model/batch_normalization/betaVarHandleOp*
shape:@*
dtype0*
_output_shapes
: *4
shared_name%#base_model/batch_normalization/beta*6
_class,
*(loc:@base_model/batch_normalization/beta

Dbase_model/batch_normalization/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp#base_model/batch_normalization/beta*
_output_shapes
: 
§
*base_model/batch_normalization/beta/AssignAssignVariableOp#base_model/batch_normalization/beta5base_model/batch_normalization/beta/Initializer/zeros*
dtype0

7base_model/batch_normalization/beta/Read/ReadVariableOpReadVariableOp#base_model/batch_normalization/beta*
dtype0*
_output_shapes
:@
Č
<base_model/batch_normalization/moving_mean/Initializer/zerosConst*
valueB@*    *
_output_shapes
:@*=
_class3
1/loc:@base_model/batch_normalization/moving_mean*
dtype0
ë
*base_model/batch_normalization/moving_meanVarHandleOp*;
shared_name,*base_model/batch_normalization/moving_mean*
_output_shapes
: *
dtype0*
shape:@*=
_class3
1/loc:@base_model/batch_normalization/moving_mean
Ľ
Kbase_model/batch_normalization/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp*base_model/batch_normalization/moving_mean*
_output_shapes
: 
ź
1base_model/batch_normalization/moving_mean/AssignAssignVariableOp*base_model/batch_normalization/moving_mean<base_model/batch_normalization/moving_mean/Initializer/zeros*
dtype0
Ľ
>base_model/batch_normalization/moving_mean/Read/ReadVariableOpReadVariableOp*base_model/batch_normalization/moving_mean*
dtype0*
_output_shapes
:@
Ď
?base_model/batch_normalization/moving_variance/Initializer/onesConst*
valueB@*  ?*
_output_shapes
:@*A
_class7
53loc:@base_model/batch_normalization/moving_variance*
dtype0
÷
.base_model/batch_normalization/moving_varianceVarHandleOp*
_output_shapes
: *A
_class7
53loc:@base_model/batch_normalization/moving_variance*
shape:@*?
shared_name0.base_model/batch_normalization/moving_variance*
dtype0
­
Obase_model/batch_normalization/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp.base_model/batch_normalization/moving_variance*
_output_shapes
: 
Ç
5base_model/batch_normalization/moving_variance/AssignAssignVariableOp.base_model/batch_normalization/moving_variance?base_model/batch_normalization/moving_variance/Initializer/ones*
dtype0
­
Bbase_model/batch_normalization/moving_variance/Read/ReadVariableOpReadVariableOp.base_model/batch_normalization/moving_variance*
dtype0*
_output_shapes
:@

-base_model/batch_normalization/ReadVariableOpReadVariableOp$base_model/batch_normalization/gamma*
dtype0*
_output_shapes
:@

/base_model/batch_normalization/ReadVariableOp_1ReadVariableOp#base_model/batch_normalization/beta*
_output_shapes
:@*
dtype0
Ľ
>base_model/batch_normalization/FusedBatchNormV3/ReadVariableOpReadVariableOp*base_model/batch_normalization/moving_mean*
_output_shapes
:@*
dtype0
Ť
@base_model/batch_normalization/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp.base_model/batch_normalization/moving_variance*
_output_shapes
:@*
dtype0
Ô
/base_model/batch_normalization/FusedBatchNormV3FusedBatchNormV3base_model/initial_conv-base_model/batch_normalization/ReadVariableOp/base_model/batch_normalization/ReadVariableOp_1>base_model/batch_normalization/FusedBatchNormV3/ReadVariableOp@base_model/batch_normalization/FusedBatchNormV3/ReadVariableOp_1*
epsilon%đ'7*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:*
T0*
is_training( *
U0
i
$base_model/batch_normalization/ConstConst*
valueB
 *fff?*
dtype0*
_output_shapes
: 

base_model/ReluRelu/base_model/batch_normalization/FusedBatchNormV3*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
ş
 base_model/max_pooling2d/MaxPoolMaxPoolbase_model/Relu*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
strides
*
paddingSAME*
ksize


base_model/initial_max_poolIdentity base_model/max_pooling2d/MaxPool*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
Ĺ
=base_model/conv2d_1/kernel/Initializer/truncated_normal/shapeConst*
dtype0*-
_class#
!loc:@base_model/conv2d_1/kernel*
_output_shapes
:*%
valueB"      @   @   
°
<base_model/conv2d_1/kernel/Initializer/truncated_normal/meanConst*-
_class#
!loc:@base_model/conv2d_1/kernel*
dtype0*
valueB
 *    *
_output_shapes
: 
˛
>base_model/conv2d_1/kernel/Initializer/truncated_normal/stddevConst*
valueB
 *6>*-
_class#
!loc:@base_model/conv2d_1/kernel*
dtype0*
_output_shapes
: 

Gbase_model/conv2d_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_1/kernel/Initializer/truncated_normal/shape*
T0*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_1/kernel*
dtype0
Ť
;base_model/conv2d_1/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_1/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_1/kernel/Initializer/truncated_normal/stddev*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_1/kernel*
T0

7base_model/conv2d_1/kernel/Initializer/truncated_normalAdd;base_model/conv2d_1/kernel/Initializer/truncated_normal/mul<base_model/conv2d_1/kernel/Initializer/truncated_normal/mean*
T0*-
_class#
!loc:@base_model/conv2d_1/kernel*&
_output_shapes
:@@
Ç
base_model/conv2d_1/kernelVarHandleOp*-
_class#
!loc:@base_model/conv2d_1/kernel*
_output_shapes
: *
shape:@@*
dtype0*+
shared_namebase_model/conv2d_1/kernel

;base_model/conv2d_1/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_1/kernel*
_output_shapes
: 

!base_model/conv2d_1/kernel/AssignAssignVariableOpbase_model/conv2d_1/kernel7base_model/conv2d_1/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_1/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_1/kernel*&
_output_shapes
:@@*
dtype0
r
!base_model/conv2d_1/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB"      

)base_model/conv2d_1/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_1/kernel*&
_output_shapes
:@@*
dtype0
ŕ
base_model/conv2d_1/Conv2DConv2Dbase_model/initial_max_pool)base_model/conv2d_1/Conv2D/ReadVariableOp*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
paddingSAME*
T0*
strides

ż
7base_model/batch_normalization_1/gamma/Initializer/onesConst*
valueB@*  ?*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_1/gamma*
_output_shapes
:@
ß
&base_model/batch_normalization_1/gammaVarHandleOp*
dtype0*7
shared_name(&base_model/batch_normalization_1/gamma*9
_class/
-+loc:@base_model/batch_normalization_1/gamma*
shape:@*
_output_shapes
: 

Gbase_model/batch_normalization_1/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_1/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_1/gamma/AssignAssignVariableOp&base_model/batch_normalization_1/gamma7base_model/batch_normalization_1/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_1/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_1/gamma*
_output_shapes
:@*
dtype0
ž
7base_model/batch_normalization_1/beta/Initializer/zerosConst*
_output_shapes
:@*8
_class.
,*loc:@base_model/batch_normalization_1/beta*
valueB@*    *
dtype0
Ü
%base_model/batch_normalization_1/betaVarHandleOp*8
_class.
,*loc:@base_model/batch_normalization_1/beta*
shape:@*
dtype0*
_output_shapes
: *6
shared_name'%base_model/batch_normalization_1/beta

Fbase_model/batch_normalization_1/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_1/beta*
_output_shapes
: 
­
,base_model/batch_normalization_1/beta/AssignAssignVariableOp%base_model/batch_normalization_1/beta7base_model/batch_normalization_1/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_1/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_1/beta*
_output_shapes
:@*
dtype0
Ě
>base_model/batch_normalization_1/moving_mean/Initializer/zerosConst*?
_class5
31loc:@base_model/batch_normalization_1/moving_mean*
dtype0*
valueB@*    *
_output_shapes
:@
ń
,base_model/batch_normalization_1/moving_meanVarHandleOp*
dtype0*
shape:@*=
shared_name.,base_model/batch_normalization_1/moving_mean*?
_class5
31loc:@base_model/batch_normalization_1/moving_mean*
_output_shapes
: 
Š
Mbase_model/batch_normalization_1/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_1/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_1/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_1/moving_mean>base_model/batch_normalization_1/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_1/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_1/moving_mean*
dtype0*
_output_shapes
:@
Ó
Abase_model/batch_normalization_1/moving_variance/Initializer/onesConst*C
_class9
75loc:@base_model/batch_normalization_1/moving_variance*
_output_shapes
:@*
dtype0*
valueB@*  ?
ý
0base_model/batch_normalization_1/moving_varianceVarHandleOp*
dtype0*
shape:@*
_output_shapes
: *C
_class9
75loc:@base_model/batch_normalization_1/moving_variance*A
shared_name20base_model/batch_normalization_1/moving_variance
ą
Qbase_model/batch_normalization_1/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_1/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_1/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_1/moving_varianceAbase_model/batch_normalization_1/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_1/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_1/moving_variance*
dtype0*
_output_shapes
:@

/base_model/batch_normalization_1/ReadVariableOpReadVariableOp&base_model/batch_normalization_1/gamma*
dtype0*
_output_shapes
:@

1base_model/batch_normalization_1/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_1/beta*
dtype0*
_output_shapes
:@
Š
@base_model/batch_normalization_1/FusedBatchNormV3/ReadVariableOpReadVariableOp,base_model/batch_normalization_1/moving_mean*
dtype0*
_output_shapes
:@
Ż
Bbase_model/batch_normalization_1/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_1/moving_variance*
dtype0*
_output_shapes
:@
á
1base_model/batch_normalization_1/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_1/Conv2D/base_model/batch_normalization_1/ReadVariableOp1base_model/batch_normalization_1/ReadVariableOp_1@base_model/batch_normalization_1/FusedBatchNormV3/ReadVariableOpBbase_model/batch_normalization_1/FusedBatchNormV3/ReadVariableOp_1*
epsilon%đ'7*
is_training( *
U0*
T0*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:
k
&base_model/batch_normalization_1/ConstConst*
_output_shapes
: *
valueB
 *fff?*
dtype0
Ĺ
=base_model/conv2d_2/kernel/Initializer/truncated_normal/shapeConst*-
_class#
!loc:@base_model/conv2d_2/kernel*
_output_shapes
:*%
valueB"      @   @   *
dtype0
°
<base_model/conv2d_2/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *-
_class#
!loc:@base_model/conv2d_2/kernel*
dtype0*
_output_shapes
: 
˛
>base_model/conv2d_2/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *B=*-
_class#
!loc:@base_model/conv2d_2/kernel

Gbase_model/conv2d_2/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_2/kernel/Initializer/truncated_normal/shape*
T0*
dtype0*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_2/kernel
Ť
;base_model/conv2d_2/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_2/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_2/kernel/Initializer/truncated_normal/stddev*-
_class#
!loc:@base_model/conv2d_2/kernel*
T0*&
_output_shapes
:@@

7base_model/conv2d_2/kernel/Initializer/truncated_normalAdd;base_model/conv2d_2/kernel/Initializer/truncated_normal/mul<base_model/conv2d_2/kernel/Initializer/truncated_normal/mean*
T0*-
_class#
!loc:@base_model/conv2d_2/kernel*&
_output_shapes
:@@
Ç
base_model/conv2d_2/kernelVarHandleOp*
_output_shapes
: *
shape:@@*
dtype0*+
shared_namebase_model/conv2d_2/kernel*-
_class#
!loc:@base_model/conv2d_2/kernel

;base_model/conv2d_2/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_2/kernel*
_output_shapes
: 

!base_model/conv2d_2/kernel/AssignAssignVariableOpbase_model/conv2d_2/kernel7base_model/conv2d_2/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_2/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_2/kernel*
dtype0*&
_output_shapes
:@@
r
!base_model/conv2d_2/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB"      

)base_model/conv2d_2/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_2/kernel*
dtype0*&
_output_shapes
:@@
ŕ
base_model/conv2d_2/Conv2DConv2Dbase_model/initial_max_pool)base_model/conv2d_2/Conv2D/ReadVariableOp*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
paddingSAME*
strides
*
T0
ż
7base_model/batch_normalization_2/gamma/Initializer/onesConst*
dtype0*
valueB@*  ?*9
_class/
-+loc:@base_model/batch_normalization_2/gamma*
_output_shapes
:@
ß
&base_model/batch_normalization_2/gammaVarHandleOp*
_output_shapes
: *
dtype0*
shape:@*7
shared_name(&base_model/batch_normalization_2/gamma*9
_class/
-+loc:@base_model/batch_normalization_2/gamma

Gbase_model/batch_normalization_2/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_2/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_2/gamma/AssignAssignVariableOp&base_model/batch_normalization_2/gamma7base_model/batch_normalization_2/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_2/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_2/gamma*
_output_shapes
:@*
dtype0
ž
7base_model/batch_normalization_2/beta/Initializer/zerosConst*
dtype0*
valueB@*    *
_output_shapes
:@*8
_class.
,*loc:@base_model/batch_normalization_2/beta
Ü
%base_model/batch_normalization_2/betaVarHandleOp*
dtype0*
shape:@*6
shared_name'%base_model/batch_normalization_2/beta*8
_class.
,*loc:@base_model/batch_normalization_2/beta*
_output_shapes
: 

Fbase_model/batch_normalization_2/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_2/beta*
_output_shapes
: 
­
,base_model/batch_normalization_2/beta/AssignAssignVariableOp%base_model/batch_normalization_2/beta7base_model/batch_normalization_2/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_2/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_2/beta*
_output_shapes
:@*
dtype0
Ě
>base_model/batch_normalization_2/moving_mean/Initializer/zerosConst*
valueB@*    *
dtype0*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean*
_output_shapes
:@
ń
,base_model/batch_normalization_2/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean*
shape:@*=
shared_name.,base_model/batch_normalization_2/moving_mean
Š
Mbase_model/batch_normalization_2/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_2/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_2/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_2/moving_mean>base_model/batch_normalization_2/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_2/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_2/moving_mean*
dtype0*
_output_shapes
:@
Ó
Abase_model/batch_normalization_2/moving_variance/Initializer/onesConst*
_output_shapes
:@*
valueB@*  ?*
dtype0*C
_class9
75loc:@base_model/batch_normalization_2/moving_variance
ý
0base_model/batch_normalization_2/moving_varianceVarHandleOp*A
shared_name20base_model/batch_normalization_2/moving_variance*
shape:@*
dtype0*C
_class9
75loc:@base_model/batch_normalization_2/moving_variance*
_output_shapes
: 
ą
Qbase_model/batch_normalization_2/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_2/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_2/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_2/moving_varianceAbase_model/batch_normalization_2/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_2/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_2/moving_variance*
dtype0*
_output_shapes
:@

/base_model/batch_normalization_2/ReadVariableOpReadVariableOp&base_model/batch_normalization_2/gamma*
dtype0*
_output_shapes
:@

1base_model/batch_normalization_2/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_2/beta*
_output_shapes
:@*
dtype0
Š
@base_model/batch_normalization_2/FusedBatchNormV3/ReadVariableOpReadVariableOp,base_model/batch_normalization_2/moving_mean*
dtype0*
_output_shapes
:@
Ż
Bbase_model/batch_normalization_2/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_2/moving_variance*
_output_shapes
:@*
dtype0
á
1base_model/batch_normalization_2/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_2/Conv2D/base_model/batch_normalization_2/ReadVariableOp1base_model/batch_normalization_2/ReadVariableOp_1@base_model/batch_normalization_2/FusedBatchNormV3/ReadVariableOpBbase_model/batch_normalization_2/FusedBatchNormV3/ReadVariableOp_1*
U0*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:*
epsilon%đ'7*
T0*
is_training( 
k
&base_model/batch_normalization_2/ConstConst*
valueB
 *fff?*
_output_shapes
: *
dtype0

base_model/Relu_1Relu1base_model/batch_normalization_2/FusedBatchNormV3*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
Ĺ
=base_model/conv2d_3/kernel/Initializer/truncated_normal/shapeConst*%
valueB"      @   @   *
dtype0*
_output_shapes
:*-
_class#
!loc:@base_model/conv2d_3/kernel
°
<base_model/conv2d_3/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *
dtype0*-
_class#
!loc:@base_model/conv2d_3/kernel*
valueB
 *    
˛
>base_model/conv2d_3/kernel/Initializer/truncated_normal/stddevConst*
valueB
 *B=*
dtype0*-
_class#
!loc:@base_model/conv2d_3/kernel*
_output_shapes
: 

Gbase_model/conv2d_3/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_3/kernel/Initializer/truncated_normal/shape*
dtype0*&
_output_shapes
:@@*
T0*-
_class#
!loc:@base_model/conv2d_3/kernel
Ť
;base_model/conv2d_3/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_3/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_3/kernel/Initializer/truncated_normal/stddev*&
_output_shapes
:@@*
T0*-
_class#
!loc:@base_model/conv2d_3/kernel

7base_model/conv2d_3/kernel/Initializer/truncated_normalAdd;base_model/conv2d_3/kernel/Initializer/truncated_normal/mul<base_model/conv2d_3/kernel/Initializer/truncated_normal/mean*-
_class#
!loc:@base_model/conv2d_3/kernel*
T0*&
_output_shapes
:@@
Ç
base_model/conv2d_3/kernelVarHandleOp*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_3/kernel*
dtype0*+
shared_namebase_model/conv2d_3/kernel*
shape:@@

;base_model/conv2d_3/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_3/kernel*
_output_shapes
: 

!base_model/conv2d_3/kernel/AssignAssignVariableOpbase_model/conv2d_3/kernel7base_model/conv2d_3/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_3/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_3/kernel*&
_output_shapes
:@@*
dtype0
r
!base_model/conv2d_3/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB"      

)base_model/conv2d_3/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_3/kernel*
dtype0*&
_output_shapes
:@@
Ö
base_model/conv2d_3/Conv2DConv2Dbase_model/Relu_1)base_model/conv2d_3/Conv2D/ReadVariableOp*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
paddingSAME*
strides

Ŕ
8base_model/batch_normalization_3/gamma/Initializer/zerosConst*
valueB@*    *
dtype0*
_output_shapes
:@*9
_class/
-+loc:@base_model/batch_normalization_3/gamma
ß
&base_model/batch_normalization_3/gammaVarHandleOp*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_3/gamma*
_output_shapes
: *
shape:@*7
shared_name(&base_model/batch_normalization_3/gamma

Gbase_model/batch_normalization_3/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_3/gamma*
_output_shapes
: 
°
-base_model/batch_normalization_3/gamma/AssignAssignVariableOp&base_model/batch_normalization_3/gamma8base_model/batch_normalization_3/gamma/Initializer/zeros*
dtype0

:base_model/batch_normalization_3/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_3/gamma*
_output_shapes
:@*
dtype0
ž
7base_model/batch_normalization_3/beta/Initializer/zerosConst*
dtype0*
valueB@*    *
_output_shapes
:@*8
_class.
,*loc:@base_model/batch_normalization_3/beta
Ü
%base_model/batch_normalization_3/betaVarHandleOp*
_output_shapes
: *8
_class.
,*loc:@base_model/batch_normalization_3/beta*6
shared_name'%base_model/batch_normalization_3/beta*
shape:@*
dtype0

Fbase_model/batch_normalization_3/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_3/beta*
_output_shapes
: 
­
,base_model/batch_normalization_3/beta/AssignAssignVariableOp%base_model/batch_normalization_3/beta7base_model/batch_normalization_3/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_3/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_3/beta*
_output_shapes
:@*
dtype0
Ě
>base_model/batch_normalization_3/moving_mean/Initializer/zerosConst*
_output_shapes
:@*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean*
dtype0*
valueB@*    
ń
,base_model/batch_normalization_3/moving_meanVarHandleOp*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean*
_output_shapes
: *=
shared_name.,base_model/batch_normalization_3/moving_mean*
dtype0*
shape:@
Š
Mbase_model/batch_normalization_3/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_3/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_3/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_3/moving_mean>base_model/batch_normalization_3/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_3/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_3/moving_mean*
dtype0*
_output_shapes
:@
Ó
Abase_model/batch_normalization_3/moving_variance/Initializer/onesConst*C
_class9
75loc:@base_model/batch_normalization_3/moving_variance*
dtype0*
_output_shapes
:@*
valueB@*  ?
ý
0base_model/batch_normalization_3/moving_varianceVarHandleOp*A
shared_name20base_model/batch_normalization_3/moving_variance*C
_class9
75loc:@base_model/batch_normalization_3/moving_variance*
_output_shapes
: *
shape:@*
dtype0
ą
Qbase_model/batch_normalization_3/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_3/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_3/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_3/moving_varianceAbase_model/batch_normalization_3/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_3/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_3/moving_variance*
dtype0*
_output_shapes
:@

/base_model/batch_normalization_3/ReadVariableOpReadVariableOp&base_model/batch_normalization_3/gamma*
dtype0*
_output_shapes
:@

1base_model/batch_normalization_3/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_3/beta*
dtype0*
_output_shapes
:@
Š
@base_model/batch_normalization_3/FusedBatchNormV3/ReadVariableOpReadVariableOp,base_model/batch_normalization_3/moving_mean*
_output_shapes
:@*
dtype0
Ż
Bbase_model/batch_normalization_3/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_3/moving_variance*
dtype0*
_output_shapes
:@
á
1base_model/batch_normalization_3/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_3/Conv2D/base_model/batch_normalization_3/ReadVariableOp1base_model/batch_normalization_3/ReadVariableOp_1@base_model/batch_normalization_3/FusedBatchNormV3/ReadVariableOpBbase_model/batch_normalization_3/FusedBatchNormV3/ReadVariableOp_1*
epsilon%đ'7*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:*
T0*
U0*
is_training( 
k
&base_model/batch_normalization_3/ConstConst*
_output_shapes
: *
valueB
 *fff?*
dtype0
É
base_model/addAddV21base_model/batch_normalization_3/FusedBatchNormV31base_model/batch_normalization_1/FusedBatchNormV3*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
u
base_model/Relu_2Relubase_model/add*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
Ĺ
=base_model/conv2d_4/kernel/Initializer/truncated_normal/shapeConst*%
valueB"      @   @   *-
_class#
!loc:@base_model/conv2d_4/kernel*
dtype0*
_output_shapes
:
°
<base_model/conv2d_4/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
_output_shapes
: *
dtype0*-
_class#
!loc:@base_model/conv2d_4/kernel
˛
>base_model/conv2d_4/kernel/Initializer/truncated_normal/stddevConst*-
_class#
!loc:@base_model/conv2d_4/kernel*
valueB
 *B=*
dtype0*
_output_shapes
: 

Gbase_model/conv2d_4/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_4/kernel/Initializer/truncated_normal/shape*
T0*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_4/kernel*
dtype0
Ť
;base_model/conv2d_4/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_4/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_4/kernel/Initializer/truncated_normal/stddev*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_4/kernel*
T0

7base_model/conv2d_4/kernel/Initializer/truncated_normalAdd;base_model/conv2d_4/kernel/Initializer/truncated_normal/mul<base_model/conv2d_4/kernel/Initializer/truncated_normal/mean*-
_class#
!loc:@base_model/conv2d_4/kernel*
T0*&
_output_shapes
:@@
Ç
base_model/conv2d_4/kernelVarHandleOp*-
_class#
!loc:@base_model/conv2d_4/kernel*
_output_shapes
: *
shape:@@*
dtype0*+
shared_namebase_model/conv2d_4/kernel

;base_model/conv2d_4/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_4/kernel*
_output_shapes
: 

!base_model/conv2d_4/kernel/AssignAssignVariableOpbase_model/conv2d_4/kernel7base_model/conv2d_4/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_4/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_4/kernel*
dtype0*&
_output_shapes
:@@
r
!base_model/conv2d_4/dilation_rateConst*
valueB"      *
dtype0*
_output_shapes
:

)base_model/conv2d_4/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_4/kernel*
dtype0*&
_output_shapes
:@@
Ö
base_model/conv2d_4/Conv2DConv2Dbase_model/Relu_2)base_model/conv2d_4/Conv2D/ReadVariableOp*
strides
*
paddingSAME*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
ż
7base_model/batch_normalization_4/gamma/Initializer/onesConst*
_output_shapes
:@*
valueB@*  ?*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_4/gamma
ß
&base_model/batch_normalization_4/gammaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_4/gamma*
_output_shapes
: *
dtype0*7
shared_name(&base_model/batch_normalization_4/gamma*
shape:@

Gbase_model/batch_normalization_4/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_4/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_4/gamma/AssignAssignVariableOp&base_model/batch_normalization_4/gamma7base_model/batch_normalization_4/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_4/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_4/gamma*
dtype0*
_output_shapes
:@
ž
7base_model/batch_normalization_4/beta/Initializer/zerosConst*
dtype0*
valueB@*    *8
_class.
,*loc:@base_model/batch_normalization_4/beta*
_output_shapes
:@
Ü
%base_model/batch_normalization_4/betaVarHandleOp*6
shared_name'%base_model/batch_normalization_4/beta*
shape:@*
dtype0*8
_class.
,*loc:@base_model/batch_normalization_4/beta*
_output_shapes
: 

Fbase_model/batch_normalization_4/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_4/beta*
_output_shapes
: 
­
,base_model/batch_normalization_4/beta/AssignAssignVariableOp%base_model/batch_normalization_4/beta7base_model/batch_normalization_4/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_4/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_4/beta*
_output_shapes
:@*
dtype0
Ě
>base_model/batch_normalization_4/moving_mean/Initializer/zerosConst*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean*
_output_shapes
:@*
valueB@*    *
dtype0
ń
,base_model/batch_normalization_4/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*=
shared_name.,base_model/batch_normalization_4/moving_mean*
shape:@*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean
Š
Mbase_model/batch_normalization_4/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_4/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_4/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_4/moving_mean>base_model/batch_normalization_4/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_4/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_4/moving_mean*
dtype0*
_output_shapes
:@
Ó
Abase_model/batch_normalization_4/moving_variance/Initializer/onesConst*C
_class9
75loc:@base_model/batch_normalization_4/moving_variance*
_output_shapes
:@*
valueB@*  ?*
dtype0
ý
0base_model/batch_normalization_4/moving_varianceVarHandleOp*
dtype0*
shape:@*C
_class9
75loc:@base_model/batch_normalization_4/moving_variance*
_output_shapes
: *A
shared_name20base_model/batch_normalization_4/moving_variance
ą
Qbase_model/batch_normalization_4/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_4/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_4/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_4/moving_varianceAbase_model/batch_normalization_4/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_4/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_4/moving_variance*
_output_shapes
:@*
dtype0

/base_model/batch_normalization_4/ReadVariableOpReadVariableOp&base_model/batch_normalization_4/gamma*
_output_shapes
:@*
dtype0

1base_model/batch_normalization_4/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_4/beta*
dtype0*
_output_shapes
:@
Š
@base_model/batch_normalization_4/FusedBatchNormV3/ReadVariableOpReadVariableOp,base_model/batch_normalization_4/moving_mean*
dtype0*
_output_shapes
:@
Ż
Bbase_model/batch_normalization_4/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_4/moving_variance*
_output_shapes
:@*
dtype0
á
1base_model/batch_normalization_4/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_4/Conv2D/base_model/batch_normalization_4/ReadVariableOp1base_model/batch_normalization_4/ReadVariableOp_1@base_model/batch_normalization_4/FusedBatchNormV3/ReadVariableOpBbase_model/batch_normalization_4/FusedBatchNormV3/ReadVariableOp_1*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:*
U0*
is_training( *
T0*
epsilon%đ'7
k
&base_model/batch_normalization_4/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *fff?

base_model/Relu_3Relu1base_model/batch_normalization_4/FusedBatchNormV3*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
Ĺ
=base_model/conv2d_5/kernel/Initializer/truncated_normal/shapeConst*
dtype0*-
_class#
!loc:@base_model/conv2d_5/kernel*%
valueB"      @   @   *
_output_shapes
:
°
<base_model/conv2d_5/kernel/Initializer/truncated_normal/meanConst*-
_class#
!loc:@base_model/conv2d_5/kernel*
_output_shapes
: *
valueB
 *    *
dtype0
˛
>base_model/conv2d_5/kernel/Initializer/truncated_normal/stddevConst*
valueB
 *B=*-
_class#
!loc:@base_model/conv2d_5/kernel*
_output_shapes
: *
dtype0

Gbase_model/conv2d_5/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_5/kernel/Initializer/truncated_normal/shape*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_5/kernel*
dtype0*
T0
Ť
;base_model/conv2d_5/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_5/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_5/kernel/Initializer/truncated_normal/stddev*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_5/kernel*
T0

7base_model/conv2d_5/kernel/Initializer/truncated_normalAdd;base_model/conv2d_5/kernel/Initializer/truncated_normal/mul<base_model/conv2d_5/kernel/Initializer/truncated_normal/mean*-
_class#
!loc:@base_model/conv2d_5/kernel*&
_output_shapes
:@@*
T0
Ç
base_model/conv2d_5/kernelVarHandleOp*
shape:@@*+
shared_namebase_model/conv2d_5/kernel*-
_class#
!loc:@base_model/conv2d_5/kernel*
_output_shapes
: *
dtype0

;base_model/conv2d_5/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_5/kernel*
_output_shapes
: 

!base_model/conv2d_5/kernel/AssignAssignVariableOpbase_model/conv2d_5/kernel7base_model/conv2d_5/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_5/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_5/kernel*
dtype0*&
_output_shapes
:@@
r
!base_model/conv2d_5/dilation_rateConst*
valueB"      *
_output_shapes
:*
dtype0

)base_model/conv2d_5/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_5/kernel*
dtype0*&
_output_shapes
:@@
Ö
base_model/conv2d_5/Conv2DConv2Dbase_model/Relu_3)base_model/conv2d_5/Conv2D/ReadVariableOp*
strides
*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0*
paddingSAME
Ŕ
8base_model/batch_normalization_5/gamma/Initializer/zerosConst*9
_class/
-+loc:@base_model/batch_normalization_5/gamma*
valueB@*    *
_output_shapes
:@*
dtype0
ß
&base_model/batch_normalization_5/gammaVarHandleOp*
dtype0*
_output_shapes
: *
shape:@*7
shared_name(&base_model/batch_normalization_5/gamma*9
_class/
-+loc:@base_model/batch_normalization_5/gamma

Gbase_model/batch_normalization_5/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_5/gamma*
_output_shapes
: 
°
-base_model/batch_normalization_5/gamma/AssignAssignVariableOp&base_model/batch_normalization_5/gamma8base_model/batch_normalization_5/gamma/Initializer/zeros*
dtype0

:base_model/batch_normalization_5/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_5/gamma*
_output_shapes
:@*
dtype0
ž
7base_model/batch_normalization_5/beta/Initializer/zerosConst*
valueB@*    *
_output_shapes
:@*8
_class.
,*loc:@base_model/batch_normalization_5/beta*
dtype0
Ü
%base_model/batch_normalization_5/betaVarHandleOp*
dtype0*
shape:@*8
_class.
,*loc:@base_model/batch_normalization_5/beta*6
shared_name'%base_model/batch_normalization_5/beta*
_output_shapes
: 

Fbase_model/batch_normalization_5/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_5/beta*
_output_shapes
: 
­
,base_model/batch_normalization_5/beta/AssignAssignVariableOp%base_model/batch_normalization_5/beta7base_model/batch_normalization_5/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_5/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_5/beta*
dtype0*
_output_shapes
:@
Ě
>base_model/batch_normalization_5/moving_mean/Initializer/zerosConst*
dtype0*
valueB@*    *?
_class5
31loc:@base_model/batch_normalization_5/moving_mean*
_output_shapes
:@
ń
,base_model/batch_normalization_5/moving_meanVarHandleOp*
dtype0*?
_class5
31loc:@base_model/batch_normalization_5/moving_mean*
_output_shapes
: *=
shared_name.,base_model/batch_normalization_5/moving_mean*
shape:@
Š
Mbase_model/batch_normalization_5/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_5/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_5/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_5/moving_mean>base_model/batch_normalization_5/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_5/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_5/moving_mean*
dtype0*
_output_shapes
:@
Ó
Abase_model/batch_normalization_5/moving_variance/Initializer/onesConst*
valueB@*  ?*
dtype0*
_output_shapes
:@*C
_class9
75loc:@base_model/batch_normalization_5/moving_variance
ý
0base_model/batch_normalization_5/moving_varianceVarHandleOp*C
_class9
75loc:@base_model/batch_normalization_5/moving_variance*A
shared_name20base_model/batch_normalization_5/moving_variance*
_output_shapes
: *
dtype0*
shape:@
ą
Qbase_model/batch_normalization_5/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_5/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_5/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_5/moving_varianceAbase_model/batch_normalization_5/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_5/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_5/moving_variance*
_output_shapes
:@*
dtype0

/base_model/batch_normalization_5/ReadVariableOpReadVariableOp&base_model/batch_normalization_5/gamma*
_output_shapes
:@*
dtype0

1base_model/batch_normalization_5/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_5/beta*
dtype0*
_output_shapes
:@
Š
@base_model/batch_normalization_5/FusedBatchNormV3/ReadVariableOpReadVariableOp,base_model/batch_normalization_5/moving_mean*
dtype0*
_output_shapes
:@
Ż
Bbase_model/batch_normalization_5/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_5/moving_variance*
_output_shapes
:@*
dtype0
á
1base_model/batch_normalization_5/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_5/Conv2D/base_model/batch_normalization_5/ReadVariableOp1base_model/batch_normalization_5/ReadVariableOp_1@base_model/batch_normalization_5/FusedBatchNormV3/ReadVariableOpBbase_model/batch_normalization_5/FusedBatchNormV3/ReadVariableOp_1*
T0*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:*
U0*
is_training( *
epsilon%đ'7
k
&base_model/batch_normalization_5/ConstConst*
valueB
 *fff?*
dtype0*
_output_shapes
: 
Ť
base_model/add_1AddV21base_model/batch_normalization_5/FusedBatchNormV3base_model/Relu_2*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
w
base_model/Relu_4Relubase_model/add_1*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@

base_model/block_group1Identitybase_model/Relu_4*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0

base_model/Pad_1/paddingsConst*9
value0B."                                 *
_output_shapes

:*
dtype0

base_model/Pad_1Padbase_model/block_group1base_model/Pad_1/paddings*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
Ĺ
=base_model/conv2d_6/kernel/Initializer/truncated_normal/shapeConst*
_output_shapes
:*%
valueB"      @      *
dtype0*-
_class#
!loc:@base_model/conv2d_6/kernel
°
<base_model/conv2d_6/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_6/kernel*
dtype0*
valueB
 *    
˛
>base_model/conv2d_6/kernel/Initializer/truncated_normal/stddevConst*
dtype0*-
_class#
!loc:@base_model/conv2d_6/kernel*
_output_shapes
: *
valueB
 *6>

Gbase_model/conv2d_6/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_6/kernel/Initializer/truncated_normal/shape*
dtype0*
T0*-
_class#
!loc:@base_model/conv2d_6/kernel*'
_output_shapes
:@
Ź
;base_model/conv2d_6/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_6/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_6/kernel/Initializer/truncated_normal/stddev*'
_output_shapes
:@*
T0*-
_class#
!loc:@base_model/conv2d_6/kernel

7base_model/conv2d_6/kernel/Initializer/truncated_normalAdd;base_model/conv2d_6/kernel/Initializer/truncated_normal/mul<base_model/conv2d_6/kernel/Initializer/truncated_normal/mean*'
_output_shapes
:@*-
_class#
!loc:@base_model/conv2d_6/kernel*
T0
Č
base_model/conv2d_6/kernelVarHandleOp*
shape:@*+
shared_namebase_model/conv2d_6/kernel*
dtype0*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_6/kernel

;base_model/conv2d_6/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_6/kernel*
_output_shapes
: 

!base_model/conv2d_6/kernel/AssignAssignVariableOpbase_model/conv2d_6/kernel7base_model/conv2d_6/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_6/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_6/kernel*
dtype0*'
_output_shapes
:@
r
!base_model/conv2d_6/dilation_rateConst*
valueB"      *
dtype0*
_output_shapes
:

)base_model/conv2d_6/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_6/kernel*'
_output_shapes
:@*
dtype0
×
base_model/conv2d_6/Conv2DConv2Dbase_model/Pad_1)base_model/conv2d_6/Conv2D/ReadVariableOp*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0*
paddingVALID*
strides

Á
7base_model/batch_normalization_6/gamma/Initializer/onesConst*
_output_shapes	
:*
valueB*  ?*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_6/gamma
ŕ
&base_model/batch_normalization_6/gammaVarHandleOp*
shape:*7
shared_name(&base_model/batch_normalization_6/gamma*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_6/gamma*
_output_shapes
: 

Gbase_model/batch_normalization_6/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_6/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_6/gamma/AssignAssignVariableOp&base_model/batch_normalization_6/gamma7base_model/batch_normalization_6/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_6/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_6/gamma*
dtype0*
_output_shapes	
:
Ŕ
7base_model/batch_normalization_6/beta/Initializer/zerosConst*
dtype0*
_output_shapes	
:*8
_class.
,*loc:@base_model/batch_normalization_6/beta*
valueB*    
Ý
%base_model/batch_normalization_6/betaVarHandleOp*6
shared_name'%base_model/batch_normalization_6/beta*
dtype0*
_output_shapes
: *8
_class.
,*loc:@base_model/batch_normalization_6/beta*
shape:

Fbase_model/batch_normalization_6/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_6/beta*
_output_shapes
: 
­
,base_model/batch_normalization_6/beta/AssignAssignVariableOp%base_model/batch_normalization_6/beta7base_model/batch_normalization_6/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_6/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_6/beta*
_output_shapes	
:*
dtype0
Î
>base_model/batch_normalization_6/moving_mean/Initializer/zerosConst*
valueB*    *
dtype0*
_output_shapes	
:*?
_class5
31loc:@base_model/batch_normalization_6/moving_mean
ň
,base_model/batch_normalization_6/moving_meanVarHandleOp*
shape:*
dtype0*=
shared_name.,base_model/batch_normalization_6/moving_mean*?
_class5
31loc:@base_model/batch_normalization_6/moving_mean*
_output_shapes
: 
Š
Mbase_model/batch_normalization_6/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_6/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_6/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_6/moving_mean>base_model/batch_normalization_6/moving_mean/Initializer/zeros*
dtype0
Ş
@base_model/batch_normalization_6/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_6/moving_mean*
_output_shapes	
:*
dtype0
Ő
Abase_model/batch_normalization_6/moving_variance/Initializer/onesConst*
dtype0*
_output_shapes	
:*
valueB*  ?*C
_class9
75loc:@base_model/batch_normalization_6/moving_variance
ţ
0base_model/batch_normalization_6/moving_varianceVarHandleOp*A
shared_name20base_model/batch_normalization_6/moving_variance*C
_class9
75loc:@base_model/batch_normalization_6/moving_variance*
_output_shapes
: *
shape:*
dtype0
ą
Qbase_model/batch_normalization_6/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_6/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_6/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_6/moving_varianceAbase_model/batch_normalization_6/moving_variance/Initializer/ones*
dtype0
˛
Dbase_model/batch_normalization_6/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_6/moving_variance*
_output_shapes	
:*
dtype0

/base_model/batch_normalization_6/ReadVariableOpReadVariableOp&base_model/batch_normalization_6/gamma*
_output_shapes	
:*
dtype0

1base_model/batch_normalization_6/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_6/beta*
_output_shapes	
:*
dtype0
Ş
@base_model/batch_normalization_6/FusedBatchNormV3/ReadVariableOpReadVariableOp,base_model/batch_normalization_6/moving_mean*
_output_shapes	
:*
dtype0
°
Bbase_model/batch_normalization_6/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_6/moving_variance*
_output_shapes	
:*
dtype0
ć
1base_model/batch_normalization_6/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_6/Conv2D/base_model/batch_normalization_6/ReadVariableOp1base_model/batch_normalization_6/ReadVariableOp_1@base_model/batch_normalization_6/FusedBatchNormV3/ReadVariableOpBbase_model/batch_normalization_6/FusedBatchNormV3/ReadVariableOp_1*
is_training( *
U0*
T0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
epsilon%đ'7
k
&base_model/batch_normalization_6/ConstConst*
valueB
 *fff?*
_output_shapes
: *
dtype0

base_model/Pad_2/paddingsConst*
_output_shapes

:*9
value0B."                             *
dtype0

base_model/Pad_2Padbase_model/block_group1base_model/Pad_2/paddings*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
Ĺ
=base_model/conv2d_7/kernel/Initializer/truncated_normal/shapeConst*-
_class#
!loc:@base_model/conv2d_7/kernel*
_output_shapes
:*%
valueB"      @      *
dtype0
°
<base_model/conv2d_7/kernel/Initializer/truncated_normal/meanConst*-
_class#
!loc:@base_model/conv2d_7/kernel*
_output_shapes
: *
valueB
 *    *
dtype0
˛
>base_model/conv2d_7/kernel/Initializer/truncated_normal/stddevConst*
dtype0*-
_class#
!loc:@base_model/conv2d_7/kernel*
valueB
 *B=*
_output_shapes
: 

Gbase_model/conv2d_7/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_7/kernel/Initializer/truncated_normal/shape*
T0*-
_class#
!loc:@base_model/conv2d_7/kernel*
dtype0*'
_output_shapes
:@
Ź
;base_model/conv2d_7/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_7/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_7/kernel/Initializer/truncated_normal/stddev*-
_class#
!loc:@base_model/conv2d_7/kernel*
T0*'
_output_shapes
:@

7base_model/conv2d_7/kernel/Initializer/truncated_normalAdd;base_model/conv2d_7/kernel/Initializer/truncated_normal/mul<base_model/conv2d_7/kernel/Initializer/truncated_normal/mean*'
_output_shapes
:@*-
_class#
!loc:@base_model/conv2d_7/kernel*
T0
Č
base_model/conv2d_7/kernelVarHandleOp*
shape:@*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_7/kernel*
dtype0*+
shared_namebase_model/conv2d_7/kernel

;base_model/conv2d_7/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_7/kernel*
_output_shapes
: 

!base_model/conv2d_7/kernel/AssignAssignVariableOpbase_model/conv2d_7/kernel7base_model/conv2d_7/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_7/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_7/kernel*'
_output_shapes
:@*
dtype0
r
!base_model/conv2d_7/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB"      

)base_model/conv2d_7/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_7/kernel*'
_output_shapes
:@*
dtype0
×
base_model/conv2d_7/Conv2DConv2Dbase_model/Pad_2)base_model/conv2d_7/Conv2D/ReadVariableOp*
T0*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingVALID
Á
7base_model/batch_normalization_7/gamma/Initializer/onesConst*9
_class/
-+loc:@base_model/batch_normalization_7/gamma*
valueB*  ?*
_output_shapes	
:*
dtype0
ŕ
&base_model/batch_normalization_7/gammaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_7/gamma*
dtype0*
shape:*
_output_shapes
: *7
shared_name(&base_model/batch_normalization_7/gamma

Gbase_model/batch_normalization_7/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_7/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_7/gamma/AssignAssignVariableOp&base_model/batch_normalization_7/gamma7base_model/batch_normalization_7/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_7/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_7/gamma*
_output_shapes	
:*
dtype0
Ŕ
7base_model/batch_normalization_7/beta/Initializer/zerosConst*
dtype0*8
_class.
,*loc:@base_model/batch_normalization_7/beta*
_output_shapes	
:*
valueB*    
Ý
%base_model/batch_normalization_7/betaVarHandleOp*
_output_shapes
: *8
_class.
,*loc:@base_model/batch_normalization_7/beta*
dtype0*6
shared_name'%base_model/batch_normalization_7/beta*
shape:

Fbase_model/batch_normalization_7/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_7/beta*
_output_shapes
: 
­
,base_model/batch_normalization_7/beta/AssignAssignVariableOp%base_model/batch_normalization_7/beta7base_model/batch_normalization_7/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_7/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_7/beta*
_output_shapes	
:*
dtype0
Î
>base_model/batch_normalization_7/moving_mean/Initializer/zerosConst*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean*
dtype0*
_output_shapes	
:*
valueB*    
ň
,base_model/batch_normalization_7/moving_meanVarHandleOp*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean*
shape:*=
shared_name.,base_model/batch_normalization_7/moving_mean*
dtype0*
_output_shapes
: 
Š
Mbase_model/batch_normalization_7/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_7/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_7/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_7/moving_mean>base_model/batch_normalization_7/moving_mean/Initializer/zeros*
dtype0
Ş
@base_model/batch_normalization_7/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_7/moving_mean*
_output_shapes	
:*
dtype0
Ő
Abase_model/batch_normalization_7/moving_variance/Initializer/onesConst*
dtype0*
valueB*  ?*
_output_shapes	
:*C
_class9
75loc:@base_model/batch_normalization_7/moving_variance
ţ
0base_model/batch_normalization_7/moving_varianceVarHandleOp*A
shared_name20base_model/batch_normalization_7/moving_variance*
shape:*C
_class9
75loc:@base_model/batch_normalization_7/moving_variance*
dtype0*
_output_shapes
: 
ą
Qbase_model/batch_normalization_7/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_7/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_7/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_7/moving_varianceAbase_model/batch_normalization_7/moving_variance/Initializer/ones*
dtype0
˛
Dbase_model/batch_normalization_7/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_7/moving_variance*
_output_shapes	
:*
dtype0

/base_model/batch_normalization_7/ReadVariableOpReadVariableOp&base_model/batch_normalization_7/gamma*
_output_shapes	
:*
dtype0

1base_model/batch_normalization_7/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_7/beta*
_output_shapes	
:*
dtype0
Ş
@base_model/batch_normalization_7/FusedBatchNormV3/ReadVariableOpReadVariableOp,base_model/batch_normalization_7/moving_mean*
_output_shapes	
:*
dtype0
°
Bbase_model/batch_normalization_7/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_7/moving_variance*
dtype0*
_output_shapes	
:
ć
1base_model/batch_normalization_7/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_7/Conv2D/base_model/batch_normalization_7/ReadVariableOp1base_model/batch_normalization_7/ReadVariableOp_1@base_model/batch_normalization_7/FusedBatchNormV3/ReadVariableOpBbase_model/batch_normalization_7/FusedBatchNormV3/ReadVariableOp_1*
is_training( *b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
U0*
T0*
epsilon%đ'7
k
&base_model/batch_normalization_7/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *fff?

base_model/Relu_5Relu1base_model/batch_normalization_7/FusedBatchNormV3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ĺ
=base_model/conv2d_8/kernel/Initializer/truncated_normal/shapeConst*
dtype0*
_output_shapes
:*%
valueB"            *-
_class#
!loc:@base_model/conv2d_8/kernel
°
<base_model/conv2d_8/kernel/Initializer/truncated_normal/meanConst*
dtype0*-
_class#
!loc:@base_model/conv2d_8/kernel*
valueB
 *    *
_output_shapes
: 
˛
>base_model/conv2d_8/kernel/Initializer/truncated_normal/stddevConst*-
_class#
!loc:@base_model/conv2d_8/kernel*
valueB
 *¸1	=*
_output_shapes
: *
dtype0

Gbase_model/conv2d_8/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_8/kernel/Initializer/truncated_normal/shape*-
_class#
!loc:@base_model/conv2d_8/kernel*
dtype0*(
_output_shapes
:*
T0
­
;base_model/conv2d_8/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_8/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_8/kernel/Initializer/truncated_normal/stddev*
T0*(
_output_shapes
:*-
_class#
!loc:@base_model/conv2d_8/kernel

7base_model/conv2d_8/kernel/Initializer/truncated_normalAdd;base_model/conv2d_8/kernel/Initializer/truncated_normal/mul<base_model/conv2d_8/kernel/Initializer/truncated_normal/mean*
T0*-
_class#
!loc:@base_model/conv2d_8/kernel*(
_output_shapes
:
É
base_model/conv2d_8/kernelVarHandleOp*
dtype0*
shape:*-
_class#
!loc:@base_model/conv2d_8/kernel*
_output_shapes
: *+
shared_namebase_model/conv2d_8/kernel

;base_model/conv2d_8/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_8/kernel*
_output_shapes
: 

!base_model/conv2d_8/kernel/AssignAssignVariableOpbase_model/conv2d_8/kernel7base_model/conv2d_8/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_8/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_8/kernel*(
_output_shapes
:*
dtype0
r
!base_model/conv2d_8/dilation_rateConst*
valueB"      *
dtype0*
_output_shapes
:

)base_model/conv2d_8/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_8/kernel*
dtype0*(
_output_shapes
:
×
base_model/conv2d_8/Conv2DConv2Dbase_model/Relu_5)base_model/conv2d_8/Conv2D/ReadVariableOp*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
strides
*
T0*
paddingSAME
Â
8base_model/batch_normalization_8/gamma/Initializer/zerosConst*
valueB*    *
_output_shapes	
:*9
_class/
-+loc:@base_model/batch_normalization_8/gamma*
dtype0
ŕ
&base_model/batch_normalization_8/gammaVarHandleOp*7
shared_name(&base_model/batch_normalization_8/gamma*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_8/gamma*
_output_shapes
: *
shape:

Gbase_model/batch_normalization_8/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_8/gamma*
_output_shapes
: 
°
-base_model/batch_normalization_8/gamma/AssignAssignVariableOp&base_model/batch_normalization_8/gamma8base_model/batch_normalization_8/gamma/Initializer/zeros*
dtype0

:base_model/batch_normalization_8/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_8/gamma*
dtype0*
_output_shapes	
:
Ŕ
7base_model/batch_normalization_8/beta/Initializer/zerosConst*8
_class.
,*loc:@base_model/batch_normalization_8/beta*
valueB*    *
dtype0*
_output_shapes	
:
Ý
%base_model/batch_normalization_8/betaVarHandleOp*6
shared_name'%base_model/batch_normalization_8/beta*
dtype0*
_output_shapes
: *8
_class.
,*loc:@base_model/batch_normalization_8/beta*
shape:

Fbase_model/batch_normalization_8/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_8/beta*
_output_shapes
: 
­
,base_model/batch_normalization_8/beta/AssignAssignVariableOp%base_model/batch_normalization_8/beta7base_model/batch_normalization_8/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_8/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_8/beta*
dtype0*
_output_shapes	
:
Î
>base_model/batch_normalization_8/moving_mean/Initializer/zerosConst*
_output_shapes	
:*
dtype0*
valueB*    *?
_class5
31loc:@base_model/batch_normalization_8/moving_mean
ň
,base_model/batch_normalization_8/moving_meanVarHandleOp*
dtype0*=
shared_name.,base_model/batch_normalization_8/moving_mean*
_output_shapes
: *?
_class5
31loc:@base_model/batch_normalization_8/moving_mean*
shape:
Š
Mbase_model/batch_normalization_8/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_8/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_8/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_8/moving_mean>base_model/batch_normalization_8/moving_mean/Initializer/zeros*
dtype0
Ş
@base_model/batch_normalization_8/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_8/moving_mean*
_output_shapes	
:*
dtype0
Ő
Abase_model/batch_normalization_8/moving_variance/Initializer/onesConst*
dtype0*C
_class9
75loc:@base_model/batch_normalization_8/moving_variance*
_output_shapes	
:*
valueB*  ?
ţ
0base_model/batch_normalization_8/moving_varianceVarHandleOp*C
_class9
75loc:@base_model/batch_normalization_8/moving_variance*
_output_shapes
: *
dtype0*
shape:*A
shared_name20base_model/batch_normalization_8/moving_variance
ą
Qbase_model/batch_normalization_8/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_8/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_8/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_8/moving_varianceAbase_model/batch_normalization_8/moving_variance/Initializer/ones*
dtype0
˛
Dbase_model/batch_normalization_8/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_8/moving_variance*
dtype0*
_output_shapes	
:

/base_model/batch_normalization_8/ReadVariableOpReadVariableOp&base_model/batch_normalization_8/gamma*
_output_shapes	
:*
dtype0

1base_model/batch_normalization_8/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_8/beta*
dtype0*
_output_shapes	
:
Ş
@base_model/batch_normalization_8/FusedBatchNormV3/ReadVariableOpReadVariableOp,base_model/batch_normalization_8/moving_mean*
dtype0*
_output_shapes	
:
°
Bbase_model/batch_normalization_8/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_8/moving_variance*
_output_shapes	
:*
dtype0
ć
1base_model/batch_normalization_8/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_8/Conv2D/base_model/batch_normalization_8/ReadVariableOp1base_model/batch_normalization_8/ReadVariableOp_1@base_model/batch_normalization_8/FusedBatchNormV3/ReadVariableOpBbase_model/batch_normalization_8/FusedBatchNormV3/ReadVariableOp_1*
U0*
is_training( *
T0*
epsilon%đ'7*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
k
&base_model/batch_normalization_8/ConstConst*
_output_shapes
: *
valueB
 *fff?*
dtype0
Ě
base_model/add_2AddV21base_model/batch_normalization_8/FusedBatchNormV31base_model/batch_normalization_6/FusedBatchNormV3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
x
base_model/Relu_6Relubase_model/add_2*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ĺ
=base_model/conv2d_9/kernel/Initializer/truncated_normal/shapeConst*%
valueB"            *
dtype0*
_output_shapes
:*-
_class#
!loc:@base_model/conv2d_9/kernel
°
<base_model/conv2d_9/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *-
_class#
!loc:@base_model/conv2d_9/kernel*
_output_shapes
: *
dtype0
˛
>base_model/conv2d_9/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_9/kernel*
valueB
 *¸1	=*
dtype0

Gbase_model/conv2d_9/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_9/kernel/Initializer/truncated_normal/shape*
dtype0*(
_output_shapes
:*
T0*-
_class#
!loc:@base_model/conv2d_9/kernel
­
;base_model/conv2d_9/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_9/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_9/kernel/Initializer/truncated_normal/stddev*
T0*(
_output_shapes
:*-
_class#
!loc:@base_model/conv2d_9/kernel

7base_model/conv2d_9/kernel/Initializer/truncated_normalAdd;base_model/conv2d_9/kernel/Initializer/truncated_normal/mul<base_model/conv2d_9/kernel/Initializer/truncated_normal/mean*
T0*-
_class#
!loc:@base_model/conv2d_9/kernel*(
_output_shapes
:
É
base_model/conv2d_9/kernelVarHandleOp*-
_class#
!loc:@base_model/conv2d_9/kernel*
dtype0*
_output_shapes
: *
shape:*+
shared_namebase_model/conv2d_9/kernel

;base_model/conv2d_9/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_9/kernel*
_output_shapes
: 

!base_model/conv2d_9/kernel/AssignAssignVariableOpbase_model/conv2d_9/kernel7base_model/conv2d_9/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_9/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_9/kernel*(
_output_shapes
:*
dtype0
r
!base_model/conv2d_9/dilation_rateConst*
_output_shapes
:*
valueB"      *
dtype0

)base_model/conv2d_9/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_9/kernel*(
_output_shapes
:*
dtype0
×
base_model/conv2d_9/Conv2DConv2Dbase_model/Relu_6)base_model/conv2d_9/Conv2D/ReadVariableOp*
T0*
paddingSAME*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
strides

Á
7base_model/batch_normalization_9/gamma/Initializer/onesConst*
_output_shapes	
:*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_9/gamma*
valueB*  ?
ŕ
&base_model/batch_normalization_9/gammaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_9/gamma*
dtype0*7
shared_name(&base_model/batch_normalization_9/gamma*
_output_shapes
: *
shape:

Gbase_model/batch_normalization_9/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_9/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_9/gamma/AssignAssignVariableOp&base_model/batch_normalization_9/gamma7base_model/batch_normalization_9/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_9/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_9/gamma*
_output_shapes	
:*
dtype0
Ŕ
7base_model/batch_normalization_9/beta/Initializer/zerosConst*
valueB*    *8
_class.
,*loc:@base_model/batch_normalization_9/beta*
dtype0*
_output_shapes	
:
Ý
%base_model/batch_normalization_9/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:*6
shared_name'%base_model/batch_normalization_9/beta*8
_class.
,*loc:@base_model/batch_normalization_9/beta

Fbase_model/batch_normalization_9/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_9/beta*
_output_shapes
: 
­
,base_model/batch_normalization_9/beta/AssignAssignVariableOp%base_model/batch_normalization_9/beta7base_model/batch_normalization_9/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_9/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_9/beta*
_output_shapes	
:*
dtype0
Î
>base_model/batch_normalization_9/moving_mean/Initializer/zerosConst*
dtype0*
_output_shapes	
:*?
_class5
31loc:@base_model/batch_normalization_9/moving_mean*
valueB*    
ň
,base_model/batch_normalization_9/moving_meanVarHandleOp*?
_class5
31loc:@base_model/batch_normalization_9/moving_mean*
_output_shapes
: *
shape:*
dtype0*=
shared_name.,base_model/batch_normalization_9/moving_mean
Š
Mbase_model/batch_normalization_9/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_9/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_9/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_9/moving_mean>base_model/batch_normalization_9/moving_mean/Initializer/zeros*
dtype0
Ş
@base_model/batch_normalization_9/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_9/moving_mean*
_output_shapes	
:*
dtype0
Ő
Abase_model/batch_normalization_9/moving_variance/Initializer/onesConst*C
_class9
75loc:@base_model/batch_normalization_9/moving_variance*
valueB*  ?*
_output_shapes	
:*
dtype0
ţ
0base_model/batch_normalization_9/moving_varianceVarHandleOp*A
shared_name20base_model/batch_normalization_9/moving_variance*
shape:*
dtype0*C
_class9
75loc:@base_model/batch_normalization_9/moving_variance*
_output_shapes
: 
ą
Qbase_model/batch_normalization_9/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_9/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_9/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_9/moving_varianceAbase_model/batch_normalization_9/moving_variance/Initializer/ones*
dtype0
˛
Dbase_model/batch_normalization_9/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_9/moving_variance*
dtype0*
_output_shapes	
:

/base_model/batch_normalization_9/ReadVariableOpReadVariableOp&base_model/batch_normalization_9/gamma*
dtype0*
_output_shapes	
:

1base_model/batch_normalization_9/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_9/beta*
_output_shapes	
:*
dtype0
Ş
@base_model/batch_normalization_9/FusedBatchNormV3/ReadVariableOpReadVariableOp,base_model/batch_normalization_9/moving_mean*
dtype0*
_output_shapes	
:
°
Bbase_model/batch_normalization_9/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_9/moving_variance*
dtype0*
_output_shapes	
:
ć
1base_model/batch_normalization_9/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_9/Conv2D/base_model/batch_normalization_9/ReadVariableOp1base_model/batch_normalization_9/ReadVariableOp_1@base_model/batch_normalization_9/FusedBatchNormV3/ReadVariableOpBbase_model/batch_normalization_9/FusedBatchNormV3/ReadVariableOp_1*
epsilon%đ'7*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
T0*
U0*
is_training( 
k
&base_model/batch_normalization_9/ConstConst*
valueB
 *fff?*
dtype0*
_output_shapes
: 

base_model/Relu_7Relu1base_model/batch_normalization_9/FusedBatchNormV3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ç
>base_model/conv2d_10/kernel/Initializer/truncated_normal/shapeConst*%
valueB"            *.
_class$
" loc:@base_model/conv2d_10/kernel*
_output_shapes
:*
dtype0
˛
=base_model/conv2d_10/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_10/kernel*
valueB
 *    *
dtype0
´
?base_model/conv2d_10/kernel/Initializer/truncated_normal/stddevConst*
dtype0*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_10/kernel*
valueB
 *¸1	=

Hbase_model/conv2d_10/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_10/kernel/Initializer/truncated_normal/shape*.
_class$
" loc:@base_model/conv2d_10/kernel*
T0*
dtype0*(
_output_shapes
:
ą
<base_model/conv2d_10/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_10/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_10/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_10/kernel*(
_output_shapes
:*
T0

8base_model/conv2d_10/kernel/Initializer/truncated_normalAdd<base_model/conv2d_10/kernel/Initializer/truncated_normal/mul=base_model/conv2d_10/kernel/Initializer/truncated_normal/mean*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_10/kernel*
T0
Ě
base_model/conv2d_10/kernelVarHandleOp*,
shared_namebase_model/conv2d_10/kernel*
_output_shapes
: *
dtype0*
shape:*.
_class$
" loc:@base_model/conv2d_10/kernel

<base_model/conv2d_10/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_10/kernel*
_output_shapes
: 

"base_model/conv2d_10/kernel/AssignAssignVariableOpbase_model/conv2d_10/kernel8base_model/conv2d_10/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_10/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_10/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_10/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB"      

*base_model/conv2d_10/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_10/kernel*
dtype0*(
_output_shapes
:
Ů
base_model/conv2d_10/Conv2DConv2Dbase_model/Relu_7*base_model/conv2d_10/Conv2D/ReadVariableOp*
paddingSAME*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0*
strides

Ä
9base_model/batch_normalization_10/gamma/Initializer/zerosConst*
valueB*    *:
_class0
.,loc:@base_model/batch_normalization_10/gamma*
_output_shapes	
:*
dtype0
ă
'base_model/batch_normalization_10/gammaVarHandleOp*
shape:*8
shared_name)'base_model/batch_normalization_10/gamma*:
_class0
.,loc:@base_model/batch_normalization_10/gamma*
dtype0*
_output_shapes
: 

Hbase_model/batch_normalization_10/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_10/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_10/gamma/AssignAssignVariableOp'base_model/batch_normalization_10/gamma9base_model/batch_normalization_10/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_10/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_10/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_10/beta/Initializer/zerosConst*
valueB*    *9
_class/
-+loc:@base_model/batch_normalization_10/beta*
dtype0*
_output_shapes	
:
ŕ
&base_model/batch_normalization_10/betaVarHandleOp*
shape:*
dtype0*
_output_shapes
: *9
_class/
-+loc:@base_model/batch_normalization_10/beta*7
shared_name(&base_model/batch_normalization_10/beta

Gbase_model/batch_normalization_10/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_10/beta*
_output_shapes
: 
°
-base_model/batch_normalization_10/beta/AssignAssignVariableOp&base_model/batch_normalization_10/beta8base_model/batch_normalization_10/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_10/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_10/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_10/moving_mean/Initializer/zerosConst*
dtype0*
valueB*    *@
_class6
42loc:@base_model/batch_normalization_10/moving_mean*
_output_shapes	
:
ő
-base_model/batch_normalization_10/moving_meanVarHandleOp*
_output_shapes
: *
shape:*
dtype0*@
_class6
42loc:@base_model/batch_normalization_10/moving_mean*>
shared_name/-base_model/batch_normalization_10/moving_mean
Ť
Nbase_model/batch_normalization_10/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_10/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_10/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_10/moving_mean?base_model/batch_normalization_10/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_10/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_10/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_10/moving_variance/Initializer/onesConst*D
_class:
86loc:@base_model/batch_normalization_10/moving_variance*
dtype0*
_output_shapes	
:*
valueB*  ?

1base_model/batch_normalization_10/moving_varianceVarHandleOp*
shape:*
dtype0*
_output_shapes
: *D
_class:
86loc:@base_model/batch_normalization_10/moving_variance*B
shared_name31base_model/batch_normalization_10/moving_variance
ł
Rbase_model/batch_normalization_10/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_10/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_10/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_10/moving_varianceBbase_model/batch_normalization_10/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_10/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_10/moving_variance*
_output_shapes	
:*
dtype0

0base_model/batch_normalization_10/ReadVariableOpReadVariableOp'base_model/batch_normalization_10/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_10/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_10/beta*
dtype0*
_output_shapes	
:
Ź
Abase_model/batch_normalization_10/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_10/moving_mean*
_output_shapes	
:*
dtype0
˛
Cbase_model/batch_normalization_10/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_10/moving_variance*
dtype0*
_output_shapes	
:
ě
2base_model/batch_normalization_10/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_10/Conv2D0base_model/batch_normalization_10/ReadVariableOp2base_model/batch_normalization_10/ReadVariableOp_1Abase_model/batch_normalization_10/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_10/FusedBatchNormV3/ReadVariableOp_1*
U0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
T0*
is_training( *
epsilon%đ'7
l
'base_model/batch_normalization_10/ConstConst*
dtype0*
valueB
 *fff?*
_output_shapes
: 
­
base_model/add_3AddV22base_model/batch_normalization_10/FusedBatchNormV3base_model/Relu_6*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
x
base_model/Relu_8Relubase_model/add_3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0

base_model/block_group2Identitybase_model/Relu_8*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0

base_model/Pad_3/paddingsConst*9
value0B."                                 *
_output_shapes

:*
dtype0

base_model/Pad_3Padbase_model/block_group2base_model/Pad_3/paddings*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_11/kernel/Initializer/truncated_normal/shapeConst*%
valueB"            *.
_class$
" loc:@base_model/conv2d_11/kernel*
dtype0*
_output_shapes
:
˛
=base_model/conv2d_11/kernel/Initializer/truncated_normal/meanConst*.
_class$
" loc:@base_model/conv2d_11/kernel*
valueB
 *    *
_output_shapes
: *
dtype0
´
?base_model/conv2d_11/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
valueB
 *ĘÍ=*
dtype0*.
_class$
" loc:@base_model/conv2d_11/kernel

Hbase_model/conv2d_11/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_11/kernel/Initializer/truncated_normal/shape*
dtype0*.
_class$
" loc:@base_model/conv2d_11/kernel*
T0*(
_output_shapes
:
ą
<base_model/conv2d_11/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_11/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_11/kernel/Initializer/truncated_normal/stddev*(
_output_shapes
:*
T0*.
_class$
" loc:@base_model/conv2d_11/kernel

8base_model/conv2d_11/kernel/Initializer/truncated_normalAdd<base_model/conv2d_11/kernel/Initializer/truncated_normal/mul=base_model/conv2d_11/kernel/Initializer/truncated_normal/mean*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_11/kernel*
T0
Ě
base_model/conv2d_11/kernelVarHandleOp*,
shared_namebase_model/conv2d_11/kernel*
_output_shapes
: *
shape:*.
_class$
" loc:@base_model/conv2d_11/kernel*
dtype0

<base_model/conv2d_11/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_11/kernel*
_output_shapes
: 

"base_model/conv2d_11/kernel/AssignAssignVariableOpbase_model/conv2d_11/kernel8base_model/conv2d_11/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_11/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_11/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_11/dilation_rateConst*
_output_shapes
:*
valueB"      *
dtype0

*base_model/conv2d_11/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_11/kernel*(
_output_shapes
:*
dtype0
Ů
base_model/conv2d_11/Conv2DConv2Dbase_model/Pad_3*base_model/conv2d_11/Conv2D/ReadVariableOp*
T0*
strides
*
paddingVALID*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ă
8base_model/batch_normalization_11/gamma/Initializer/onesConst*
dtype0*
valueB*  ?*:
_class0
.,loc:@base_model/batch_normalization_11/gamma*
_output_shapes	
:
ă
'base_model/batch_normalization_11/gammaVarHandleOp*8
shared_name)'base_model/batch_normalization_11/gamma*
shape:*
_output_shapes
: *:
_class0
.,loc:@base_model/batch_normalization_11/gamma*
dtype0

Hbase_model/batch_normalization_11/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_11/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_11/gamma/AssignAssignVariableOp'base_model/batch_normalization_11/gamma8base_model/batch_normalization_11/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_11/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_11/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_11/beta/Initializer/zerosConst*9
_class/
-+loc:@base_model/batch_normalization_11/beta*
valueB*    *
_output_shapes	
:*
dtype0
ŕ
&base_model/batch_normalization_11/betaVarHandleOp*
shape:*
_output_shapes
: *
dtype0*9
_class/
-+loc:@base_model/batch_normalization_11/beta*7
shared_name(&base_model/batch_normalization_11/beta

Gbase_model/batch_normalization_11/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_11/beta*
_output_shapes
: 
°
-base_model/batch_normalization_11/beta/AssignAssignVariableOp&base_model/batch_normalization_11/beta8base_model/batch_normalization_11/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_11/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_11/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_11/moving_mean/Initializer/zerosConst*@
_class6
42loc:@base_model/batch_normalization_11/moving_mean*
valueB*    *
_output_shapes	
:*
dtype0
ő
-base_model/batch_normalization_11/moving_meanVarHandleOp*
_output_shapes
: *
dtype0*
shape:*@
_class6
42loc:@base_model/batch_normalization_11/moving_mean*>
shared_name/-base_model/batch_normalization_11/moving_mean
Ť
Nbase_model/batch_normalization_11/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_11/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_11/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_11/moving_mean?base_model/batch_normalization_11/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_11/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_11/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_11/moving_variance/Initializer/onesConst*
_output_shapes	
:*
dtype0*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance*
valueB*  ?

1base_model/batch_normalization_11/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_11/moving_variance*
dtype0*
shape:*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance*
_output_shapes
: 
ł
Rbase_model/batch_normalization_11/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_11/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_11/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_11/moving_varianceBbase_model/batch_normalization_11/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_11/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_11/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_11/ReadVariableOpReadVariableOp'base_model/batch_normalization_11/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_11/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_11/beta*
_output_shapes	
:*
dtype0
Ź
Abase_model/batch_normalization_11/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_11/moving_mean*
_output_shapes	
:*
dtype0
˛
Cbase_model/batch_normalization_11/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_11/moving_variance*
dtype0*
_output_shapes	
:
ě
2base_model/batch_normalization_11/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_11/Conv2D0base_model/batch_normalization_11/ReadVariableOp2base_model/batch_normalization_11/ReadVariableOp_1Abase_model/batch_normalization_11/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_11/FusedBatchNormV3/ReadVariableOp_1*
is_training( *
U0*
T0*
epsilon%đ'7*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
l
'base_model/batch_normalization_11/ConstConst*
dtype0*
valueB
 *fff?*
_output_shapes
: 

base_model/Pad_4/paddingsConst*
dtype0*
_output_shapes

:*9
value0B."                             

base_model/Pad_4Padbase_model/block_group2base_model/Pad_4/paddings*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_12/kernel/Initializer/truncated_normal/shapeConst*
dtype0*.
_class$
" loc:@base_model/conv2d_12/kernel*
_output_shapes
:*%
valueB"            
˛
=base_model/conv2d_12/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
_output_shapes
: *
dtype0*.
_class$
" loc:@base_model/conv2d_12/kernel
´
?base_model/conv2d_12/kernel/Initializer/truncated_normal/stddevConst*
valueB
 *¸1	=*.
_class$
" loc:@base_model/conv2d_12/kernel*
_output_shapes
: *
dtype0

Hbase_model/conv2d_12/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_12/kernel/Initializer/truncated_normal/shape*(
_output_shapes
:*
T0*.
_class$
" loc:@base_model/conv2d_12/kernel*
dtype0
ą
<base_model/conv2d_12/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_12/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_12/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_12/kernel*(
_output_shapes
:*
T0

8base_model/conv2d_12/kernel/Initializer/truncated_normalAdd<base_model/conv2d_12/kernel/Initializer/truncated_normal/mul=base_model/conv2d_12/kernel/Initializer/truncated_normal/mean*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_12/kernel
Ě
base_model/conv2d_12/kernelVarHandleOp*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_12/kernel*,
shared_namebase_model/conv2d_12/kernel*
shape:*
dtype0

<base_model/conv2d_12/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_12/kernel*
_output_shapes
: 

"base_model/conv2d_12/kernel/AssignAssignVariableOpbase_model/conv2d_12/kernel8base_model/conv2d_12/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_12/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_12/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_12/dilation_rateConst*
valueB"      *
dtype0*
_output_shapes
:

*base_model/conv2d_12/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_12/kernel*
dtype0*(
_output_shapes
:
Ů
base_model/conv2d_12/Conv2DConv2Dbase_model/Pad_4*base_model/conv2d_12/Conv2D/ReadVariableOp*
strides
*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingVALID
Ă
8base_model/batch_normalization_12/gamma/Initializer/onesConst*
valueB*  ?*:
_class0
.,loc:@base_model/batch_normalization_12/gamma*
dtype0*
_output_shapes	
:
ă
'base_model/batch_normalization_12/gammaVarHandleOp*
_output_shapes
: *8
shared_name)'base_model/batch_normalization_12/gamma*:
_class0
.,loc:@base_model/batch_normalization_12/gamma*
dtype0*
shape:

Hbase_model/batch_normalization_12/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_12/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_12/gamma/AssignAssignVariableOp'base_model/batch_normalization_12/gamma8base_model/batch_normalization_12/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_12/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_12/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_12/beta/Initializer/zerosConst*
dtype0*
valueB*    *9
_class/
-+loc:@base_model/batch_normalization_12/beta*
_output_shapes	
:
ŕ
&base_model/batch_normalization_12/betaVarHandleOp*7
shared_name(&base_model/batch_normalization_12/beta*
_output_shapes
: *
shape:*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_12/beta

Gbase_model/batch_normalization_12/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_12/beta*
_output_shapes
: 
°
-base_model/batch_normalization_12/beta/AssignAssignVariableOp&base_model/batch_normalization_12/beta8base_model/batch_normalization_12/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_12/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_12/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_12/moving_mean/Initializer/zerosConst*
dtype0*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean*
valueB*    
ő
-base_model/batch_normalization_12/moving_meanVarHandleOp*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean*
shape:*>
shared_name/-base_model/batch_normalization_12/moving_mean*
_output_shapes
: *
dtype0
Ť
Nbase_model/batch_normalization_12/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_12/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_12/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_12/moving_mean?base_model/batch_normalization_12/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_12/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_12/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_12/moving_variance/Initializer/onesConst*
dtype0*
_output_shapes	
:*
valueB*  ?*D
_class:
86loc:@base_model/batch_normalization_12/moving_variance

1base_model/batch_normalization_12/moving_varianceVarHandleOp*
dtype0*
shape:*B
shared_name31base_model/batch_normalization_12/moving_variance*
_output_shapes
: *D
_class:
86loc:@base_model/batch_normalization_12/moving_variance
ł
Rbase_model/batch_normalization_12/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_12/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_12/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_12/moving_varianceBbase_model/batch_normalization_12/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_12/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_12/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_12/ReadVariableOpReadVariableOp'base_model/batch_normalization_12/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_12/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_12/beta*
_output_shapes	
:*
dtype0
Ź
Abase_model/batch_normalization_12/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_12/moving_mean*
_output_shapes	
:*
dtype0
˛
Cbase_model/batch_normalization_12/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_12/moving_variance*
_output_shapes	
:*
dtype0
ě
2base_model/batch_normalization_12/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_12/Conv2D0base_model/batch_normalization_12/ReadVariableOp2base_model/batch_normalization_12/ReadVariableOp_1Abase_model/batch_normalization_12/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_12/FusedBatchNormV3/ReadVariableOp_1*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
U0*
T0*
epsilon%đ'7*
is_training( 
l
'base_model/batch_normalization_12/ConstConst*
valueB
 *fff?*
dtype0*
_output_shapes
: 

base_model/Relu_9Relu2base_model/batch_normalization_12/FusedBatchNormV3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_13/kernel/Initializer/truncated_normal/shapeConst*
dtype0*.
_class$
" loc:@base_model/conv2d_13/kernel*
_output_shapes
:*%
valueB"            
˛
=base_model/conv2d_13/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
dtype0*.
_class$
" loc:@base_model/conv2d_13/kernel*
_output_shapes
: 
´
?base_model/conv2d_13/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
valueB
 *Â<*
dtype0*.
_class$
" loc:@base_model/conv2d_13/kernel

Hbase_model/conv2d_13/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_13/kernel/Initializer/truncated_normal/shape*(
_output_shapes
:*
T0*.
_class$
" loc:@base_model/conv2d_13/kernel*
dtype0
ą
<base_model/conv2d_13/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_13/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_13/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_13/kernel*(
_output_shapes
:*
T0

8base_model/conv2d_13/kernel/Initializer/truncated_normalAdd<base_model/conv2d_13/kernel/Initializer/truncated_normal/mul=base_model/conv2d_13/kernel/Initializer/truncated_normal/mean*.
_class$
" loc:@base_model/conv2d_13/kernel*(
_output_shapes
:*
T0
Ě
base_model/conv2d_13/kernelVarHandleOp*,
shared_namebase_model/conv2d_13/kernel*
dtype0*
shape:*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_13/kernel

<base_model/conv2d_13/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_13/kernel*
_output_shapes
: 

"base_model/conv2d_13/kernel/AssignAssignVariableOpbase_model/conv2d_13/kernel8base_model/conv2d_13/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_13/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_13/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_13/dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      

*base_model/conv2d_13/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_13/kernel*(
_output_shapes
:*
dtype0
Ů
base_model/conv2d_13/Conv2DConv2Dbase_model/Relu_9*base_model/conv2d_13/Conv2D/ReadVariableOp*
strides
*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingSAME
Ä
9base_model/batch_normalization_13/gamma/Initializer/zerosConst*
dtype0*:
_class0
.,loc:@base_model/batch_normalization_13/gamma*
valueB*    *
_output_shapes	
:
ă
'base_model/batch_normalization_13/gammaVarHandleOp*:
_class0
.,loc:@base_model/batch_normalization_13/gamma*
shape:*
_output_shapes
: *
dtype0*8
shared_name)'base_model/batch_normalization_13/gamma

Hbase_model/batch_normalization_13/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_13/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_13/gamma/AssignAssignVariableOp'base_model/batch_normalization_13/gamma9base_model/batch_normalization_13/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_13/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_13/gamma*
dtype0*
_output_shapes	
:
Â
8base_model/batch_normalization_13/beta/Initializer/zerosConst*
dtype0*
_output_shapes	
:*9
_class/
-+loc:@base_model/batch_normalization_13/beta*
valueB*    
ŕ
&base_model/batch_normalization_13/betaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_13/beta*7
shared_name(&base_model/batch_normalization_13/beta*
dtype0*
shape:*
_output_shapes
: 

Gbase_model/batch_normalization_13/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_13/beta*
_output_shapes
: 
°
-base_model/batch_normalization_13/beta/AssignAssignVariableOp&base_model/batch_normalization_13/beta8base_model/batch_normalization_13/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_13/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_13/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_13/moving_mean/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@base_model/batch_normalization_13/moving_mean*
_output_shapes	
:*
dtype0
ő
-base_model/batch_normalization_13/moving_meanVarHandleOp*
dtype0*
shape:*
_output_shapes
: *@
_class6
42loc:@base_model/batch_normalization_13/moving_mean*>
shared_name/-base_model/batch_normalization_13/moving_mean
Ť
Nbase_model/batch_normalization_13/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_13/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_13/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_13/moving_mean?base_model/batch_normalization_13/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_13/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_13/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_13/moving_variance/Initializer/onesConst*
_output_shapes	
:*
valueB*  ?*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance*
dtype0

1base_model/batch_normalization_13/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_13/moving_variance*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance*
shape:*
dtype0*
_output_shapes
: 
ł
Rbase_model/batch_normalization_13/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_13/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_13/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_13/moving_varianceBbase_model/batch_normalization_13/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_13/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_13/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_13/ReadVariableOpReadVariableOp'base_model/batch_normalization_13/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_13/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_13/beta*
dtype0*
_output_shapes	
:
Ź
Abase_model/batch_normalization_13/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_13/moving_mean*
dtype0*
_output_shapes	
:
˛
Cbase_model/batch_normalization_13/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_13/moving_variance*
dtype0*
_output_shapes	
:
ě
2base_model/batch_normalization_13/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_13/Conv2D0base_model/batch_normalization_13/ReadVariableOp2base_model/batch_normalization_13/ReadVariableOp_1Abase_model/batch_normalization_13/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_13/FusedBatchNormV3/ReadVariableOp_1*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
U0*
T0*
epsilon%đ'7*
is_training( 
l
'base_model/batch_normalization_13/ConstConst*
dtype0*
valueB
 *fff?*
_output_shapes
: 
Î
base_model/add_4AddV22base_model/batch_normalization_13/FusedBatchNormV32base_model/batch_normalization_11/FusedBatchNormV3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
y
base_model/Relu_10Relubase_model/add_4*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_14/kernel/Initializer/truncated_normal/shapeConst*
dtype0*
_output_shapes
:*%
valueB"            *.
_class$
" loc:@base_model/conv2d_14/kernel
˛
=base_model/conv2d_14/kernel/Initializer/truncated_normal/meanConst*.
_class$
" loc:@base_model/conv2d_14/kernel*
dtype0*
valueB
 *    *
_output_shapes
: 
´
?base_model/conv2d_14/kernel/Initializer/truncated_normal/stddevConst*
dtype0*.
_class$
" loc:@base_model/conv2d_14/kernel*
valueB
 *Â<*
_output_shapes
: 

Hbase_model/conv2d_14/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_14/kernel/Initializer/truncated_normal/shape*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_14/kernel*
dtype0
ą
<base_model/conv2d_14/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_14/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_14/kernel/Initializer/truncated_normal/stddev*
T0*.
_class$
" loc:@base_model/conv2d_14/kernel*(
_output_shapes
:

8base_model/conv2d_14/kernel/Initializer/truncated_normalAdd<base_model/conv2d_14/kernel/Initializer/truncated_normal/mul=base_model/conv2d_14/kernel/Initializer/truncated_normal/mean*.
_class$
" loc:@base_model/conv2d_14/kernel*
T0*(
_output_shapes
:
Ě
base_model/conv2d_14/kernelVarHandleOp*
shape:*.
_class$
" loc:@base_model/conv2d_14/kernel*
_output_shapes
: *
dtype0*,
shared_namebase_model/conv2d_14/kernel

<base_model/conv2d_14/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_14/kernel*
_output_shapes
: 

"base_model/conv2d_14/kernel/AssignAssignVariableOpbase_model/conv2d_14/kernel8base_model/conv2d_14/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_14/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_14/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_14/dilation_rateConst*
dtype0*
valueB"      *
_output_shapes
:

*base_model/conv2d_14/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_14/kernel*(
_output_shapes
:*
dtype0
Ú
base_model/conv2d_14/Conv2DConv2Dbase_model/Relu_10*base_model/conv2d_14/Conv2D/ReadVariableOp*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingSAME*
T0
Ă
8base_model/batch_normalization_14/gamma/Initializer/onesConst*
_output_shapes	
:*:
_class0
.,loc:@base_model/batch_normalization_14/gamma*
valueB*  ?*
dtype0
ă
'base_model/batch_normalization_14/gammaVarHandleOp*8
shared_name)'base_model/batch_normalization_14/gamma*
_output_shapes
: *
shape:*:
_class0
.,loc:@base_model/batch_normalization_14/gamma*
dtype0

Hbase_model/batch_normalization_14/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_14/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_14/gamma/AssignAssignVariableOp'base_model/batch_normalization_14/gamma8base_model/batch_normalization_14/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_14/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_14/gamma*
dtype0*
_output_shapes	
:
Â
8base_model/batch_normalization_14/beta/Initializer/zerosConst*9
_class/
-+loc:@base_model/batch_normalization_14/beta*
dtype0*
valueB*    *
_output_shapes	
:
ŕ
&base_model/batch_normalization_14/betaVarHandleOp*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_14/beta*7
shared_name(&base_model/batch_normalization_14/beta*
_output_shapes
: *
shape:

Gbase_model/batch_normalization_14/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_14/beta*
_output_shapes
: 
°
-base_model/batch_normalization_14/beta/AssignAssignVariableOp&base_model/batch_normalization_14/beta8base_model/batch_normalization_14/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_14/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_14/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_14/moving_mean/Initializer/zerosConst*
_output_shapes	
:*
dtype0*
valueB*    *@
_class6
42loc:@base_model/batch_normalization_14/moving_mean
ő
-base_model/batch_normalization_14/moving_meanVarHandleOp*@
_class6
42loc:@base_model/batch_normalization_14/moving_mean*
dtype0*>
shared_name/-base_model/batch_normalization_14/moving_mean*
shape:*
_output_shapes
: 
Ť
Nbase_model/batch_normalization_14/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_14/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_14/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_14/moving_mean?base_model/batch_normalization_14/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_14/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_14/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_14/moving_variance/Initializer/onesConst*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
dtype0*
valueB*  ?*
_output_shapes	
:

1base_model/batch_normalization_14/moving_varianceVarHandleOp*
shape:*
_output_shapes
: *B
shared_name31base_model/batch_normalization_14/moving_variance*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
dtype0
ł
Rbase_model/batch_normalization_14/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_14/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_14/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_14/moving_varianceBbase_model/batch_normalization_14/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_14/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_14/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_14/ReadVariableOpReadVariableOp'base_model/batch_normalization_14/gamma*
_output_shapes	
:*
dtype0

2base_model/batch_normalization_14/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_14/beta*
_output_shapes	
:*
dtype0
Ź
Abase_model/batch_normalization_14/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_14/moving_mean*
dtype0*
_output_shapes	
:
˛
Cbase_model/batch_normalization_14/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_14/moving_variance*
_output_shapes	
:*
dtype0
ě
2base_model/batch_normalization_14/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_14/Conv2D0base_model/batch_normalization_14/ReadVariableOp2base_model/batch_normalization_14/ReadVariableOp_1Abase_model/batch_normalization_14/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_14/FusedBatchNormV3/ReadVariableOp_1*
T0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
is_training( *
U0*
epsilon%đ'7
l
'base_model/batch_normalization_14/ConstConst*
valueB
 *fff?*
dtype0*
_output_shapes
: 

base_model/Relu_11Relu2base_model/batch_normalization_14/FusedBatchNormV3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_15/kernel/Initializer/truncated_normal/shapeConst*
_output_shapes
:*%
valueB"            *.
_class$
" loc:@base_model/conv2d_15/kernel*
dtype0
˛
=base_model/conv2d_15/kernel/Initializer/truncated_normal/meanConst*
dtype0*
valueB
 *    *.
_class$
" loc:@base_model/conv2d_15/kernel*
_output_shapes
: 
´
?base_model/conv2d_15/kernel/Initializer/truncated_normal/stddevConst*
dtype0*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_15/kernel*
valueB
 *Â<

Hbase_model/conv2d_15/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_15/kernel/Initializer/truncated_normal/shape*.
_class$
" loc:@base_model/conv2d_15/kernel*
dtype0*
T0*(
_output_shapes
:
ą
<base_model/conv2d_15/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_15/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_15/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_15/kernel*
T0*(
_output_shapes
:

8base_model/conv2d_15/kernel/Initializer/truncated_normalAdd<base_model/conv2d_15/kernel/Initializer/truncated_normal/mul=base_model/conv2d_15/kernel/Initializer/truncated_normal/mean*(
_output_shapes
:*
T0*.
_class$
" loc:@base_model/conv2d_15/kernel
Ě
base_model/conv2d_15/kernelVarHandleOp*.
_class$
" loc:@base_model/conv2d_15/kernel*
_output_shapes
: *,
shared_namebase_model/conv2d_15/kernel*
dtype0*
shape:

<base_model/conv2d_15/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_15/kernel*
_output_shapes
: 

"base_model/conv2d_15/kernel/AssignAssignVariableOpbase_model/conv2d_15/kernel8base_model/conv2d_15/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_15/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_15/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_15/dilation_rateConst*
_output_shapes
:*
valueB"      *
dtype0

*base_model/conv2d_15/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_15/kernel*(
_output_shapes
:*
dtype0
Ú
base_model/conv2d_15/Conv2DConv2Dbase_model/Relu_11*base_model/conv2d_15/Conv2D/ReadVariableOp*
paddingSAME*
strides
*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ä
9base_model/batch_normalization_15/gamma/Initializer/zerosConst*
dtype0*
_output_shapes	
:*:
_class0
.,loc:@base_model/batch_normalization_15/gamma*
valueB*    
ă
'base_model/batch_normalization_15/gammaVarHandleOp*
dtype0*:
_class0
.,loc:@base_model/batch_normalization_15/gamma*
_output_shapes
: *
shape:*8
shared_name)'base_model/batch_normalization_15/gamma

Hbase_model/batch_normalization_15/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_15/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_15/gamma/AssignAssignVariableOp'base_model/batch_normalization_15/gamma9base_model/batch_normalization_15/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_15/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_15/gamma*
dtype0*
_output_shapes	
:
Â
8base_model/batch_normalization_15/beta/Initializer/zerosConst*9
_class/
-+loc:@base_model/batch_normalization_15/beta*
valueB*    *
_output_shapes	
:*
dtype0
ŕ
&base_model/batch_normalization_15/betaVarHandleOp*
_output_shapes
: *7
shared_name(&base_model/batch_normalization_15/beta*
shape:*9
_class/
-+loc:@base_model/batch_normalization_15/beta*
dtype0

Gbase_model/batch_normalization_15/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_15/beta*
_output_shapes
: 
°
-base_model/batch_normalization_15/beta/AssignAssignVariableOp&base_model/batch_normalization_15/beta8base_model/batch_normalization_15/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_15/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_15/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_15/moving_mean/Initializer/zerosConst*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean*
valueB*    *
dtype0*
_output_shapes	
:
ő
-base_model/batch_normalization_15/moving_meanVarHandleOp*
dtype0*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean*
_output_shapes
: *
shape:*>
shared_name/-base_model/batch_normalization_15/moving_mean
Ť
Nbase_model/batch_normalization_15/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_15/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_15/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_15/moving_mean?base_model/batch_normalization_15/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_15/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_15/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_15/moving_variance/Initializer/onesConst*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_15/moving_variance*
dtype0*
valueB*  ?

1base_model/batch_normalization_15/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_15/moving_variance*D
_class:
86loc:@base_model/batch_normalization_15/moving_variance*
shape:*
_output_shapes
: *
dtype0
ł
Rbase_model/batch_normalization_15/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_15/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_15/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_15/moving_varianceBbase_model/batch_normalization_15/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_15/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_15/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_15/ReadVariableOpReadVariableOp'base_model/batch_normalization_15/gamma*
_output_shapes	
:*
dtype0

2base_model/batch_normalization_15/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_15/beta*
_output_shapes	
:*
dtype0
Ź
Abase_model/batch_normalization_15/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_15/moving_mean*
dtype0*
_output_shapes	
:
˛
Cbase_model/batch_normalization_15/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_15/moving_variance*
_output_shapes	
:*
dtype0
ě
2base_model/batch_normalization_15/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_15/Conv2D0base_model/batch_normalization_15/ReadVariableOp2base_model/batch_normalization_15/ReadVariableOp_1Abase_model/batch_normalization_15/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_15/FusedBatchNormV3/ReadVariableOp_1*
U0*
T0*
is_training( *
epsilon%đ'7*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
l
'base_model/batch_normalization_15/ConstConst*
_output_shapes
: *
valueB
 *fff?*
dtype0
Ž
base_model/add_5AddV22base_model/batch_normalization_15/FusedBatchNormV3base_model/Relu_10*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
y
base_model/Relu_12Relubase_model/add_5*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0

base_model/block_group3Identitybase_model/Relu_12*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

base_model/Pad_5/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                                 

base_model/Pad_5Padbase_model/block_group3base_model/Pad_5/paddings*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ç
>base_model/conv2d_16/kernel/Initializer/truncated_normal/shapeConst*
dtype0*%
valueB"            *
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_16/kernel
˛
=base_model/conv2d_16/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
dtype0*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_16/kernel
´
?base_model/conv2d_16/kernel/Initializer/truncated_normal/stddevConst*.
_class$
" loc:@base_model/conv2d_16/kernel*
valueB
 *6=*
dtype0*
_output_shapes
: 

Hbase_model/conv2d_16/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_16/kernel/Initializer/truncated_normal/shape*
T0*(
_output_shapes
:*
dtype0*.
_class$
" loc:@base_model/conv2d_16/kernel
ą
<base_model/conv2d_16/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_16/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_16/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_16/kernel*(
_output_shapes
:*
T0

8base_model/conv2d_16/kernel/Initializer/truncated_normalAdd<base_model/conv2d_16/kernel/Initializer/truncated_normal/mul=base_model/conv2d_16/kernel/Initializer/truncated_normal/mean*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_16/kernel*
T0
Ě
base_model/conv2d_16/kernelVarHandleOp*,
shared_namebase_model/conv2d_16/kernel*.
_class$
" loc:@base_model/conv2d_16/kernel*
shape:*
dtype0*
_output_shapes
: 

<base_model/conv2d_16/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_16/kernel*
_output_shapes
: 

"base_model/conv2d_16/kernel/AssignAssignVariableOpbase_model/conv2d_16/kernel8base_model/conv2d_16/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_16/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_16/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_16/dilation_rateConst*
dtype0*
valueB"      *
_output_shapes
:

*base_model/conv2d_16/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_16/kernel*
dtype0*(
_output_shapes
:
Ů
base_model/conv2d_16/Conv2DConv2Dbase_model/Pad_5*base_model/conv2d_16/Conv2D/ReadVariableOp*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingVALID*
strides

Ă
8base_model/batch_normalization_16/gamma/Initializer/onesConst*
valueB*  ?*
dtype0*:
_class0
.,loc:@base_model/batch_normalization_16/gamma*
_output_shapes	
:
ă
'base_model/batch_normalization_16/gammaVarHandleOp*
shape:*8
shared_name)'base_model/batch_normalization_16/gamma*:
_class0
.,loc:@base_model/batch_normalization_16/gamma*
dtype0*
_output_shapes
: 

Hbase_model/batch_normalization_16/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_16/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_16/gamma/AssignAssignVariableOp'base_model/batch_normalization_16/gamma8base_model/batch_normalization_16/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_16/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_16/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_16/beta/Initializer/zerosConst*
_output_shapes	
:*
valueB*    *
dtype0*9
_class/
-+loc:@base_model/batch_normalization_16/beta
ŕ
&base_model/batch_normalization_16/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:*9
_class/
-+loc:@base_model/batch_normalization_16/beta*7
shared_name(&base_model/batch_normalization_16/beta

Gbase_model/batch_normalization_16/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_16/beta*
_output_shapes
: 
°
-base_model/batch_normalization_16/beta/AssignAssignVariableOp&base_model/batch_normalization_16/beta8base_model/batch_normalization_16/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_16/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_16/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_16/moving_mean/Initializer/zerosConst*
valueB*    *
dtype0*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_16/moving_mean
ő
-base_model/batch_normalization_16/moving_meanVarHandleOp*@
_class6
42loc:@base_model/batch_normalization_16/moving_mean*
dtype0*
shape:*
_output_shapes
: *>
shared_name/-base_model/batch_normalization_16/moving_mean
Ť
Nbase_model/batch_normalization_16/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_16/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_16/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_16/moving_mean?base_model/batch_normalization_16/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_16/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_16/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_16/moving_variance/Initializer/onesConst*D
_class:
86loc:@base_model/batch_normalization_16/moving_variance*
valueB*  ?*
_output_shapes	
:*
dtype0

1base_model/batch_normalization_16/moving_varianceVarHandleOp*
dtype0*
shape:*
_output_shapes
: *D
_class:
86loc:@base_model/batch_normalization_16/moving_variance*B
shared_name31base_model/batch_normalization_16/moving_variance
ł
Rbase_model/batch_normalization_16/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_16/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_16/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_16/moving_varianceBbase_model/batch_normalization_16/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_16/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_16/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_16/ReadVariableOpReadVariableOp'base_model/batch_normalization_16/gamma*
_output_shapes	
:*
dtype0

2base_model/batch_normalization_16/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_16/beta*
_output_shapes	
:*
dtype0
Ź
Abase_model/batch_normalization_16/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_16/moving_mean*
dtype0*
_output_shapes	
:
˛
Cbase_model/batch_normalization_16/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_16/moving_variance*
_output_shapes	
:*
dtype0
ě
2base_model/batch_normalization_16/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_16/Conv2D0base_model/batch_normalization_16/ReadVariableOp2base_model/batch_normalization_16/ReadVariableOp_1Abase_model/batch_normalization_16/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_16/FusedBatchNormV3/ReadVariableOp_1*
U0*
T0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
is_training( *
epsilon%đ'7
l
'base_model/batch_normalization_16/ConstConst*
valueB
 *fff?*
_output_shapes
: *
dtype0

base_model/Pad_6/paddingsConst*9
value0B."                             *
dtype0*
_output_shapes

:

base_model/Pad_6Padbase_model/block_group3base_model/Pad_6/paddings*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ç
>base_model/conv2d_17/kernel/Initializer/truncated_normal/shapeConst*%
valueB"            *
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_17/kernel*
dtype0
˛
=base_model/conv2d_17/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *.
_class$
" loc:@base_model/conv2d_17/kernel*
_output_shapes
: *
dtype0
´
?base_model/conv2d_17/kernel/Initializer/truncated_normal/stddevConst*
valueB
 *Â<*
_output_shapes
: *
dtype0*.
_class$
" loc:@base_model/conv2d_17/kernel

Hbase_model/conv2d_17/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_17/kernel/Initializer/truncated_normal/shape*
dtype0*.
_class$
" loc:@base_model/conv2d_17/kernel*(
_output_shapes
:*
T0
ą
<base_model/conv2d_17/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_17/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_17/kernel/Initializer/truncated_normal/stddev*
T0*.
_class$
" loc:@base_model/conv2d_17/kernel*(
_output_shapes
:

8base_model/conv2d_17/kernel/Initializer/truncated_normalAdd<base_model/conv2d_17/kernel/Initializer/truncated_normal/mul=base_model/conv2d_17/kernel/Initializer/truncated_normal/mean*
T0*.
_class$
" loc:@base_model/conv2d_17/kernel*(
_output_shapes
:
Ě
base_model/conv2d_17/kernelVarHandleOp*
dtype0*
shape:*.
_class$
" loc:@base_model/conv2d_17/kernel*,
shared_namebase_model/conv2d_17/kernel*
_output_shapes
: 

<base_model/conv2d_17/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_17/kernel*
_output_shapes
: 

"base_model/conv2d_17/kernel/AssignAssignVariableOpbase_model/conv2d_17/kernel8base_model/conv2d_17/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_17/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_17/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_17/dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      

*base_model/conv2d_17/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_17/kernel*
dtype0*(
_output_shapes
:
Ů
base_model/conv2d_17/Conv2DConv2Dbase_model/Pad_6*base_model/conv2d_17/Conv2D/ReadVariableOp*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingVALID*
T0
Ă
8base_model/batch_normalization_17/gamma/Initializer/onesConst*
_output_shapes	
:*
dtype0*
valueB*  ?*:
_class0
.,loc:@base_model/batch_normalization_17/gamma
ă
'base_model/batch_normalization_17/gammaVarHandleOp*:
_class0
.,loc:@base_model/batch_normalization_17/gamma*8
shared_name)'base_model/batch_normalization_17/gamma*
dtype0*
shape:*
_output_shapes
: 

Hbase_model/batch_normalization_17/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_17/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_17/gamma/AssignAssignVariableOp'base_model/batch_normalization_17/gamma8base_model/batch_normalization_17/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_17/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_17/gamma*
dtype0*
_output_shapes	
:
Â
8base_model/batch_normalization_17/beta/Initializer/zerosConst*
dtype0*
valueB*    *
_output_shapes	
:*9
_class/
-+loc:@base_model/batch_normalization_17/beta
ŕ
&base_model/batch_normalization_17/betaVarHandleOp*7
shared_name(&base_model/batch_normalization_17/beta*
shape:*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_17/beta*
_output_shapes
: 

Gbase_model/batch_normalization_17/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_17/beta*
_output_shapes
: 
°
-base_model/batch_normalization_17/beta/AssignAssignVariableOp&base_model/batch_normalization_17/beta8base_model/batch_normalization_17/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_17/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_17/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_17/moving_mean/Initializer/zerosConst*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_17/moving_mean*
dtype0*
valueB*    
ő
-base_model/batch_normalization_17/moving_meanVarHandleOp*>
shared_name/-base_model/batch_normalization_17/moving_mean*@
_class6
42loc:@base_model/batch_normalization_17/moving_mean*
shape:*
_output_shapes
: *
dtype0
Ť
Nbase_model/batch_normalization_17/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_17/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_17/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_17/moving_mean?base_model/batch_normalization_17/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_17/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_17/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_17/moving_variance/Initializer/onesConst*
dtype0*D
_class:
86loc:@base_model/batch_normalization_17/moving_variance*
valueB*  ?*
_output_shapes	
:

1base_model/batch_normalization_17/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_17/moving_variance*
_output_shapes
: *
dtype0*
shape:*D
_class:
86loc:@base_model/batch_normalization_17/moving_variance
ł
Rbase_model/batch_normalization_17/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_17/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_17/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_17/moving_varianceBbase_model/batch_normalization_17/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_17/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_17/moving_variance*
_output_shapes	
:*
dtype0

0base_model/batch_normalization_17/ReadVariableOpReadVariableOp'base_model/batch_normalization_17/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_17/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_17/beta*
dtype0*
_output_shapes	
:
Ź
Abase_model/batch_normalization_17/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_17/moving_mean*
_output_shapes	
:*
dtype0
˛
Cbase_model/batch_normalization_17/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_17/moving_variance*
_output_shapes	
:*
dtype0
ě
2base_model/batch_normalization_17/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_17/Conv2D0base_model/batch_normalization_17/ReadVariableOp2base_model/batch_normalization_17/ReadVariableOp_1Abase_model/batch_normalization_17/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_17/FusedBatchNormV3/ReadVariableOp_1*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
T0*
epsilon%đ'7*
is_training( *
U0
l
'base_model/batch_normalization_17/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *fff?

base_model/Relu_13Relu2base_model/batch_normalization_17/FusedBatchNormV3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_18/kernel/Initializer/truncated_normal/shapeConst*.
_class$
" loc:@base_model/conv2d_18/kernel*
_output_shapes
:*%
valueB"            *
dtype0
˛
=base_model/conv2d_18/kernel/Initializer/truncated_normal/meanConst*
dtype0*
_output_shapes
: *
valueB
 *    *.
_class$
" loc:@base_model/conv2d_18/kernel
´
?base_model/conv2d_18/kernel/Initializer/truncated_normal/stddevConst*
valueB
 *¸1<*.
_class$
" loc:@base_model/conv2d_18/kernel*
dtype0*
_output_shapes
: 

Hbase_model/conv2d_18/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_18/kernel/Initializer/truncated_normal/shape*
dtype0*
T0*.
_class$
" loc:@base_model/conv2d_18/kernel*(
_output_shapes
:
ą
<base_model/conv2d_18/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_18/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_18/kernel/Initializer/truncated_normal/stddev*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_18/kernel

8base_model/conv2d_18/kernel/Initializer/truncated_normalAdd<base_model/conv2d_18/kernel/Initializer/truncated_normal/mul=base_model/conv2d_18/kernel/Initializer/truncated_normal/mean*
T0*.
_class$
" loc:@base_model/conv2d_18/kernel*(
_output_shapes
:
Ě
base_model/conv2d_18/kernelVarHandleOp*
dtype0*
shape:*.
_class$
" loc:@base_model/conv2d_18/kernel*
_output_shapes
: *,
shared_namebase_model/conv2d_18/kernel

<base_model/conv2d_18/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_18/kernel*
_output_shapes
: 

"base_model/conv2d_18/kernel/AssignAssignVariableOpbase_model/conv2d_18/kernel8base_model/conv2d_18/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_18/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_18/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_18/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB"      

*base_model/conv2d_18/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_18/kernel*(
_output_shapes
:*
dtype0
Ú
base_model/conv2d_18/Conv2DConv2Dbase_model/Relu_13*base_model/conv2d_18/Conv2D/ReadVariableOp*
T0*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingSAME
Ä
9base_model/batch_normalization_18/gamma/Initializer/zerosConst*:
_class0
.,loc:@base_model/batch_normalization_18/gamma*
valueB*    *
dtype0*
_output_shapes	
:
ă
'base_model/batch_normalization_18/gammaVarHandleOp*8
shared_name)'base_model/batch_normalization_18/gamma*
_output_shapes
: *
shape:*:
_class0
.,loc:@base_model/batch_normalization_18/gamma*
dtype0

Hbase_model/batch_normalization_18/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_18/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_18/gamma/AssignAssignVariableOp'base_model/batch_normalization_18/gamma9base_model/batch_normalization_18/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_18/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_18/gamma*
dtype0*
_output_shapes	
:
Â
8base_model/batch_normalization_18/beta/Initializer/zerosConst*
valueB*    *
_output_shapes	
:*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_18/beta
ŕ
&base_model/batch_normalization_18/betaVarHandleOp*
_output_shapes
: *
dtype0*
shape:*9
_class/
-+loc:@base_model/batch_normalization_18/beta*7
shared_name(&base_model/batch_normalization_18/beta

Gbase_model/batch_normalization_18/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_18/beta*
_output_shapes
: 
°
-base_model/batch_normalization_18/beta/AssignAssignVariableOp&base_model/batch_normalization_18/beta8base_model/batch_normalization_18/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_18/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_18/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_18/moving_mean/Initializer/zerosConst*@
_class6
42loc:@base_model/batch_normalization_18/moving_mean*
valueB*    *
dtype0*
_output_shapes	
:
ő
-base_model/batch_normalization_18/moving_meanVarHandleOp*
dtype0*
shape:*@
_class6
42loc:@base_model/batch_normalization_18/moving_mean*>
shared_name/-base_model/batch_normalization_18/moving_mean*
_output_shapes
: 
Ť
Nbase_model/batch_normalization_18/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_18/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_18/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_18/moving_mean?base_model/batch_normalization_18/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_18/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_18/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_18/moving_variance/Initializer/onesConst*D
_class:
86loc:@base_model/batch_normalization_18/moving_variance*
valueB*  ?*
_output_shapes	
:*
dtype0

1base_model/batch_normalization_18/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_18/moving_variance*
_output_shapes
: *
shape:*D
_class:
86loc:@base_model/batch_normalization_18/moving_variance*
dtype0
ł
Rbase_model/batch_normalization_18/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_18/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_18/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_18/moving_varianceBbase_model/batch_normalization_18/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_18/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_18/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_18/ReadVariableOpReadVariableOp'base_model/batch_normalization_18/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_18/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_18/beta*
dtype0*
_output_shapes	
:
Ź
Abase_model/batch_normalization_18/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_18/moving_mean*
dtype0*
_output_shapes	
:
˛
Cbase_model/batch_normalization_18/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_18/moving_variance*
dtype0*
_output_shapes	
:
ě
2base_model/batch_normalization_18/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_18/Conv2D0base_model/batch_normalization_18/ReadVariableOp2base_model/batch_normalization_18/ReadVariableOp_1Abase_model/batch_normalization_18/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_18/FusedBatchNormV3/ReadVariableOp_1*
epsilon%đ'7*
U0*
is_training( *
T0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
l
'base_model/batch_normalization_18/ConstConst*
_output_shapes
: *
valueB
 *fff?*
dtype0
Î
base_model/add_6AddV22base_model/batch_normalization_18/FusedBatchNormV32base_model/batch_normalization_16/FusedBatchNormV3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
y
base_model/Relu_14Relubase_model/add_6*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_19/kernel/Initializer/truncated_normal/shapeConst*
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_19/kernel*
dtype0*%
valueB"            
˛
=base_model/conv2d_19/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *
valueB
 *    *
dtype0*.
_class$
" loc:@base_model/conv2d_19/kernel
´
?base_model/conv2d_19/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
valueB
 *¸1<*
dtype0*.
_class$
" loc:@base_model/conv2d_19/kernel

Hbase_model/conv2d_19/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_19/kernel/Initializer/truncated_normal/shape*.
_class$
" loc:@base_model/conv2d_19/kernel*
dtype0*(
_output_shapes
:*
T0
ą
<base_model/conv2d_19/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_19/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_19/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_19/kernel*
T0*(
_output_shapes
:

8base_model/conv2d_19/kernel/Initializer/truncated_normalAdd<base_model/conv2d_19/kernel/Initializer/truncated_normal/mul=base_model/conv2d_19/kernel/Initializer/truncated_normal/mean*.
_class$
" loc:@base_model/conv2d_19/kernel*
T0*(
_output_shapes
:
Ě
base_model/conv2d_19/kernelVarHandleOp*
dtype0*
shape:*,
shared_namebase_model/conv2d_19/kernel*.
_class$
" loc:@base_model/conv2d_19/kernel*
_output_shapes
: 

<base_model/conv2d_19/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_19/kernel*
_output_shapes
: 

"base_model/conv2d_19/kernel/AssignAssignVariableOpbase_model/conv2d_19/kernel8base_model/conv2d_19/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_19/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_19/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_19/dilation_rateConst*
dtype0*
valueB"      *
_output_shapes
:

*base_model/conv2d_19/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_19/kernel*(
_output_shapes
:*
dtype0
Ú
base_model/conv2d_19/Conv2DConv2Dbase_model/Relu_14*base_model/conv2d_19/Conv2D/ReadVariableOp*
T0*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingSAME
Ă
8base_model/batch_normalization_19/gamma/Initializer/onesConst*
dtype0*:
_class0
.,loc:@base_model/batch_normalization_19/gamma*
valueB*  ?*
_output_shapes	
:
ă
'base_model/batch_normalization_19/gammaVarHandleOp*
shape:*
dtype0*
_output_shapes
: *:
_class0
.,loc:@base_model/batch_normalization_19/gamma*8
shared_name)'base_model/batch_normalization_19/gamma

Hbase_model/batch_normalization_19/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_19/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_19/gamma/AssignAssignVariableOp'base_model/batch_normalization_19/gamma8base_model/batch_normalization_19/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_19/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_19/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_19/beta/Initializer/zerosConst*
_output_shapes	
:*9
_class/
-+loc:@base_model/batch_normalization_19/beta*
valueB*    *
dtype0
ŕ
&base_model/batch_normalization_19/betaVarHandleOp*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_19/beta*7
shared_name(&base_model/batch_normalization_19/beta*
_output_shapes
: *
shape:

Gbase_model/batch_normalization_19/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_19/beta*
_output_shapes
: 
°
-base_model/batch_normalization_19/beta/AssignAssignVariableOp&base_model/batch_normalization_19/beta8base_model/batch_normalization_19/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_19/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_19/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_19/moving_mean/Initializer/zerosConst*
dtype0*
valueB*    *@
_class6
42loc:@base_model/batch_normalization_19/moving_mean*
_output_shapes	
:
ő
-base_model/batch_normalization_19/moving_meanVarHandleOp*>
shared_name/-base_model/batch_normalization_19/moving_mean*
_output_shapes
: *
shape:*@
_class6
42loc:@base_model/batch_normalization_19/moving_mean*
dtype0
Ť
Nbase_model/batch_normalization_19/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_19/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_19/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_19/moving_mean?base_model/batch_normalization_19/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_19/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_19/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_19/moving_variance/Initializer/onesConst*
_output_shapes	
:*
dtype0*D
_class:
86loc:@base_model/batch_normalization_19/moving_variance*
valueB*  ?

1base_model/batch_normalization_19/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_19/moving_variance*D
_class:
86loc:@base_model/batch_normalization_19/moving_variance*
dtype0*
shape:*
_output_shapes
: 
ł
Rbase_model/batch_normalization_19/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_19/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_19/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_19/moving_varianceBbase_model/batch_normalization_19/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_19/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_19/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_19/ReadVariableOpReadVariableOp'base_model/batch_normalization_19/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_19/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_19/beta*
_output_shapes	
:*
dtype0
Ź
Abase_model/batch_normalization_19/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_19/moving_mean*
_output_shapes	
:*
dtype0
˛
Cbase_model/batch_normalization_19/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_19/moving_variance*
dtype0*
_output_shapes	
:
ě
2base_model/batch_normalization_19/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_19/Conv2D0base_model/batch_normalization_19/ReadVariableOp2base_model/batch_normalization_19/ReadVariableOp_1Abase_model/batch_normalization_19/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_19/FusedBatchNormV3/ReadVariableOp_1*
epsilon%đ'7*
is_training( *
U0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
T0
l
'base_model/batch_normalization_19/ConstConst*
_output_shapes
: *
valueB
 *fff?*
dtype0

base_model/Relu_15Relu2base_model/batch_normalization_19/FusedBatchNormV3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ç
>base_model/conv2d_20/kernel/Initializer/truncated_normal/shapeConst*%
valueB"            *.
_class$
" loc:@base_model/conv2d_20/kernel*
_output_shapes
:*
dtype0
˛
=base_model/conv2d_20/kernel/Initializer/truncated_normal/meanConst*
dtype0*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_20/kernel*
valueB
 *    
´
?base_model/conv2d_20/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *¸1<*.
_class$
" loc:@base_model/conv2d_20/kernel

Hbase_model/conv2d_20/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_20/kernel/Initializer/truncated_normal/shape*.
_class$
" loc:@base_model/conv2d_20/kernel*(
_output_shapes
:*
dtype0*
T0
ą
<base_model/conv2d_20/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_20/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_20/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_20/kernel*(
_output_shapes
:*
T0

8base_model/conv2d_20/kernel/Initializer/truncated_normalAdd<base_model/conv2d_20/kernel/Initializer/truncated_normal/mul=base_model/conv2d_20/kernel/Initializer/truncated_normal/mean*(
_output_shapes
:*
T0*.
_class$
" loc:@base_model/conv2d_20/kernel
Ě
base_model/conv2d_20/kernelVarHandleOp*,
shared_namebase_model/conv2d_20/kernel*
dtype0*.
_class$
" loc:@base_model/conv2d_20/kernel*
shape:*
_output_shapes
: 

<base_model/conv2d_20/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_20/kernel*
_output_shapes
: 

"base_model/conv2d_20/kernel/AssignAssignVariableOpbase_model/conv2d_20/kernel8base_model/conv2d_20/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_20/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_20/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_20/dilation_rateConst*
valueB"      *
_output_shapes
:*
dtype0

*base_model/conv2d_20/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_20/kernel*
dtype0*(
_output_shapes
:
Ú
base_model/conv2d_20/Conv2DConv2Dbase_model/Relu_15*base_model/conv2d_20/Conv2D/ReadVariableOp*
T0*
paddingSAME*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
strides

Ä
9base_model/batch_normalization_20/gamma/Initializer/zerosConst*
dtype0*
valueB*    *:
_class0
.,loc:@base_model/batch_normalization_20/gamma*
_output_shapes	
:
ă
'base_model/batch_normalization_20/gammaVarHandleOp*
_output_shapes
: *8
shared_name)'base_model/batch_normalization_20/gamma*:
_class0
.,loc:@base_model/batch_normalization_20/gamma*
shape:*
dtype0

Hbase_model/batch_normalization_20/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_20/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_20/gamma/AssignAssignVariableOp'base_model/batch_normalization_20/gamma9base_model/batch_normalization_20/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_20/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_20/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_20/beta/Initializer/zerosConst*
dtype0*
_output_shapes	
:*9
_class/
-+loc:@base_model/batch_normalization_20/beta*
valueB*    
ŕ
&base_model/batch_normalization_20/betaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_20/beta*
dtype0*7
shared_name(&base_model/batch_normalization_20/beta*
_output_shapes
: *
shape:

Gbase_model/batch_normalization_20/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_20/beta*
_output_shapes
: 
°
-base_model/batch_normalization_20/beta/AssignAssignVariableOp&base_model/batch_normalization_20/beta8base_model/batch_normalization_20/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_20/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_20/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_20/moving_mean/Initializer/zerosConst*
_output_shapes	
:*
valueB*    *@
_class6
42loc:@base_model/batch_normalization_20/moving_mean*
dtype0
ő
-base_model/batch_normalization_20/moving_meanVarHandleOp*>
shared_name/-base_model/batch_normalization_20/moving_mean*
shape:*@
_class6
42loc:@base_model/batch_normalization_20/moving_mean*
_output_shapes
: *
dtype0
Ť
Nbase_model/batch_normalization_20/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_20/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_20/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_20/moving_mean?base_model/batch_normalization_20/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_20/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_20/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_20/moving_variance/Initializer/onesConst*
valueB*  ?*
dtype0*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance

1base_model/batch_normalization_20/moving_varianceVarHandleOp*
shape:*
_output_shapes
: *B
shared_name31base_model/batch_normalization_20/moving_variance*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance*
dtype0
ł
Rbase_model/batch_normalization_20/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_20/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_20/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_20/moving_varianceBbase_model/batch_normalization_20/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_20/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_20/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_20/ReadVariableOpReadVariableOp'base_model/batch_normalization_20/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_20/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_20/beta*
_output_shapes	
:*
dtype0
Ź
Abase_model/batch_normalization_20/FusedBatchNormV3/ReadVariableOpReadVariableOp-base_model/batch_normalization_20/moving_mean*
dtype0*
_output_shapes	
:
˛
Cbase_model/batch_normalization_20/FusedBatchNormV3/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_20/moving_variance*
_output_shapes	
:*
dtype0
ě
2base_model/batch_normalization_20/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_20/Conv2D0base_model/batch_normalization_20/ReadVariableOp2base_model/batch_normalization_20/ReadVariableOp_1Abase_model/batch_normalization_20/FusedBatchNormV3/ReadVariableOpCbase_model/batch_normalization_20/FusedBatchNormV3/ReadVariableOp_1*
T0*
is_training( *b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
epsilon%đ'7*
U0
l
'base_model/batch_normalization_20/ConstConst*
dtype0*
_output_shapes
: *
valueB
 *fff?
Ž
base_model/add_7AddV22base_model/batch_normalization_20/FusedBatchNormV3base_model/Relu_14*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
y
base_model/Relu_16Relubase_model/add_7*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

base_model/block_group4Identitybase_model/Relu_16*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

base_model/StopGradientStopGradientbase_model/block_group4*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
r
!base_model/Mean/reduction_indicesConst*
dtype0*
_output_shapes
:*
valueB"      

base_model/MeanMeanbase_model/StopGradient!base_model/Mean/reduction_indices*(
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0
i
base_model/final_avg_poolIdentitybase_model/Mean*
T0*(
_output_shapes
:˙˙˙˙˙˙˙˙˙
Ř
Ihead_supervised/linear_layer/dense/kernel/Initializer/random_normal/shapeConst*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
valueB"      *
dtype0*
_output_shapes
:
Ë
Hhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/meanConst*
valueB
 *    *<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
_output_shapes
: *
dtype0
Í
Jhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/stddevConst*
dtype0*
_output_shapes
: *
valueB
 *
×#<*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel
°
Xhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/RandomStandardNormalRandomStandardNormalIhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/shape*
_output_shapes
:	*
dtype0*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
T0
Ü
Ghead_supervised/linear_layer/dense/kernel/Initializer/random_normal/mulMulXhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/RandomStandardNormalJhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/stddev*
T0*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
_output_shapes
:	
Ĺ
Chead_supervised/linear_layer/dense/kernel/Initializer/random_normalAddGhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/mulHhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/mean*
T0*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
_output_shapes
:	
í
)head_supervised/linear_layer/dense/kernelVarHandleOp*
dtype0*
_output_shapes
: *<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
shape:	*:
shared_name+)head_supervised/linear_layer/dense/kernel
Ł
Jhead_supervised/linear_layer/dense/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOp)head_supervised/linear_layer/dense/kernel*
_output_shapes
: 
Á
0head_supervised/linear_layer/dense/kernel/AssignAssignVariableOp)head_supervised/linear_layer/dense/kernelChead_supervised/linear_layer/dense/kernel/Initializer/random_normal*
dtype0
¨
=head_supervised/linear_layer/dense/kernel/Read/ReadVariableOpReadVariableOp)head_supervised/linear_layer/dense/kernel*
_output_shapes
:	*
dtype0
Â
9head_supervised/linear_layer/dense/bias/Initializer/zerosConst*
dtype0*
valueB*    *:
_class0
.,loc:@head_supervised/linear_layer/dense/bias*
_output_shapes
:
â
'head_supervised/linear_layer/dense/biasVarHandleOp*8
shared_name)'head_supervised/linear_layer/dense/bias*
_output_shapes
: *
shape:*:
_class0
.,loc:@head_supervised/linear_layer/dense/bias*
dtype0

Hhead_supervised/linear_layer/dense/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOp'head_supervised/linear_layer/dense/bias*
_output_shapes
: 
ł
.head_supervised/linear_layer/dense/bias/AssignAssignVariableOp'head_supervised/linear_layer/dense/bias9head_supervised/linear_layer/dense/bias/Initializer/zeros*
dtype0

;head_supervised/linear_layer/dense/bias/Read/ReadVariableOpReadVariableOp'head_supervised/linear_layer/dense/bias*
dtype0*
_output_shapes
:
Ł
8head_supervised/linear_layer/dense/MatMul/ReadVariableOpReadVariableOp)head_supervised/linear_layer/dense/kernel*
dtype0*
_output_shapes
:	
ş
)head_supervised/linear_layer/dense/MatMulMatMulbase_model/final_avg_pool8head_supervised/linear_layer/dense/MatMul/ReadVariableOp*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0

9head_supervised/linear_layer/dense/BiasAdd/ReadVariableOpReadVariableOp'head_supervised/linear_layer/dense/bias*
_output_shapes
:*
dtype0
Í
*head_supervised/linear_layer/dense/BiasAddBiasAdd)head_supervised/linear_layer/dense/MatMul9head_supervised/linear_layer/dense/BiasAdd/ReadVariableOp*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0

-head_supervised/linear_layer/linear_layer_outIdentity*head_supervised/linear_layer/dense/BiasAdd*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0""ź
	variablesź˙ť
Ş
base_model/conv2d/kernel:0base_model/conv2d/kernel/Assign.base_model/conv2d/kernel/Read/ReadVariableOp:0(27base_model/conv2d/kernel/Initializer/truncated_normal:08
Î
&base_model/batch_normalization/gamma:0+base_model/batch_normalization/gamma/Assign:base_model/batch_normalization/gamma/Read/ReadVariableOp:0(27base_model/batch_normalization/gamma/Initializer/ones:08
Ë
%base_model/batch_normalization/beta:0*base_model/batch_normalization/beta/Assign9base_model/batch_normalization/beta/Read/ReadVariableOp:0(27base_model/batch_normalization/beta/Initializer/zeros:08
é
,base_model/batch_normalization/moving_mean:01base_model/batch_normalization/moving_mean/Assign@base_model/batch_normalization/moving_mean/Read/ReadVariableOp:0(2>base_model/batch_normalization/moving_mean/Initializer/zeros:0@H
ř
0base_model/batch_normalization/moving_variance:05base_model/batch_normalization/moving_variance/AssignDbase_model/batch_normalization/moving_variance/Read/ReadVariableOp:0(2Abase_model/batch_normalization/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_1/kernel:0!base_model/conv2d_1/kernel/Assign0base_model/conv2d_1/kernel/Read/ReadVariableOp:0(29base_model/conv2d_1/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_1/gamma:0-base_model/batch_normalization_1/gamma/Assign<base_model/batch_normalization_1/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_1/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_1/beta:0,base_model/batch_normalization_1/beta/Assign;base_model/batch_normalization_1/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_1/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_1/moving_mean:03base_model/batch_normalization_1/moving_mean/AssignBbase_model/batch_normalization_1/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_1/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_1/moving_variance:07base_model/batch_normalization_1/moving_variance/AssignFbase_model/batch_normalization_1/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_1/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_2/kernel:0!base_model/conv2d_2/kernel/Assign0base_model/conv2d_2/kernel/Read/ReadVariableOp:0(29base_model/conv2d_2/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_2/gamma:0-base_model/batch_normalization_2/gamma/Assign<base_model/batch_normalization_2/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_2/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_2/beta:0,base_model/batch_normalization_2/beta/Assign;base_model/batch_normalization_2/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_2/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_2/moving_mean:03base_model/batch_normalization_2/moving_mean/AssignBbase_model/batch_normalization_2/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_2/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_2/moving_variance:07base_model/batch_normalization_2/moving_variance/AssignFbase_model/batch_normalization_2/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_2/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_3/kernel:0!base_model/conv2d_3/kernel/Assign0base_model/conv2d_3/kernel/Read/ReadVariableOp:0(29base_model/conv2d_3/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_3/gamma:0-base_model/batch_normalization_3/gamma/Assign<base_model/batch_normalization_3/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_3/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_3/beta:0,base_model/batch_normalization_3/beta/Assign;base_model/batch_normalization_3/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_3/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_3/moving_mean:03base_model/batch_normalization_3/moving_mean/AssignBbase_model/batch_normalization_3/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_3/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_3/moving_variance:07base_model/batch_normalization_3/moving_variance/AssignFbase_model/batch_normalization_3/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_3/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_4/kernel:0!base_model/conv2d_4/kernel/Assign0base_model/conv2d_4/kernel/Read/ReadVariableOp:0(29base_model/conv2d_4/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_4/gamma:0-base_model/batch_normalization_4/gamma/Assign<base_model/batch_normalization_4/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_4/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_4/beta:0,base_model/batch_normalization_4/beta/Assign;base_model/batch_normalization_4/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_4/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_4/moving_mean:03base_model/batch_normalization_4/moving_mean/AssignBbase_model/batch_normalization_4/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_4/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_4/moving_variance:07base_model/batch_normalization_4/moving_variance/AssignFbase_model/batch_normalization_4/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_4/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_5/kernel:0!base_model/conv2d_5/kernel/Assign0base_model/conv2d_5/kernel/Read/ReadVariableOp:0(29base_model/conv2d_5/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_5/gamma:0-base_model/batch_normalization_5/gamma/Assign<base_model/batch_normalization_5/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_5/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_5/beta:0,base_model/batch_normalization_5/beta/Assign;base_model/batch_normalization_5/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_5/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_5/moving_mean:03base_model/batch_normalization_5/moving_mean/AssignBbase_model/batch_normalization_5/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_5/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_5/moving_variance:07base_model/batch_normalization_5/moving_variance/AssignFbase_model/batch_normalization_5/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_5/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_6/kernel:0!base_model/conv2d_6/kernel/Assign0base_model/conv2d_6/kernel/Read/ReadVariableOp:0(29base_model/conv2d_6/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_6/gamma:0-base_model/batch_normalization_6/gamma/Assign<base_model/batch_normalization_6/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_6/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_6/beta:0,base_model/batch_normalization_6/beta/Assign;base_model/batch_normalization_6/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_6/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_6/moving_mean:03base_model/batch_normalization_6/moving_mean/AssignBbase_model/batch_normalization_6/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_6/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_6/moving_variance:07base_model/batch_normalization_6/moving_variance/AssignFbase_model/batch_normalization_6/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_6/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_7/kernel:0!base_model/conv2d_7/kernel/Assign0base_model/conv2d_7/kernel/Read/ReadVariableOp:0(29base_model/conv2d_7/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_7/gamma:0-base_model/batch_normalization_7/gamma/Assign<base_model/batch_normalization_7/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_7/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_7/beta:0,base_model/batch_normalization_7/beta/Assign;base_model/batch_normalization_7/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_7/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_7/moving_mean:03base_model/batch_normalization_7/moving_mean/AssignBbase_model/batch_normalization_7/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_7/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_7/moving_variance:07base_model/batch_normalization_7/moving_variance/AssignFbase_model/batch_normalization_7/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_7/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_8/kernel:0!base_model/conv2d_8/kernel/Assign0base_model/conv2d_8/kernel/Read/ReadVariableOp:0(29base_model/conv2d_8/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_8/gamma:0-base_model/batch_normalization_8/gamma/Assign<base_model/batch_normalization_8/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_8/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_8/beta:0,base_model/batch_normalization_8/beta/Assign;base_model/batch_normalization_8/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_8/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_8/moving_mean:03base_model/batch_normalization_8/moving_mean/AssignBbase_model/batch_normalization_8/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_8/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_8/moving_variance:07base_model/batch_normalization_8/moving_variance/AssignFbase_model/batch_normalization_8/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_8/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_9/kernel:0!base_model/conv2d_9/kernel/Assign0base_model/conv2d_9/kernel/Read/ReadVariableOp:0(29base_model/conv2d_9/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_9/gamma:0-base_model/batch_normalization_9/gamma/Assign<base_model/batch_normalization_9/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_9/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_9/beta:0,base_model/batch_normalization_9/beta/Assign;base_model/batch_normalization_9/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_9/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_9/moving_mean:03base_model/batch_normalization_9/moving_mean/AssignBbase_model/batch_normalization_9/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_9/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_9/moving_variance:07base_model/batch_normalization_9/moving_variance/AssignFbase_model/batch_normalization_9/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_9/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_10/kernel:0"base_model/conv2d_10/kernel/Assign1base_model/conv2d_10/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_10/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_10/gamma:0.base_model/batch_normalization_10/gamma/Assign=base_model/batch_normalization_10/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_10/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_10/beta:0-base_model/batch_normalization_10/beta/Assign<base_model/batch_normalization_10/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_10/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_10/moving_mean:04base_model/batch_normalization_10/moving_mean/AssignCbase_model/batch_normalization_10/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_10/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_10/moving_variance:08base_model/batch_normalization_10/moving_variance/AssignGbase_model/batch_normalization_10/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_10/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_11/kernel:0"base_model/conv2d_11/kernel/Assign1base_model/conv2d_11/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_11/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_11/gamma:0.base_model/batch_normalization_11/gamma/Assign=base_model/batch_normalization_11/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_11/gamma/Initializer/ones:08
×
(base_model/batch_normalization_11/beta:0-base_model/batch_normalization_11/beta/Assign<base_model/batch_normalization_11/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_11/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_11/moving_mean:04base_model/batch_normalization_11/moving_mean/AssignCbase_model/batch_normalization_11/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_11/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_11/moving_variance:08base_model/batch_normalization_11/moving_variance/AssignGbase_model/batch_normalization_11/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_11/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_12/kernel:0"base_model/conv2d_12/kernel/Assign1base_model/conv2d_12/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_12/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_12/gamma:0.base_model/batch_normalization_12/gamma/Assign=base_model/batch_normalization_12/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_12/gamma/Initializer/ones:08
×
(base_model/batch_normalization_12/beta:0-base_model/batch_normalization_12/beta/Assign<base_model/batch_normalization_12/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_12/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_12/moving_mean:04base_model/batch_normalization_12/moving_mean/AssignCbase_model/batch_normalization_12/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_12/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_12/moving_variance:08base_model/batch_normalization_12/moving_variance/AssignGbase_model/batch_normalization_12/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_12/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_13/kernel:0"base_model/conv2d_13/kernel/Assign1base_model/conv2d_13/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_13/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_13/gamma:0.base_model/batch_normalization_13/gamma/Assign=base_model/batch_normalization_13/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_13/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_13/beta:0-base_model/batch_normalization_13/beta/Assign<base_model/batch_normalization_13/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_13/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_13/moving_mean:04base_model/batch_normalization_13/moving_mean/AssignCbase_model/batch_normalization_13/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_13/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_13/moving_variance:08base_model/batch_normalization_13/moving_variance/AssignGbase_model/batch_normalization_13/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_13/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_14/kernel:0"base_model/conv2d_14/kernel/Assign1base_model/conv2d_14/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_14/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_14/gamma:0.base_model/batch_normalization_14/gamma/Assign=base_model/batch_normalization_14/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_14/gamma/Initializer/ones:08
×
(base_model/batch_normalization_14/beta:0-base_model/batch_normalization_14/beta/Assign<base_model/batch_normalization_14/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_14/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_14/moving_mean:04base_model/batch_normalization_14/moving_mean/AssignCbase_model/batch_normalization_14/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_14/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_14/moving_variance:08base_model/batch_normalization_14/moving_variance/AssignGbase_model/batch_normalization_14/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_14/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_15/kernel:0"base_model/conv2d_15/kernel/Assign1base_model/conv2d_15/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_15/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_15/gamma:0.base_model/batch_normalization_15/gamma/Assign=base_model/batch_normalization_15/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_15/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_15/beta:0-base_model/batch_normalization_15/beta/Assign<base_model/batch_normalization_15/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_15/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_15/moving_mean:04base_model/batch_normalization_15/moving_mean/AssignCbase_model/batch_normalization_15/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_15/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_15/moving_variance:08base_model/batch_normalization_15/moving_variance/AssignGbase_model/batch_normalization_15/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_15/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_16/kernel:0"base_model/conv2d_16/kernel/Assign1base_model/conv2d_16/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_16/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_16/gamma:0.base_model/batch_normalization_16/gamma/Assign=base_model/batch_normalization_16/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_16/gamma/Initializer/ones:08
×
(base_model/batch_normalization_16/beta:0-base_model/batch_normalization_16/beta/Assign<base_model/batch_normalization_16/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_16/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_16/moving_mean:04base_model/batch_normalization_16/moving_mean/AssignCbase_model/batch_normalization_16/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_16/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_16/moving_variance:08base_model/batch_normalization_16/moving_variance/AssignGbase_model/batch_normalization_16/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_16/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_17/kernel:0"base_model/conv2d_17/kernel/Assign1base_model/conv2d_17/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_17/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_17/gamma:0.base_model/batch_normalization_17/gamma/Assign=base_model/batch_normalization_17/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_17/gamma/Initializer/ones:08
×
(base_model/batch_normalization_17/beta:0-base_model/batch_normalization_17/beta/Assign<base_model/batch_normalization_17/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_17/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_17/moving_mean:04base_model/batch_normalization_17/moving_mean/AssignCbase_model/batch_normalization_17/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_17/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_17/moving_variance:08base_model/batch_normalization_17/moving_variance/AssignGbase_model/batch_normalization_17/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_17/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_18/kernel:0"base_model/conv2d_18/kernel/Assign1base_model/conv2d_18/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_18/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_18/gamma:0.base_model/batch_normalization_18/gamma/Assign=base_model/batch_normalization_18/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_18/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_18/beta:0-base_model/batch_normalization_18/beta/Assign<base_model/batch_normalization_18/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_18/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_18/moving_mean:04base_model/batch_normalization_18/moving_mean/AssignCbase_model/batch_normalization_18/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_18/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_18/moving_variance:08base_model/batch_normalization_18/moving_variance/AssignGbase_model/batch_normalization_18/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_18/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_19/kernel:0"base_model/conv2d_19/kernel/Assign1base_model/conv2d_19/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_19/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_19/gamma:0.base_model/batch_normalization_19/gamma/Assign=base_model/batch_normalization_19/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_19/gamma/Initializer/ones:08
×
(base_model/batch_normalization_19/beta:0-base_model/batch_normalization_19/beta/Assign<base_model/batch_normalization_19/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_19/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_19/moving_mean:04base_model/batch_normalization_19/moving_mean/AssignCbase_model/batch_normalization_19/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_19/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_19/moving_variance:08base_model/batch_normalization_19/moving_variance/AssignGbase_model/batch_normalization_19/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_19/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_20/kernel:0"base_model/conv2d_20/kernel/Assign1base_model/conv2d_20/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_20/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_20/gamma:0.base_model/batch_normalization_20/gamma/Assign=base_model/batch_normalization_20/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_20/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_20/beta:0-base_model/batch_normalization_20/beta/Assign<base_model/batch_normalization_20/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_20/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_20/moving_mean:04base_model/batch_normalization_20/moving_mean/AssignCbase_model/batch_normalization_20/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_20/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_20/moving_variance:08base_model/batch_normalization_20/moving_variance/AssignGbase_model/batch_normalization_20/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_20/moving_variance/Initializer/ones:0@H
ë
+head_supervised/linear_layer/dense/kernel:00head_supervised/linear_layer/dense/kernel/Assign?head_supervised/linear_layer/dense/kernel/Read/ReadVariableOp:0(2Ehead_supervised/linear_layer/dense/kernel/Initializer/random_normal:08
Ű
)head_supervised/linear_layer/dense/bias:0.head_supervised/linear_layer/dense/bias/Assign=head_supervised/linear_layer/dense/bias/Read/ReadVariableOp:0(2;head_supervised/linear_layer/dense/bias/Initializer/zeros:08"i
trainable_variables÷hôh
Ş
base_model/conv2d/kernel:0base_model/conv2d/kernel/Assign.base_model/conv2d/kernel/Read/ReadVariableOp:0(27base_model/conv2d/kernel/Initializer/truncated_normal:08
Î
&base_model/batch_normalization/gamma:0+base_model/batch_normalization/gamma/Assign:base_model/batch_normalization/gamma/Read/ReadVariableOp:0(27base_model/batch_normalization/gamma/Initializer/ones:08
Ë
%base_model/batch_normalization/beta:0*base_model/batch_normalization/beta/Assign9base_model/batch_normalization/beta/Read/ReadVariableOp:0(27base_model/batch_normalization/beta/Initializer/zeros:08
˛
base_model/conv2d_1/kernel:0!base_model/conv2d_1/kernel/Assign0base_model/conv2d_1/kernel/Read/ReadVariableOp:0(29base_model/conv2d_1/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_1/gamma:0-base_model/batch_normalization_1/gamma/Assign<base_model/batch_normalization_1/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_1/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_1/beta:0,base_model/batch_normalization_1/beta/Assign;base_model/batch_normalization_1/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_1/beta/Initializer/zeros:08
˛
base_model/conv2d_2/kernel:0!base_model/conv2d_2/kernel/Assign0base_model/conv2d_2/kernel/Read/ReadVariableOp:0(29base_model/conv2d_2/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_2/gamma:0-base_model/batch_normalization_2/gamma/Assign<base_model/batch_normalization_2/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_2/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_2/beta:0,base_model/batch_normalization_2/beta/Assign;base_model/batch_normalization_2/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_2/beta/Initializer/zeros:08
˛
base_model/conv2d_3/kernel:0!base_model/conv2d_3/kernel/Assign0base_model/conv2d_3/kernel/Read/ReadVariableOp:0(29base_model/conv2d_3/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_3/gamma:0-base_model/batch_normalization_3/gamma/Assign<base_model/batch_normalization_3/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_3/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_3/beta:0,base_model/batch_normalization_3/beta/Assign;base_model/batch_normalization_3/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_3/beta/Initializer/zeros:08
˛
base_model/conv2d_4/kernel:0!base_model/conv2d_4/kernel/Assign0base_model/conv2d_4/kernel/Read/ReadVariableOp:0(29base_model/conv2d_4/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_4/gamma:0-base_model/batch_normalization_4/gamma/Assign<base_model/batch_normalization_4/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_4/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_4/beta:0,base_model/batch_normalization_4/beta/Assign;base_model/batch_normalization_4/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_4/beta/Initializer/zeros:08
˛
base_model/conv2d_5/kernel:0!base_model/conv2d_5/kernel/Assign0base_model/conv2d_5/kernel/Read/ReadVariableOp:0(29base_model/conv2d_5/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_5/gamma:0-base_model/batch_normalization_5/gamma/Assign<base_model/batch_normalization_5/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_5/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_5/beta:0,base_model/batch_normalization_5/beta/Assign;base_model/batch_normalization_5/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_5/beta/Initializer/zeros:08
˛
base_model/conv2d_6/kernel:0!base_model/conv2d_6/kernel/Assign0base_model/conv2d_6/kernel/Read/ReadVariableOp:0(29base_model/conv2d_6/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_6/gamma:0-base_model/batch_normalization_6/gamma/Assign<base_model/batch_normalization_6/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_6/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_6/beta:0,base_model/batch_normalization_6/beta/Assign;base_model/batch_normalization_6/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_6/beta/Initializer/zeros:08
˛
base_model/conv2d_7/kernel:0!base_model/conv2d_7/kernel/Assign0base_model/conv2d_7/kernel/Read/ReadVariableOp:0(29base_model/conv2d_7/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_7/gamma:0-base_model/batch_normalization_7/gamma/Assign<base_model/batch_normalization_7/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_7/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_7/beta:0,base_model/batch_normalization_7/beta/Assign;base_model/batch_normalization_7/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_7/beta/Initializer/zeros:08
˛
base_model/conv2d_8/kernel:0!base_model/conv2d_8/kernel/Assign0base_model/conv2d_8/kernel/Read/ReadVariableOp:0(29base_model/conv2d_8/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_8/gamma:0-base_model/batch_normalization_8/gamma/Assign<base_model/batch_normalization_8/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_8/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_8/beta:0,base_model/batch_normalization_8/beta/Assign;base_model/batch_normalization_8/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_8/beta/Initializer/zeros:08
˛
base_model/conv2d_9/kernel:0!base_model/conv2d_9/kernel/Assign0base_model/conv2d_9/kernel/Read/ReadVariableOp:0(29base_model/conv2d_9/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_9/gamma:0-base_model/batch_normalization_9/gamma/Assign<base_model/batch_normalization_9/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_9/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_9/beta:0,base_model/batch_normalization_9/beta/Assign;base_model/batch_normalization_9/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_9/beta/Initializer/zeros:08
ś
base_model/conv2d_10/kernel:0"base_model/conv2d_10/kernel/Assign1base_model/conv2d_10/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_10/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_10/gamma:0.base_model/batch_normalization_10/gamma/Assign=base_model/batch_normalization_10/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_10/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_10/beta:0-base_model/batch_normalization_10/beta/Assign<base_model/batch_normalization_10/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_10/beta/Initializer/zeros:08
ś
base_model/conv2d_11/kernel:0"base_model/conv2d_11/kernel/Assign1base_model/conv2d_11/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_11/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_11/gamma:0.base_model/batch_normalization_11/gamma/Assign=base_model/batch_normalization_11/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_11/gamma/Initializer/ones:08
×
(base_model/batch_normalization_11/beta:0-base_model/batch_normalization_11/beta/Assign<base_model/batch_normalization_11/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_11/beta/Initializer/zeros:08
ś
base_model/conv2d_12/kernel:0"base_model/conv2d_12/kernel/Assign1base_model/conv2d_12/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_12/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_12/gamma:0.base_model/batch_normalization_12/gamma/Assign=base_model/batch_normalization_12/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_12/gamma/Initializer/ones:08
×
(base_model/batch_normalization_12/beta:0-base_model/batch_normalization_12/beta/Assign<base_model/batch_normalization_12/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_12/beta/Initializer/zeros:08
ś
base_model/conv2d_13/kernel:0"base_model/conv2d_13/kernel/Assign1base_model/conv2d_13/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_13/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_13/gamma:0.base_model/batch_normalization_13/gamma/Assign=base_model/batch_normalization_13/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_13/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_13/beta:0-base_model/batch_normalization_13/beta/Assign<base_model/batch_normalization_13/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_13/beta/Initializer/zeros:08
ś
base_model/conv2d_14/kernel:0"base_model/conv2d_14/kernel/Assign1base_model/conv2d_14/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_14/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_14/gamma:0.base_model/batch_normalization_14/gamma/Assign=base_model/batch_normalization_14/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_14/gamma/Initializer/ones:08
×
(base_model/batch_normalization_14/beta:0-base_model/batch_normalization_14/beta/Assign<base_model/batch_normalization_14/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_14/beta/Initializer/zeros:08
ś
base_model/conv2d_15/kernel:0"base_model/conv2d_15/kernel/Assign1base_model/conv2d_15/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_15/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_15/gamma:0.base_model/batch_normalization_15/gamma/Assign=base_model/batch_normalization_15/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_15/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_15/beta:0-base_model/batch_normalization_15/beta/Assign<base_model/batch_normalization_15/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_15/beta/Initializer/zeros:08
ś
base_model/conv2d_16/kernel:0"base_model/conv2d_16/kernel/Assign1base_model/conv2d_16/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_16/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_16/gamma:0.base_model/batch_normalization_16/gamma/Assign=base_model/batch_normalization_16/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_16/gamma/Initializer/ones:08
×
(base_model/batch_normalization_16/beta:0-base_model/batch_normalization_16/beta/Assign<base_model/batch_normalization_16/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_16/beta/Initializer/zeros:08
ś
base_model/conv2d_17/kernel:0"base_model/conv2d_17/kernel/Assign1base_model/conv2d_17/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_17/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_17/gamma:0.base_model/batch_normalization_17/gamma/Assign=base_model/batch_normalization_17/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_17/gamma/Initializer/ones:08
×
(base_model/batch_normalization_17/beta:0-base_model/batch_normalization_17/beta/Assign<base_model/batch_normalization_17/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_17/beta/Initializer/zeros:08
ś
base_model/conv2d_18/kernel:0"base_model/conv2d_18/kernel/Assign1base_model/conv2d_18/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_18/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_18/gamma:0.base_model/batch_normalization_18/gamma/Assign=base_model/batch_normalization_18/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_18/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_18/beta:0-base_model/batch_normalization_18/beta/Assign<base_model/batch_normalization_18/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_18/beta/Initializer/zeros:08
ś
base_model/conv2d_19/kernel:0"base_model/conv2d_19/kernel/Assign1base_model/conv2d_19/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_19/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_19/gamma:0.base_model/batch_normalization_19/gamma/Assign=base_model/batch_normalization_19/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_19/gamma/Initializer/ones:08
×
(base_model/batch_normalization_19/beta:0-base_model/batch_normalization_19/beta/Assign<base_model/batch_normalization_19/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_19/beta/Initializer/zeros:08
ś
base_model/conv2d_20/kernel:0"base_model/conv2d_20/kernel/Assign1base_model/conv2d_20/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_20/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_20/gamma:0.base_model/batch_normalization_20/gamma/Assign=base_model/batch_normalization_20/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_20/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_20/beta:0-base_model/batch_normalization_20/beta/Assign<base_model/batch_normalization_20/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_20/beta/Initializer/zeros:08
ë
+head_supervised/linear_layer/dense/kernel:00head_supervised/linear_layer/dense/kernel/Assign?head_supervised/linear_layer/dense/kernel/Read/ReadVariableOp:0(2Ehead_supervised/linear_layer/dense/kernel/Initializer/random_normal:08
Ű
)head_supervised/linear_layer/dense/bias:0.head_supervised/linear_layer/dense/bias/Assign=head_supervised/linear_layer/dense/bias/Read/ReadVariableOp:0(2;head_supervised/linear_layer/dense/bias/Initializer/zeros:08*ć
defaultÚ
H
images>
Placeholder:0+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙Z
initial_convJ
base_model/initial_conv:0+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@E
final_avg_pool3
base_model/final_avg_pool:0˙˙˙˙˙˙˙˙˙[
block_group3K
base_model/block_group3:0,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙>
default3
base_model/final_avg_pool:0˙˙˙˙˙˙˙˙˙Z
block_group1J
base_model/block_group1:0+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@[
block_group4K
base_model/block_group4:0,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙[
block_group2K
base_model/block_group2:0,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙b
initial_max_poolN
base_model/initial_max_pool:0+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@T

logits_supF
/head_supervised/linear_layer/linear_layer_out:0˙˙˙˙˙˙˙˙˙ÍĐ
ňÉ
:
Add
x"T
y"T
z"T"
Ttype:
2	
A
AddV2
x"T
y"T
z"T"
Ttype:
2	
E
AssignSubVariableOp
resource
value"dtype"
dtypetype
B
AssignVariableOp
resource
value"dtype"
dtypetype
~
BiasAdd

value"T	
bias"T
output"T" 
Ttype:
2	"-
data_formatstringNHWC:
NHWCNCHW
8
Const
output"dtype"
valuetensor"
dtypetype

Conv2D

input"T
filter"T
output"T"
Ttype:
2"
strides	list(int)"
use_cudnn_on_gpubool(",
paddingstring:
SAMEVALIDEXPLICIT""
explicit_paddings	list(int)
 "-
data_formatstringNHWC:
NHWCNCHW" 
	dilations	list(int)

Ä
FusedBatchNormV3
x"T

scale"U
offset"U	
mean"U
variance"U
y"T

batch_mean"U
batch_variance"U
reserve_space_1"U
reserve_space_2"U
reserve_space_3"U"
Ttype:
2"
Utype:
2"
epsilonfloat%ˇŃ8"-
data_formatstringNHWC:
NHWCNCHW"
is_trainingbool(
.
Identity

input"T
output"T"	
Ttype
q
MatMul
a"T
b"T
product"T"
transpose_abool( "
transpose_bbool( "
Ttype:

2	
Ô
MaxPool

input"T
output"T"
Ttype0:
2	"
ksize	list(int)(0"
strides	list(int)(0""
paddingstring:
SAMEVALID":
data_formatstringNHWC:
NHWCNCHWNCHW_VECT_C

Mean

input"T
reduction_indices"Tidx
output"T"
	keep_dimsbool( " 
Ttype:
2	"
Tidxtype0:
2	
=
Mul
x"T
y"T
z"T"
Ttype:
2	
_
Pad

input"T
paddings"	Tpaddings
output"T"	
Ttype"
	Tpaddingstype0:
2	
C
Placeholder
output"dtype"
dtypetype"
shapeshape:

RandomStandardNormal

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
@
ReadVariableOp
resource
value"dtype"
dtypetype
E
Relu
features"T
activations"T"
Ttype:
2	
2
StopGradient

input"T
output"T"	
Ttype
:
Sub
x"T
y"T
z"T"
Ttype:
2	

TruncatedNormal

shape"T
output"dtype"
seedint "
seed2int "
dtypetype:
2"
Ttype:
2	
q
VarHandleOp
resource"
	containerstring "
shared_namestring "
dtypetype"
shapeshape
9
VarIsInitializedOp
resource
is_initialized
"train*1.15.42v1.15.3-68-gdf8c55c8öř
˘
PlaceholderPlaceholder*
dtype0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*6
shape-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

base_model/Pad/paddingsConst*
dtype0*9
value0B."                             *
_output_shapes

:

base_model/PadPadPlaceholderbase_model/Pad/paddings*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Á
;base_model/conv2d/kernel/Initializer/truncated_normal/shapeConst*
dtype0*%
valueB"         @   *+
_class!
loc:@base_model/conv2d/kernel*
_output_shapes
:
Ź
:base_model/conv2d/kernel/Initializer/truncated_normal/meanConst*+
_class!
loc:@base_model/conv2d/kernel*
_output_shapes
: *
valueB
 *    *
dtype0
Ž
<base_model/conv2d/kernel/Initializer/truncated_normal/stddevConst*+
_class!
loc:@base_model/conv2d/kernel*
_output_shapes
: *
dtype0*
valueB
 *+Ŕ=

Ebase_model/conv2d/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal;base_model/conv2d/kernel/Initializer/truncated_normal/shape*&
_output_shapes
:@*
T0*
dtype0*+
_class!
loc:@base_model/conv2d/kernel
Ł
9base_model/conv2d/kernel/Initializer/truncated_normal/mulMulEbase_model/conv2d/kernel/Initializer/truncated_normal/TruncatedNormal<base_model/conv2d/kernel/Initializer/truncated_normal/stddev*+
_class!
loc:@base_model/conv2d/kernel*&
_output_shapes
:@*
T0

5base_model/conv2d/kernel/Initializer/truncated_normalAdd9base_model/conv2d/kernel/Initializer/truncated_normal/mul:base_model/conv2d/kernel/Initializer/truncated_normal/mean*+
_class!
loc:@base_model/conv2d/kernel*&
_output_shapes
:@*
T0
Á
base_model/conv2d/kernelVarHandleOp*
_output_shapes
: *)
shared_namebase_model/conv2d/kernel*+
_class!
loc:@base_model/conv2d/kernel*
dtype0*
shape:@

9base_model/conv2d/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d/kernel*
_output_shapes
: 

base_model/conv2d/kernel/AssignAssignVariableOpbase_model/conv2d/kernel5base_model/conv2d/kernel/Initializer/truncated_normal*
dtype0

,base_model/conv2d/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d/kernel*&
_output_shapes
:@*
dtype0
p
base_model/conv2d/dilation_rateConst*
_output_shapes
:*
valueB"      *
dtype0

'base_model/conv2d/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d/kernel*
dtype0*&
_output_shapes
:@
Đ
base_model/conv2d/Conv2DConv2Dbase_model/Pad'base_model/conv2d/Conv2D/ReadVariableOp*
paddingVALID*
strides
*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0

base_model/initial_convIdentitybase_model/conv2d/Conv2D*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
ť
5base_model/batch_normalization/gamma/Initializer/onesConst*
valueB@*  ?*7
_class-
+)loc:@base_model/batch_normalization/gamma*
dtype0*
_output_shapes
:@
Ů
$base_model/batch_normalization/gammaVarHandleOp*5
shared_name&$base_model/batch_normalization/gamma*
shape:@*
_output_shapes
: *
dtype0*7
_class-
+)loc:@base_model/batch_normalization/gamma

Ebase_model/batch_normalization/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp$base_model/batch_normalization/gamma*
_output_shapes
: 
Š
+base_model/batch_normalization/gamma/AssignAssignVariableOp$base_model/batch_normalization/gamma5base_model/batch_normalization/gamma/Initializer/ones*
dtype0

8base_model/batch_normalization/gamma/Read/ReadVariableOpReadVariableOp$base_model/batch_normalization/gamma*
_output_shapes
:@*
dtype0
ş
5base_model/batch_normalization/beta/Initializer/zerosConst*
valueB@*    *6
_class,
*(loc:@base_model/batch_normalization/beta*
_output_shapes
:@*
dtype0
Ö
#base_model/batch_normalization/betaVarHandleOp*
dtype0*4
shared_name%#base_model/batch_normalization/beta*
_output_shapes
: *6
_class,
*(loc:@base_model/batch_normalization/beta*
shape:@

Dbase_model/batch_normalization/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp#base_model/batch_normalization/beta*
_output_shapes
: 
§
*base_model/batch_normalization/beta/AssignAssignVariableOp#base_model/batch_normalization/beta5base_model/batch_normalization/beta/Initializer/zeros*
dtype0

7base_model/batch_normalization/beta/Read/ReadVariableOpReadVariableOp#base_model/batch_normalization/beta*
dtype0*
_output_shapes
:@
Č
<base_model/batch_normalization/moving_mean/Initializer/zerosConst*
dtype0*=
_class3
1/loc:@base_model/batch_normalization/moving_mean*
_output_shapes
:@*
valueB@*    
ë
*base_model/batch_normalization/moving_meanVarHandleOp*=
_class3
1/loc:@base_model/batch_normalization/moving_mean*
shape:@*
dtype0*
_output_shapes
: *;
shared_name,*base_model/batch_normalization/moving_mean
Ľ
Kbase_model/batch_normalization/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp*base_model/batch_normalization/moving_mean*
_output_shapes
: 
ź
1base_model/batch_normalization/moving_mean/AssignAssignVariableOp*base_model/batch_normalization/moving_mean<base_model/batch_normalization/moving_mean/Initializer/zeros*
dtype0
Ľ
>base_model/batch_normalization/moving_mean/Read/ReadVariableOpReadVariableOp*base_model/batch_normalization/moving_mean*
_output_shapes
:@*
dtype0
Ď
?base_model/batch_normalization/moving_variance/Initializer/onesConst*
valueB@*  ?*
_output_shapes
:@*A
_class7
53loc:@base_model/batch_normalization/moving_variance*
dtype0
÷
.base_model/batch_normalization/moving_varianceVarHandleOp*
shape:@*
_output_shapes
: *
dtype0*A
_class7
53loc:@base_model/batch_normalization/moving_variance*?
shared_name0.base_model/batch_normalization/moving_variance
­
Obase_model/batch_normalization/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp.base_model/batch_normalization/moving_variance*
_output_shapes
: 
Ç
5base_model/batch_normalization/moving_variance/AssignAssignVariableOp.base_model/batch_normalization/moving_variance?base_model/batch_normalization/moving_variance/Initializer/ones*
dtype0
­
Bbase_model/batch_normalization/moving_variance/Read/ReadVariableOpReadVariableOp.base_model/batch_normalization/moving_variance*
dtype0*
_output_shapes
:@

-base_model/batch_normalization/ReadVariableOpReadVariableOp$base_model/batch_normalization/gamma*
_output_shapes
:@*
dtype0

/base_model/batch_normalization/ReadVariableOp_1ReadVariableOp#base_model/batch_normalization/beta*
dtype0*
_output_shapes
:@
g
$base_model/batch_normalization/ConstConst*
valueB *
dtype0*
_output_shapes
: 
i
&base_model/batch_normalization/Const_1Const*
_output_shapes
: *
valueB *
dtype0

/base_model/batch_normalization/FusedBatchNormV3FusedBatchNormV3base_model/initial_conv-base_model/batch_normalization/ReadVariableOp/base_model/batch_normalization/ReadVariableOp_1$base_model/batch_normalization/Const&base_model/batch_normalization/Const_1*
epsilon%đ'7*
T0*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:*
U0
k
&base_model/batch_normalization/Const_2Const*
valueB
 *fff?*
dtype0*
_output_shapes
: 
¸
4base_model/batch_normalization/AssignMovingAvg/sub/xConst*
_output_shapes
: *
valueB
 *  ?*
dtype0*=
_class3
1/loc:@base_model/batch_normalization/moving_mean
÷
2base_model/batch_normalization/AssignMovingAvg/subSub4base_model/batch_normalization/AssignMovingAvg/sub/x&base_model/batch_normalization/Const_2*
T0*
_output_shapes
: *=
_class3
1/loc:@base_model/batch_normalization/moving_mean
¤
=base_model/batch_normalization/AssignMovingAvg/ReadVariableOpReadVariableOp*base_model/batch_normalization/moving_mean*
dtype0*
_output_shapes
:@

4base_model/batch_normalization/AssignMovingAvg/sub_1Sub=base_model/batch_normalization/AssignMovingAvg/ReadVariableOp1base_model/batch_normalization/FusedBatchNormV3:1*
_output_shapes
:@*
T0*=
_class3
1/loc:@base_model/batch_normalization/moving_mean

2base_model/batch_normalization/AssignMovingAvg/mulMul4base_model/batch_normalization/AssignMovingAvg/sub_12base_model/batch_normalization/AssignMovingAvg/sub*=
_class3
1/loc:@base_model/batch_normalization/moving_mean*
_output_shapes
:@*
T0

Bbase_model/batch_normalization/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp*base_model/batch_normalization/moving_mean2base_model/batch_normalization/AssignMovingAvg/mul*=
_class3
1/loc:@base_model/batch_normalization/moving_mean*
dtype0
Ş
?base_model/batch_normalization/AssignMovingAvg/ReadVariableOp_1ReadVariableOp*base_model/batch_normalization/moving_meanC^base_model/batch_normalization/AssignMovingAvg/AssignSubVariableOp*=
_class3
1/loc:@base_model/batch_normalization/moving_mean*
dtype0*
_output_shapes
:@
ž
6base_model/batch_normalization/AssignMovingAvg_1/sub/xConst*A
_class7
53loc:@base_model/batch_normalization/moving_variance*
dtype0*
_output_shapes
: *
valueB
 *  ?
˙
4base_model/batch_normalization/AssignMovingAvg_1/subSub6base_model/batch_normalization/AssignMovingAvg_1/sub/x&base_model/batch_normalization/Const_2*A
_class7
53loc:@base_model/batch_normalization/moving_variance*
_output_shapes
: *
T0
Ş
?base_model/batch_normalization/AssignMovingAvg_1/ReadVariableOpReadVariableOp.base_model/batch_normalization/moving_variance*
_output_shapes
:@*
dtype0

6base_model/batch_normalization/AssignMovingAvg_1/sub_1Sub?base_model/batch_normalization/AssignMovingAvg_1/ReadVariableOp1base_model/batch_normalization/FusedBatchNormV3:2*A
_class7
53loc:@base_model/batch_normalization/moving_variance*
T0*
_output_shapes
:@

4base_model/batch_normalization/AssignMovingAvg_1/mulMul6base_model/batch_normalization/AssignMovingAvg_1/sub_14base_model/batch_normalization/AssignMovingAvg_1/sub*
T0*
_output_shapes
:@*A
_class7
53loc:@base_model/batch_normalization/moving_variance

Dbase_model/batch_normalization/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp.base_model/batch_normalization/moving_variance4base_model/batch_normalization/AssignMovingAvg_1/mul*
dtype0*A
_class7
53loc:@base_model/batch_normalization/moving_variance
ś
Abase_model/batch_normalization/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp.base_model/batch_normalization/moving_varianceE^base_model/batch_normalization/AssignMovingAvg_1/AssignSubVariableOp*A
_class7
53loc:@base_model/batch_normalization/moving_variance*
dtype0*
_output_shapes
:@

base_model/ReluRelu/base_model/batch_normalization/FusedBatchNormV3*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
ş
 base_model/max_pooling2d/MaxPoolMaxPoolbase_model/Relu*
paddingSAME*
strides
*
ksize
*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@

base_model/initial_max_poolIdentity base_model/max_pooling2d/MaxPool*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
Ĺ
=base_model/conv2d_1/kernel/Initializer/truncated_normal/shapeConst*%
valueB"      @   @   *
_output_shapes
:*
dtype0*-
_class#
!loc:@base_model/conv2d_1/kernel
°
<base_model/conv2d_1/kernel/Initializer/truncated_normal/meanConst*-
_class#
!loc:@base_model/conv2d_1/kernel*
dtype0*
_output_shapes
: *
valueB
 *    
˛
>base_model/conv2d_1/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
valueB
 *6>*
dtype0*-
_class#
!loc:@base_model/conv2d_1/kernel

Gbase_model/conv2d_1/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_1/kernel/Initializer/truncated_normal/shape*&
_output_shapes
:@@*
dtype0*
T0*-
_class#
!loc:@base_model/conv2d_1/kernel
Ť
;base_model/conv2d_1/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_1/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_1/kernel/Initializer/truncated_normal/stddev*&
_output_shapes
:@@*
T0*-
_class#
!loc:@base_model/conv2d_1/kernel

7base_model/conv2d_1/kernel/Initializer/truncated_normalAdd;base_model/conv2d_1/kernel/Initializer/truncated_normal/mul<base_model/conv2d_1/kernel/Initializer/truncated_normal/mean*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_1/kernel*
T0
Ç
base_model/conv2d_1/kernelVarHandleOp*
dtype0*
_output_shapes
: *
shape:@@*-
_class#
!loc:@base_model/conv2d_1/kernel*+
shared_namebase_model/conv2d_1/kernel

;base_model/conv2d_1/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_1/kernel*
_output_shapes
: 

!base_model/conv2d_1/kernel/AssignAssignVariableOpbase_model/conv2d_1/kernel7base_model/conv2d_1/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_1/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_1/kernel*
dtype0*&
_output_shapes
:@@
r
!base_model/conv2d_1/dilation_rateConst*
valueB"      *
_output_shapes
:*
dtype0

)base_model/conv2d_1/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_1/kernel*
dtype0*&
_output_shapes
:@@
ŕ
base_model/conv2d_1/Conv2DConv2Dbase_model/initial_max_pool)base_model/conv2d_1/Conv2D/ReadVariableOp*
T0*
strides
*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
paddingSAME
ż
7base_model/batch_normalization_1/gamma/Initializer/onesConst*
dtype0*
_output_shapes
:@*9
_class/
-+loc:@base_model/batch_normalization_1/gamma*
valueB@*  ?
ß
&base_model/batch_normalization_1/gammaVarHandleOp*
shape:@*
dtype0*7
shared_name(&base_model/batch_normalization_1/gamma*9
_class/
-+loc:@base_model/batch_normalization_1/gamma*
_output_shapes
: 

Gbase_model/batch_normalization_1/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_1/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_1/gamma/AssignAssignVariableOp&base_model/batch_normalization_1/gamma7base_model/batch_normalization_1/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_1/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_1/gamma*
dtype0*
_output_shapes
:@
ž
7base_model/batch_normalization_1/beta/Initializer/zerosConst*
_output_shapes
:@*8
_class.
,*loc:@base_model/batch_normalization_1/beta*
valueB@*    *
dtype0
Ü
%base_model/batch_normalization_1/betaVarHandleOp*6
shared_name'%base_model/batch_normalization_1/beta*
_output_shapes
: *8
_class.
,*loc:@base_model/batch_normalization_1/beta*
dtype0*
shape:@

Fbase_model/batch_normalization_1/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_1/beta*
_output_shapes
: 
­
,base_model/batch_normalization_1/beta/AssignAssignVariableOp%base_model/batch_normalization_1/beta7base_model/batch_normalization_1/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_1/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_1/beta*
dtype0*
_output_shapes
:@
Ě
>base_model/batch_normalization_1/moving_mean/Initializer/zerosConst*
valueB@*    *
dtype0*?
_class5
31loc:@base_model/batch_normalization_1/moving_mean*
_output_shapes
:@
ń
,base_model/batch_normalization_1/moving_meanVarHandleOp*
dtype0*
_output_shapes
: *?
_class5
31loc:@base_model/batch_normalization_1/moving_mean*
shape:@*=
shared_name.,base_model/batch_normalization_1/moving_mean
Š
Mbase_model/batch_normalization_1/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_1/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_1/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_1/moving_mean>base_model/batch_normalization_1/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_1/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_1/moving_mean*
dtype0*
_output_shapes
:@
Ó
Abase_model/batch_normalization_1/moving_variance/Initializer/onesConst*
_output_shapes
:@*C
_class9
75loc:@base_model/batch_normalization_1/moving_variance*
dtype0*
valueB@*  ?
ý
0base_model/batch_normalization_1/moving_varianceVarHandleOp*
shape:@*
dtype0*C
_class9
75loc:@base_model/batch_normalization_1/moving_variance*A
shared_name20base_model/batch_normalization_1/moving_variance*
_output_shapes
: 
ą
Qbase_model/batch_normalization_1/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_1/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_1/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_1/moving_varianceAbase_model/batch_normalization_1/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_1/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_1/moving_variance*
dtype0*
_output_shapes
:@

/base_model/batch_normalization_1/ReadVariableOpReadVariableOp&base_model/batch_normalization_1/gamma*
dtype0*
_output_shapes
:@

1base_model/batch_normalization_1/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_1/beta*
_output_shapes
:@*
dtype0
i
&base_model/batch_normalization_1/ConstConst*
valueB *
dtype0*
_output_shapes
: 
k
(base_model/batch_normalization_1/Const_1Const*
_output_shapes
: *
valueB *
dtype0

1base_model/batch_normalization_1/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_1/Conv2D/base_model/batch_normalization_1/ReadVariableOp1base_model/batch_normalization_1/ReadVariableOp_1&base_model/batch_normalization_1/Const(base_model/batch_normalization_1/Const_1*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:*
T0*
U0*
epsilon%đ'7
m
(base_model/batch_normalization_1/Const_2Const*
_output_shapes
: *
valueB
 *fff?*
dtype0
ź
6base_model/batch_normalization_1/AssignMovingAvg/sub/xConst*
_output_shapes
: *?
_class5
31loc:@base_model/batch_normalization_1/moving_mean*
valueB
 *  ?*
dtype0
˙
4base_model/batch_normalization_1/AssignMovingAvg/subSub6base_model/batch_normalization_1/AssignMovingAvg/sub/x(base_model/batch_normalization_1/Const_2*
_output_shapes
: *?
_class5
31loc:@base_model/batch_normalization_1/moving_mean*
T0
¨
?base_model/batch_normalization_1/AssignMovingAvg/ReadVariableOpReadVariableOp,base_model/batch_normalization_1/moving_mean*
_output_shapes
:@*
dtype0

6base_model/batch_normalization_1/AssignMovingAvg/sub_1Sub?base_model/batch_normalization_1/AssignMovingAvg/ReadVariableOp3base_model/batch_normalization_1/FusedBatchNormV3:1*
T0*?
_class5
31loc:@base_model/batch_normalization_1/moving_mean*
_output_shapes
:@

4base_model/batch_normalization_1/AssignMovingAvg/mulMul6base_model/batch_normalization_1/AssignMovingAvg/sub_14base_model/batch_normalization_1/AssignMovingAvg/sub*?
_class5
31loc:@base_model/batch_normalization_1/moving_mean*
T0*
_output_shapes
:@

Dbase_model/batch_normalization_1/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp,base_model/batch_normalization_1/moving_mean4base_model/batch_normalization_1/AssignMovingAvg/mul*
dtype0*?
_class5
31loc:@base_model/batch_normalization_1/moving_mean
˛
Abase_model/batch_normalization_1/AssignMovingAvg/ReadVariableOp_1ReadVariableOp,base_model/batch_normalization_1/moving_meanE^base_model/batch_normalization_1/AssignMovingAvg/AssignSubVariableOp*
dtype0*?
_class5
31loc:@base_model/batch_normalization_1/moving_mean*
_output_shapes
:@
Â
8base_model/batch_normalization_1/AssignMovingAvg_1/sub/xConst*
valueB
 *  ?*C
_class9
75loc:@base_model/batch_normalization_1/moving_variance*
dtype0*
_output_shapes
: 

6base_model/batch_normalization_1/AssignMovingAvg_1/subSub8base_model/batch_normalization_1/AssignMovingAvg_1/sub/x(base_model/batch_normalization_1/Const_2*
T0*C
_class9
75loc:@base_model/batch_normalization_1/moving_variance*
_output_shapes
: 
Ž
Abase_model/batch_normalization_1/AssignMovingAvg_1/ReadVariableOpReadVariableOp0base_model/batch_normalization_1/moving_variance*
_output_shapes
:@*
dtype0
Ą
8base_model/batch_normalization_1/AssignMovingAvg_1/sub_1SubAbase_model/batch_normalization_1/AssignMovingAvg_1/ReadVariableOp3base_model/batch_normalization_1/FusedBatchNormV3:2*
T0*
_output_shapes
:@*C
_class9
75loc:@base_model/batch_normalization_1/moving_variance

6base_model/batch_normalization_1/AssignMovingAvg_1/mulMul8base_model/batch_normalization_1/AssignMovingAvg_1/sub_16base_model/batch_normalization_1/AssignMovingAvg_1/sub*C
_class9
75loc:@base_model/batch_normalization_1/moving_variance*
_output_shapes
:@*
T0

Fbase_model/batch_normalization_1/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0base_model/batch_normalization_1/moving_variance6base_model/batch_normalization_1/AssignMovingAvg_1/mul*
dtype0*C
_class9
75loc:@base_model/batch_normalization_1/moving_variance
ž
Cbase_model/batch_normalization_1/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_1/moving_varianceG^base_model/batch_normalization_1/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*C
_class9
75loc:@base_model/batch_normalization_1/moving_variance*
_output_shapes
:@
Ĺ
=base_model/conv2d_2/kernel/Initializer/truncated_normal/shapeConst*
dtype0*%
valueB"      @   @   *
_output_shapes
:*-
_class#
!loc:@base_model/conv2d_2/kernel
°
<base_model/conv2d_2/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
dtype0*-
_class#
!loc:@base_model/conv2d_2/kernel*
_output_shapes
: 
˛
>base_model/conv2d_2/kernel/Initializer/truncated_normal/stddevConst*
dtype0*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_2/kernel*
valueB
 *B=

Gbase_model/conv2d_2/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_2/kernel/Initializer/truncated_normal/shape*-
_class#
!loc:@base_model/conv2d_2/kernel*
T0*&
_output_shapes
:@@*
dtype0
Ť
;base_model/conv2d_2/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_2/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_2/kernel/Initializer/truncated_normal/stddev*
T0*-
_class#
!loc:@base_model/conv2d_2/kernel*&
_output_shapes
:@@

7base_model/conv2d_2/kernel/Initializer/truncated_normalAdd;base_model/conv2d_2/kernel/Initializer/truncated_normal/mul<base_model/conv2d_2/kernel/Initializer/truncated_normal/mean*
T0*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_2/kernel
Ç
base_model/conv2d_2/kernelVarHandleOp*
dtype0*+
shared_namebase_model/conv2d_2/kernel*-
_class#
!loc:@base_model/conv2d_2/kernel*
shape:@@*
_output_shapes
: 

;base_model/conv2d_2/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_2/kernel*
_output_shapes
: 

!base_model/conv2d_2/kernel/AssignAssignVariableOpbase_model/conv2d_2/kernel7base_model/conv2d_2/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_2/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_2/kernel*&
_output_shapes
:@@*
dtype0
r
!base_model/conv2d_2/dilation_rateConst*
valueB"      *
_output_shapes
:*
dtype0

)base_model/conv2d_2/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_2/kernel*&
_output_shapes
:@@*
dtype0
ŕ
base_model/conv2d_2/Conv2DConv2Dbase_model/initial_max_pool)base_model/conv2d_2/Conv2D/ReadVariableOp*
paddingSAME*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
strides

ż
7base_model/batch_normalization_2/gamma/Initializer/onesConst*
dtype0*
valueB@*  ?*
_output_shapes
:@*9
_class/
-+loc:@base_model/batch_normalization_2/gamma
ß
&base_model/batch_normalization_2/gammaVarHandleOp*7
shared_name(&base_model/batch_normalization_2/gamma*
_output_shapes
: *9
_class/
-+loc:@base_model/batch_normalization_2/gamma*
dtype0*
shape:@

Gbase_model/batch_normalization_2/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_2/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_2/gamma/AssignAssignVariableOp&base_model/batch_normalization_2/gamma7base_model/batch_normalization_2/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_2/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_2/gamma*
dtype0*
_output_shapes
:@
ž
7base_model/batch_normalization_2/beta/Initializer/zerosConst*
valueB@*    *
_output_shapes
:@*8
_class.
,*loc:@base_model/batch_normalization_2/beta*
dtype0
Ü
%base_model/batch_normalization_2/betaVarHandleOp*8
_class.
,*loc:@base_model/batch_normalization_2/beta*
_output_shapes
: *6
shared_name'%base_model/batch_normalization_2/beta*
shape:@*
dtype0

Fbase_model/batch_normalization_2/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_2/beta*
_output_shapes
: 
­
,base_model/batch_normalization_2/beta/AssignAssignVariableOp%base_model/batch_normalization_2/beta7base_model/batch_normalization_2/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_2/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_2/beta*
_output_shapes
:@*
dtype0
Ě
>base_model/batch_normalization_2/moving_mean/Initializer/zerosConst*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean*
dtype0*
valueB@*    *
_output_shapes
:@
ń
,base_model/batch_normalization_2/moving_meanVarHandleOp*=
shared_name.,base_model/batch_normalization_2/moving_mean*
shape:@*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean*
dtype0*
_output_shapes
: 
Š
Mbase_model/batch_normalization_2/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_2/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_2/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_2/moving_mean>base_model/batch_normalization_2/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_2/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_2/moving_mean*
_output_shapes
:@*
dtype0
Ó
Abase_model/batch_normalization_2/moving_variance/Initializer/onesConst*C
_class9
75loc:@base_model/batch_normalization_2/moving_variance*
valueB@*  ?*
dtype0*
_output_shapes
:@
ý
0base_model/batch_normalization_2/moving_varianceVarHandleOp*
dtype0*C
_class9
75loc:@base_model/batch_normalization_2/moving_variance*
shape:@*A
shared_name20base_model/batch_normalization_2/moving_variance*
_output_shapes
: 
ą
Qbase_model/batch_normalization_2/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_2/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_2/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_2/moving_varianceAbase_model/batch_normalization_2/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_2/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_2/moving_variance*
dtype0*
_output_shapes
:@

/base_model/batch_normalization_2/ReadVariableOpReadVariableOp&base_model/batch_normalization_2/gamma*
_output_shapes
:@*
dtype0

1base_model/batch_normalization_2/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_2/beta*
dtype0*
_output_shapes
:@
i
&base_model/batch_normalization_2/ConstConst*
dtype0*
_output_shapes
: *
valueB 
k
(base_model/batch_normalization_2/Const_1Const*
valueB *
_output_shapes
: *
dtype0

1base_model/batch_normalization_2/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_2/Conv2D/base_model/batch_normalization_2/ReadVariableOp1base_model/batch_normalization_2/ReadVariableOp_1&base_model/batch_normalization_2/Const(base_model/batch_normalization_2/Const_1*
epsilon%đ'7*
T0*
U0*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:
m
(base_model/batch_normalization_2/Const_2Const*
valueB
 *fff?*
dtype0*
_output_shapes
: 
ź
6base_model/batch_normalization_2/AssignMovingAvg/sub/xConst*
valueB
 *  ?*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean*
dtype0*
_output_shapes
: 
˙
4base_model/batch_normalization_2/AssignMovingAvg/subSub6base_model/batch_normalization_2/AssignMovingAvg/sub/x(base_model/batch_normalization_2/Const_2*
_output_shapes
: *
T0*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean
¨
?base_model/batch_normalization_2/AssignMovingAvg/ReadVariableOpReadVariableOp,base_model/batch_normalization_2/moving_mean*
dtype0*
_output_shapes
:@

6base_model/batch_normalization_2/AssignMovingAvg/sub_1Sub?base_model/batch_normalization_2/AssignMovingAvg/ReadVariableOp3base_model/batch_normalization_2/FusedBatchNormV3:1*
T0*
_output_shapes
:@*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean

4base_model/batch_normalization_2/AssignMovingAvg/mulMul6base_model/batch_normalization_2/AssignMovingAvg/sub_14base_model/batch_normalization_2/AssignMovingAvg/sub*
T0*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean*
_output_shapes
:@

Dbase_model/batch_normalization_2/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp,base_model/batch_normalization_2/moving_mean4base_model/batch_normalization_2/AssignMovingAvg/mul*
dtype0*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean
˛
Abase_model/batch_normalization_2/AssignMovingAvg/ReadVariableOp_1ReadVariableOp,base_model/batch_normalization_2/moving_meanE^base_model/batch_normalization_2/AssignMovingAvg/AssignSubVariableOp*?
_class5
31loc:@base_model/batch_normalization_2/moving_mean*
dtype0*
_output_shapes
:@
Â
8base_model/batch_normalization_2/AssignMovingAvg_1/sub/xConst*
_output_shapes
: *C
_class9
75loc:@base_model/batch_normalization_2/moving_variance*
dtype0*
valueB
 *  ?

6base_model/batch_normalization_2/AssignMovingAvg_1/subSub8base_model/batch_normalization_2/AssignMovingAvg_1/sub/x(base_model/batch_normalization_2/Const_2*C
_class9
75loc:@base_model/batch_normalization_2/moving_variance*
_output_shapes
: *
T0
Ž
Abase_model/batch_normalization_2/AssignMovingAvg_1/ReadVariableOpReadVariableOp0base_model/batch_normalization_2/moving_variance*
dtype0*
_output_shapes
:@
Ą
8base_model/batch_normalization_2/AssignMovingAvg_1/sub_1SubAbase_model/batch_normalization_2/AssignMovingAvg_1/ReadVariableOp3base_model/batch_normalization_2/FusedBatchNormV3:2*
_output_shapes
:@*
T0*C
_class9
75loc:@base_model/batch_normalization_2/moving_variance

6base_model/batch_normalization_2/AssignMovingAvg_1/mulMul8base_model/batch_normalization_2/AssignMovingAvg_1/sub_16base_model/batch_normalization_2/AssignMovingAvg_1/sub*
T0*C
_class9
75loc:@base_model/batch_normalization_2/moving_variance*
_output_shapes
:@

Fbase_model/batch_normalization_2/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0base_model/batch_normalization_2/moving_variance6base_model/batch_normalization_2/AssignMovingAvg_1/mul*
dtype0*C
_class9
75loc:@base_model/batch_normalization_2/moving_variance
ž
Cbase_model/batch_normalization_2/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_2/moving_varianceG^base_model/batch_normalization_2/AssignMovingAvg_1/AssignSubVariableOp*
_output_shapes
:@*
dtype0*C
_class9
75loc:@base_model/batch_normalization_2/moving_variance

base_model/Relu_1Relu1base_model/batch_normalization_2/FusedBatchNormV3*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
Ĺ
=base_model/conv2d_3/kernel/Initializer/truncated_normal/shapeConst*%
valueB"      @   @   *-
_class#
!loc:@base_model/conv2d_3/kernel*
dtype0*
_output_shapes
:
°
<base_model/conv2d_3/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_3/kernel*
dtype0
˛
>base_model/conv2d_3/kernel/Initializer/truncated_normal/stddevConst*
dtype0*
valueB
 *B=*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_3/kernel

Gbase_model/conv2d_3/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_3/kernel/Initializer/truncated_normal/shape*
dtype0*
T0*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_3/kernel
Ť
;base_model/conv2d_3/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_3/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_3/kernel/Initializer/truncated_normal/stddev*
T0*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_3/kernel

7base_model/conv2d_3/kernel/Initializer/truncated_normalAdd;base_model/conv2d_3/kernel/Initializer/truncated_normal/mul<base_model/conv2d_3/kernel/Initializer/truncated_normal/mean*
T0*-
_class#
!loc:@base_model/conv2d_3/kernel*&
_output_shapes
:@@
Ç
base_model/conv2d_3/kernelVarHandleOp*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_3/kernel*
dtype0*
shape:@@*+
shared_namebase_model/conv2d_3/kernel

;base_model/conv2d_3/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_3/kernel*
_output_shapes
: 

!base_model/conv2d_3/kernel/AssignAssignVariableOpbase_model/conv2d_3/kernel7base_model/conv2d_3/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_3/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_3/kernel*&
_output_shapes
:@@*
dtype0
r
!base_model/conv2d_3/dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      

)base_model/conv2d_3/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_3/kernel*
dtype0*&
_output_shapes
:@@
Ö
base_model/conv2d_3/Conv2DConv2Dbase_model/Relu_1)base_model/conv2d_3/Conv2D/ReadVariableOp*
strides
*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0*
paddingSAME
Ŕ
8base_model/batch_normalization_3/gamma/Initializer/zerosConst*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_3/gamma*
valueB@*    *
_output_shapes
:@
ß
&base_model/batch_normalization_3/gammaVarHandleOp*7
shared_name(&base_model/batch_normalization_3/gamma*
_output_shapes
: *
shape:@*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_3/gamma

Gbase_model/batch_normalization_3/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_3/gamma*
_output_shapes
: 
°
-base_model/batch_normalization_3/gamma/AssignAssignVariableOp&base_model/batch_normalization_3/gamma8base_model/batch_normalization_3/gamma/Initializer/zeros*
dtype0

:base_model/batch_normalization_3/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_3/gamma*
dtype0*
_output_shapes
:@
ž
7base_model/batch_normalization_3/beta/Initializer/zerosConst*
dtype0*
_output_shapes
:@*
valueB@*    *8
_class.
,*loc:@base_model/batch_normalization_3/beta
Ü
%base_model/batch_normalization_3/betaVarHandleOp*
dtype0*
shape:@*8
_class.
,*loc:@base_model/batch_normalization_3/beta*
_output_shapes
: *6
shared_name'%base_model/batch_normalization_3/beta

Fbase_model/batch_normalization_3/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_3/beta*
_output_shapes
: 
­
,base_model/batch_normalization_3/beta/AssignAssignVariableOp%base_model/batch_normalization_3/beta7base_model/batch_normalization_3/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_3/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_3/beta*
_output_shapes
:@*
dtype0
Ě
>base_model/batch_normalization_3/moving_mean/Initializer/zerosConst*
valueB@*    *
dtype0*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean*
_output_shapes
:@
ń
,base_model/batch_normalization_3/moving_meanVarHandleOp*
dtype0*
_output_shapes
: *
shape:@*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean*=
shared_name.,base_model/batch_normalization_3/moving_mean
Š
Mbase_model/batch_normalization_3/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_3/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_3/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_3/moving_mean>base_model/batch_normalization_3/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_3/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_3/moving_mean*
_output_shapes
:@*
dtype0
Ó
Abase_model/batch_normalization_3/moving_variance/Initializer/onesConst*C
_class9
75loc:@base_model/batch_normalization_3/moving_variance*
_output_shapes
:@*
dtype0*
valueB@*  ?
ý
0base_model/batch_normalization_3/moving_varianceVarHandleOp*A
shared_name20base_model/batch_normalization_3/moving_variance*
dtype0*
shape:@*
_output_shapes
: *C
_class9
75loc:@base_model/batch_normalization_3/moving_variance
ą
Qbase_model/batch_normalization_3/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_3/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_3/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_3/moving_varianceAbase_model/batch_normalization_3/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_3/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_3/moving_variance*
dtype0*
_output_shapes
:@

/base_model/batch_normalization_3/ReadVariableOpReadVariableOp&base_model/batch_normalization_3/gamma*
dtype0*
_output_shapes
:@

1base_model/batch_normalization_3/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_3/beta*
_output_shapes
:@*
dtype0
i
&base_model/batch_normalization_3/ConstConst*
_output_shapes
: *
dtype0*
valueB 
k
(base_model/batch_normalization_3/Const_1Const*
_output_shapes
: *
valueB *
dtype0

1base_model/batch_normalization_3/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_3/Conv2D/base_model/batch_normalization_3/ReadVariableOp1base_model/batch_normalization_3/ReadVariableOp_1&base_model/batch_normalization_3/Const(base_model/batch_normalization_3/Const_1*
T0*
epsilon%đ'7*
U0*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:
m
(base_model/batch_normalization_3/Const_2Const*
valueB
 *fff?*
dtype0*
_output_shapes
: 
ź
6base_model/batch_normalization_3/AssignMovingAvg/sub/xConst*
valueB
 *  ?*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean*
_output_shapes
: *
dtype0
˙
4base_model/batch_normalization_3/AssignMovingAvg/subSub6base_model/batch_normalization_3/AssignMovingAvg/sub/x(base_model/batch_normalization_3/Const_2*
_output_shapes
: *
T0*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean
¨
?base_model/batch_normalization_3/AssignMovingAvg/ReadVariableOpReadVariableOp,base_model/batch_normalization_3/moving_mean*
_output_shapes
:@*
dtype0

6base_model/batch_normalization_3/AssignMovingAvg/sub_1Sub?base_model/batch_normalization_3/AssignMovingAvg/ReadVariableOp3base_model/batch_normalization_3/FusedBatchNormV3:1*
T0*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean*
_output_shapes
:@

4base_model/batch_normalization_3/AssignMovingAvg/mulMul6base_model/batch_normalization_3/AssignMovingAvg/sub_14base_model/batch_normalization_3/AssignMovingAvg/sub*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean*
_output_shapes
:@*
T0

Dbase_model/batch_normalization_3/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp,base_model/batch_normalization_3/moving_mean4base_model/batch_normalization_3/AssignMovingAvg/mul*
dtype0*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean
˛
Abase_model/batch_normalization_3/AssignMovingAvg/ReadVariableOp_1ReadVariableOp,base_model/batch_normalization_3/moving_meanE^base_model/batch_normalization_3/AssignMovingAvg/AssignSubVariableOp*?
_class5
31loc:@base_model/batch_normalization_3/moving_mean*
_output_shapes
:@*
dtype0
Â
8base_model/batch_normalization_3/AssignMovingAvg_1/sub/xConst*C
_class9
75loc:@base_model/batch_normalization_3/moving_variance*
valueB
 *  ?*
_output_shapes
: *
dtype0

6base_model/batch_normalization_3/AssignMovingAvg_1/subSub8base_model/batch_normalization_3/AssignMovingAvg_1/sub/x(base_model/batch_normalization_3/Const_2*C
_class9
75loc:@base_model/batch_normalization_3/moving_variance*
T0*
_output_shapes
: 
Ž
Abase_model/batch_normalization_3/AssignMovingAvg_1/ReadVariableOpReadVariableOp0base_model/batch_normalization_3/moving_variance*
dtype0*
_output_shapes
:@
Ą
8base_model/batch_normalization_3/AssignMovingAvg_1/sub_1SubAbase_model/batch_normalization_3/AssignMovingAvg_1/ReadVariableOp3base_model/batch_normalization_3/FusedBatchNormV3:2*
T0*
_output_shapes
:@*C
_class9
75loc:@base_model/batch_normalization_3/moving_variance

6base_model/batch_normalization_3/AssignMovingAvg_1/mulMul8base_model/batch_normalization_3/AssignMovingAvg_1/sub_16base_model/batch_normalization_3/AssignMovingAvg_1/sub*
_output_shapes
:@*
T0*C
_class9
75loc:@base_model/batch_normalization_3/moving_variance

Fbase_model/batch_normalization_3/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0base_model/batch_normalization_3/moving_variance6base_model/batch_normalization_3/AssignMovingAvg_1/mul*C
_class9
75loc:@base_model/batch_normalization_3/moving_variance*
dtype0
ž
Cbase_model/batch_normalization_3/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_3/moving_varianceG^base_model/batch_normalization_3/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*C
_class9
75loc:@base_model/batch_normalization_3/moving_variance*
_output_shapes
:@
É
base_model/addAddV21base_model/batch_normalization_3/FusedBatchNormV31base_model/batch_normalization_1/FusedBatchNormV3*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
u
base_model/Relu_2Relubase_model/add*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
Ĺ
=base_model/conv2d_4/kernel/Initializer/truncated_normal/shapeConst*
dtype0*%
valueB"      @   @   *
_output_shapes
:*-
_class#
!loc:@base_model/conv2d_4/kernel
°
<base_model/conv2d_4/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *
dtype0*-
_class#
!loc:@base_model/conv2d_4/kernel*
valueB
 *    
˛
>base_model/conv2d_4/kernel/Initializer/truncated_normal/stddevConst*-
_class#
!loc:@base_model/conv2d_4/kernel*
valueB
 *B=*
dtype0*
_output_shapes
: 

Gbase_model/conv2d_4/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_4/kernel/Initializer/truncated_normal/shape*
T0*
dtype0*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_4/kernel
Ť
;base_model/conv2d_4/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_4/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_4/kernel/Initializer/truncated_normal/stddev*-
_class#
!loc:@base_model/conv2d_4/kernel*&
_output_shapes
:@@*
T0

7base_model/conv2d_4/kernel/Initializer/truncated_normalAdd;base_model/conv2d_4/kernel/Initializer/truncated_normal/mul<base_model/conv2d_4/kernel/Initializer/truncated_normal/mean*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_4/kernel*
T0
Ç
base_model/conv2d_4/kernelVarHandleOp*+
shared_namebase_model/conv2d_4/kernel*
shape:@@*-
_class#
!loc:@base_model/conv2d_4/kernel*
dtype0*
_output_shapes
: 

;base_model/conv2d_4/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_4/kernel*
_output_shapes
: 

!base_model/conv2d_4/kernel/AssignAssignVariableOpbase_model/conv2d_4/kernel7base_model/conv2d_4/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_4/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_4/kernel*
dtype0*&
_output_shapes
:@@
r
!base_model/conv2d_4/dilation_rateConst*
dtype0*
valueB"      *
_output_shapes
:

)base_model/conv2d_4/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_4/kernel*&
_output_shapes
:@@*
dtype0
Ö
base_model/conv2d_4/Conv2DConv2Dbase_model/Relu_2)base_model/conv2d_4/Conv2D/ReadVariableOp*
T0*
strides
*
paddingSAME*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
ż
7base_model/batch_normalization_4/gamma/Initializer/onesConst*9
_class/
-+loc:@base_model/batch_normalization_4/gamma*
dtype0*
_output_shapes
:@*
valueB@*  ?
ß
&base_model/batch_normalization_4/gammaVarHandleOp*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_4/gamma*
shape:@*
_output_shapes
: *7
shared_name(&base_model/batch_normalization_4/gamma

Gbase_model/batch_normalization_4/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_4/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_4/gamma/AssignAssignVariableOp&base_model/batch_normalization_4/gamma7base_model/batch_normalization_4/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_4/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_4/gamma*
_output_shapes
:@*
dtype0
ž
7base_model/batch_normalization_4/beta/Initializer/zerosConst*
dtype0*
_output_shapes
:@*
valueB@*    *8
_class.
,*loc:@base_model/batch_normalization_4/beta
Ü
%base_model/batch_normalization_4/betaVarHandleOp*8
_class.
,*loc:@base_model/batch_normalization_4/beta*
shape:@*
dtype0*
_output_shapes
: *6
shared_name'%base_model/batch_normalization_4/beta

Fbase_model/batch_normalization_4/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_4/beta*
_output_shapes
: 
­
,base_model/batch_normalization_4/beta/AssignAssignVariableOp%base_model/batch_normalization_4/beta7base_model/batch_normalization_4/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_4/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_4/beta*
dtype0*
_output_shapes
:@
Ě
>base_model/batch_normalization_4/moving_mean/Initializer/zerosConst*
_output_shapes
:@*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean*
dtype0*
valueB@*    
ń
,base_model/batch_normalization_4/moving_meanVarHandleOp*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean*
shape:@*=
shared_name.,base_model/batch_normalization_4/moving_mean*
_output_shapes
: *
dtype0
Š
Mbase_model/batch_normalization_4/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_4/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_4/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_4/moving_mean>base_model/batch_normalization_4/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_4/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_4/moving_mean*
dtype0*
_output_shapes
:@
Ó
Abase_model/batch_normalization_4/moving_variance/Initializer/onesConst*
_output_shapes
:@*
dtype0*
valueB@*  ?*C
_class9
75loc:@base_model/batch_normalization_4/moving_variance
ý
0base_model/batch_normalization_4/moving_varianceVarHandleOp*A
shared_name20base_model/batch_normalization_4/moving_variance*
shape:@*
_output_shapes
: *C
_class9
75loc:@base_model/batch_normalization_4/moving_variance*
dtype0
ą
Qbase_model/batch_normalization_4/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_4/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_4/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_4/moving_varianceAbase_model/batch_normalization_4/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_4/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_4/moving_variance*
_output_shapes
:@*
dtype0

/base_model/batch_normalization_4/ReadVariableOpReadVariableOp&base_model/batch_normalization_4/gamma*
_output_shapes
:@*
dtype0

1base_model/batch_normalization_4/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_4/beta*
_output_shapes
:@*
dtype0
i
&base_model/batch_normalization_4/ConstConst*
dtype0*
valueB *
_output_shapes
: 
k
(base_model/batch_normalization_4/Const_1Const*
_output_shapes
: *
dtype0*
valueB 

1base_model/batch_normalization_4/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_4/Conv2D/base_model/batch_normalization_4/ReadVariableOp1base_model/batch_normalization_4/ReadVariableOp_1&base_model/batch_normalization_4/Const(base_model/batch_normalization_4/Const_1*
epsilon%đ'7*
T0*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:*
U0
m
(base_model/batch_normalization_4/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *fff?
ź
6base_model/batch_normalization_4/AssignMovingAvg/sub/xConst*
dtype0*
valueB
 *  ?*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean*
_output_shapes
: 
˙
4base_model/batch_normalization_4/AssignMovingAvg/subSub6base_model/batch_normalization_4/AssignMovingAvg/sub/x(base_model/batch_normalization_4/Const_2*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean*
T0*
_output_shapes
: 
¨
?base_model/batch_normalization_4/AssignMovingAvg/ReadVariableOpReadVariableOp,base_model/batch_normalization_4/moving_mean*
_output_shapes
:@*
dtype0

6base_model/batch_normalization_4/AssignMovingAvg/sub_1Sub?base_model/batch_normalization_4/AssignMovingAvg/ReadVariableOp3base_model/batch_normalization_4/FusedBatchNormV3:1*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean*
T0*
_output_shapes
:@

4base_model/batch_normalization_4/AssignMovingAvg/mulMul6base_model/batch_normalization_4/AssignMovingAvg/sub_14base_model/batch_normalization_4/AssignMovingAvg/sub*
_output_shapes
:@*
T0*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean

Dbase_model/batch_normalization_4/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp,base_model/batch_normalization_4/moving_mean4base_model/batch_normalization_4/AssignMovingAvg/mul*
dtype0*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean
˛
Abase_model/batch_normalization_4/AssignMovingAvg/ReadVariableOp_1ReadVariableOp,base_model/batch_normalization_4/moving_meanE^base_model/batch_normalization_4/AssignMovingAvg/AssignSubVariableOp*
_output_shapes
:@*?
_class5
31loc:@base_model/batch_normalization_4/moving_mean*
dtype0
Â
8base_model/batch_normalization_4/AssignMovingAvg_1/sub/xConst*
dtype0*C
_class9
75loc:@base_model/batch_normalization_4/moving_variance*
_output_shapes
: *
valueB
 *  ?

6base_model/batch_normalization_4/AssignMovingAvg_1/subSub8base_model/batch_normalization_4/AssignMovingAvg_1/sub/x(base_model/batch_normalization_4/Const_2*
T0*C
_class9
75loc:@base_model/batch_normalization_4/moving_variance*
_output_shapes
: 
Ž
Abase_model/batch_normalization_4/AssignMovingAvg_1/ReadVariableOpReadVariableOp0base_model/batch_normalization_4/moving_variance*
dtype0*
_output_shapes
:@
Ą
8base_model/batch_normalization_4/AssignMovingAvg_1/sub_1SubAbase_model/batch_normalization_4/AssignMovingAvg_1/ReadVariableOp3base_model/batch_normalization_4/FusedBatchNormV3:2*
T0*
_output_shapes
:@*C
_class9
75loc:@base_model/batch_normalization_4/moving_variance

6base_model/batch_normalization_4/AssignMovingAvg_1/mulMul8base_model/batch_normalization_4/AssignMovingAvg_1/sub_16base_model/batch_normalization_4/AssignMovingAvg_1/sub*
T0*
_output_shapes
:@*C
_class9
75loc:@base_model/batch_normalization_4/moving_variance

Fbase_model/batch_normalization_4/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0base_model/batch_normalization_4/moving_variance6base_model/batch_normalization_4/AssignMovingAvg_1/mul*C
_class9
75loc:@base_model/batch_normalization_4/moving_variance*
dtype0
ž
Cbase_model/batch_normalization_4/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_4/moving_varianceG^base_model/batch_normalization_4/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*
_output_shapes
:@*C
_class9
75loc:@base_model/batch_normalization_4/moving_variance

base_model/Relu_3Relu1base_model/batch_normalization_4/FusedBatchNormV3*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
Ĺ
=base_model/conv2d_5/kernel/Initializer/truncated_normal/shapeConst*-
_class#
!loc:@base_model/conv2d_5/kernel*
_output_shapes
:*%
valueB"      @   @   *
dtype0
°
<base_model/conv2d_5/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
dtype0*-
_class#
!loc:@base_model/conv2d_5/kernel*
_output_shapes
: 
˛
>base_model/conv2d_5/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *B=*-
_class#
!loc:@base_model/conv2d_5/kernel

Gbase_model/conv2d_5/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_5/kernel/Initializer/truncated_normal/shape*-
_class#
!loc:@base_model/conv2d_5/kernel*&
_output_shapes
:@@*
T0*
dtype0
Ť
;base_model/conv2d_5/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_5/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_5/kernel/Initializer/truncated_normal/stddev*&
_output_shapes
:@@*
T0*-
_class#
!loc:@base_model/conv2d_5/kernel

7base_model/conv2d_5/kernel/Initializer/truncated_normalAdd;base_model/conv2d_5/kernel/Initializer/truncated_normal/mul<base_model/conv2d_5/kernel/Initializer/truncated_normal/mean*
T0*&
_output_shapes
:@@*-
_class#
!loc:@base_model/conv2d_5/kernel
Ç
base_model/conv2d_5/kernelVarHandleOp*
_output_shapes
: *
shape:@@*+
shared_namebase_model/conv2d_5/kernel*-
_class#
!loc:@base_model/conv2d_5/kernel*
dtype0

;base_model/conv2d_5/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_5/kernel*
_output_shapes
: 

!base_model/conv2d_5/kernel/AssignAssignVariableOpbase_model/conv2d_5/kernel7base_model/conv2d_5/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_5/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_5/kernel*
dtype0*&
_output_shapes
:@@
r
!base_model/conv2d_5/dilation_rateConst*
valueB"      *
_output_shapes
:*
dtype0

)base_model/conv2d_5/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_5/kernel*
dtype0*&
_output_shapes
:@@
Ö
base_model/conv2d_5/Conv2DConv2Dbase_model/Relu_3)base_model/conv2d_5/Conv2D/ReadVariableOp*
strides
*
T0*
paddingSAME*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@
Ŕ
8base_model/batch_normalization_5/gamma/Initializer/zerosConst*
_output_shapes
:@*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_5/gamma*
valueB@*    
ß
&base_model/batch_normalization_5/gammaVarHandleOp*
dtype0*7
shared_name(&base_model/batch_normalization_5/gamma*
shape:@*
_output_shapes
: *9
_class/
-+loc:@base_model/batch_normalization_5/gamma

Gbase_model/batch_normalization_5/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_5/gamma*
_output_shapes
: 
°
-base_model/batch_normalization_5/gamma/AssignAssignVariableOp&base_model/batch_normalization_5/gamma8base_model/batch_normalization_5/gamma/Initializer/zeros*
dtype0

:base_model/batch_normalization_5/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_5/gamma*
dtype0*
_output_shapes
:@
ž
7base_model/batch_normalization_5/beta/Initializer/zerosConst*
valueB@*    *
_output_shapes
:@*8
_class.
,*loc:@base_model/batch_normalization_5/beta*
dtype0
Ü
%base_model/batch_normalization_5/betaVarHandleOp*8
_class.
,*loc:@base_model/batch_normalization_5/beta*
shape:@*
_output_shapes
: *
dtype0*6
shared_name'%base_model/batch_normalization_5/beta

Fbase_model/batch_normalization_5/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_5/beta*
_output_shapes
: 
­
,base_model/batch_normalization_5/beta/AssignAssignVariableOp%base_model/batch_normalization_5/beta7base_model/batch_normalization_5/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_5/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_5/beta*
_output_shapes
:@*
dtype0
Ě
>base_model/batch_normalization_5/moving_mean/Initializer/zerosConst*?
_class5
31loc:@base_model/batch_normalization_5/moving_mean*
dtype0*
valueB@*    *
_output_shapes
:@
ń
,base_model/batch_normalization_5/moving_meanVarHandleOp*?
_class5
31loc:@base_model/batch_normalization_5/moving_mean*
shape:@*=
shared_name.,base_model/batch_normalization_5/moving_mean*
dtype0*
_output_shapes
: 
Š
Mbase_model/batch_normalization_5/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_5/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_5/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_5/moving_mean>base_model/batch_normalization_5/moving_mean/Initializer/zeros*
dtype0
Š
@base_model/batch_normalization_5/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_5/moving_mean*
_output_shapes
:@*
dtype0
Ó
Abase_model/batch_normalization_5/moving_variance/Initializer/onesConst*
valueB@*  ?*
_output_shapes
:@*
dtype0*C
_class9
75loc:@base_model/batch_normalization_5/moving_variance
ý
0base_model/batch_normalization_5/moving_varianceVarHandleOp*
_output_shapes
: *C
_class9
75loc:@base_model/batch_normalization_5/moving_variance*
dtype0*A
shared_name20base_model/batch_normalization_5/moving_variance*
shape:@
ą
Qbase_model/batch_normalization_5/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_5/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_5/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_5/moving_varianceAbase_model/batch_normalization_5/moving_variance/Initializer/ones*
dtype0
ą
Dbase_model/batch_normalization_5/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_5/moving_variance*
dtype0*
_output_shapes
:@

/base_model/batch_normalization_5/ReadVariableOpReadVariableOp&base_model/batch_normalization_5/gamma*
_output_shapes
:@*
dtype0

1base_model/batch_normalization_5/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_5/beta*
dtype0*
_output_shapes
:@
i
&base_model/batch_normalization_5/ConstConst*
_output_shapes
: *
valueB *
dtype0
k
(base_model/batch_normalization_5/Const_1Const*
_output_shapes
: *
valueB *
dtype0

1base_model/batch_normalization_5/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_5/Conv2D/base_model/batch_normalization_5/ReadVariableOp1base_model/batch_normalization_5/ReadVariableOp_1&base_model/batch_normalization_5/Const(base_model/batch_normalization_5/Const_1*
T0*
U0*
epsilon%đ'7*]
_output_shapesK
I:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@:@:@:@:@:
m
(base_model/batch_normalization_5/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *fff?
ź
6base_model/batch_normalization_5/AssignMovingAvg/sub/xConst*
valueB
 *  ?*
dtype0*?
_class5
31loc:@base_model/batch_normalization_5/moving_mean*
_output_shapes
: 
˙
4base_model/batch_normalization_5/AssignMovingAvg/subSub6base_model/batch_normalization_5/AssignMovingAvg/sub/x(base_model/batch_normalization_5/Const_2*
_output_shapes
: *
T0*?
_class5
31loc:@base_model/batch_normalization_5/moving_mean
¨
?base_model/batch_normalization_5/AssignMovingAvg/ReadVariableOpReadVariableOp,base_model/batch_normalization_5/moving_mean*
_output_shapes
:@*
dtype0

6base_model/batch_normalization_5/AssignMovingAvg/sub_1Sub?base_model/batch_normalization_5/AssignMovingAvg/ReadVariableOp3base_model/batch_normalization_5/FusedBatchNormV3:1*
T0*
_output_shapes
:@*?
_class5
31loc:@base_model/batch_normalization_5/moving_mean

4base_model/batch_normalization_5/AssignMovingAvg/mulMul6base_model/batch_normalization_5/AssignMovingAvg/sub_14base_model/batch_normalization_5/AssignMovingAvg/sub*?
_class5
31loc:@base_model/batch_normalization_5/moving_mean*
T0*
_output_shapes
:@

Dbase_model/batch_normalization_5/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp,base_model/batch_normalization_5/moving_mean4base_model/batch_normalization_5/AssignMovingAvg/mul*?
_class5
31loc:@base_model/batch_normalization_5/moving_mean*
dtype0
˛
Abase_model/batch_normalization_5/AssignMovingAvg/ReadVariableOp_1ReadVariableOp,base_model/batch_normalization_5/moving_meanE^base_model/batch_normalization_5/AssignMovingAvg/AssignSubVariableOp*?
_class5
31loc:@base_model/batch_normalization_5/moving_mean*
dtype0*
_output_shapes
:@
Â
8base_model/batch_normalization_5/AssignMovingAvg_1/sub/xConst*
dtype0*C
_class9
75loc:@base_model/batch_normalization_5/moving_variance*
_output_shapes
: *
valueB
 *  ?

6base_model/batch_normalization_5/AssignMovingAvg_1/subSub8base_model/batch_normalization_5/AssignMovingAvg_1/sub/x(base_model/batch_normalization_5/Const_2*C
_class9
75loc:@base_model/batch_normalization_5/moving_variance*
T0*
_output_shapes
: 
Ž
Abase_model/batch_normalization_5/AssignMovingAvg_1/ReadVariableOpReadVariableOp0base_model/batch_normalization_5/moving_variance*
dtype0*
_output_shapes
:@
Ą
8base_model/batch_normalization_5/AssignMovingAvg_1/sub_1SubAbase_model/batch_normalization_5/AssignMovingAvg_1/ReadVariableOp3base_model/batch_normalization_5/FusedBatchNormV3:2*
_output_shapes
:@*C
_class9
75loc:@base_model/batch_normalization_5/moving_variance*
T0

6base_model/batch_normalization_5/AssignMovingAvg_1/mulMul8base_model/batch_normalization_5/AssignMovingAvg_1/sub_16base_model/batch_normalization_5/AssignMovingAvg_1/sub*
_output_shapes
:@*
T0*C
_class9
75loc:@base_model/batch_normalization_5/moving_variance

Fbase_model/batch_normalization_5/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0base_model/batch_normalization_5/moving_variance6base_model/batch_normalization_5/AssignMovingAvg_1/mul*C
_class9
75loc:@base_model/batch_normalization_5/moving_variance*
dtype0
ž
Cbase_model/batch_normalization_5/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_5/moving_varianceG^base_model/batch_normalization_5/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*
_output_shapes
:@*C
_class9
75loc:@base_model/batch_normalization_5/moving_variance
Ť
base_model/add_1AddV21base_model/batch_normalization_5/FusedBatchNormV3base_model/Relu_2*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
w
base_model/Relu_4Relubase_model/add_1*
T0*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@

base_model/block_group1Identitybase_model/Relu_4*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0

base_model/Pad_1/paddingsConst*9
value0B."                                 *
_output_shapes

:*
dtype0

base_model/Pad_1Padbase_model/block_group1base_model/Pad_1/paddings*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
Ĺ
=base_model/conv2d_6/kernel/Initializer/truncated_normal/shapeConst*
_output_shapes
:*
dtype0*-
_class#
!loc:@base_model/conv2d_6/kernel*%
valueB"      @      
°
<base_model/conv2d_6/kernel/Initializer/truncated_normal/meanConst*
dtype0*-
_class#
!loc:@base_model/conv2d_6/kernel*
_output_shapes
: *
valueB
 *    
˛
>base_model/conv2d_6/kernel/Initializer/truncated_normal/stddevConst*-
_class#
!loc:@base_model/conv2d_6/kernel*
_output_shapes
: *
valueB
 *6>*
dtype0

Gbase_model/conv2d_6/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_6/kernel/Initializer/truncated_normal/shape*-
_class#
!loc:@base_model/conv2d_6/kernel*
T0*'
_output_shapes
:@*
dtype0
Ź
;base_model/conv2d_6/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_6/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_6/kernel/Initializer/truncated_normal/stddev*'
_output_shapes
:@*
T0*-
_class#
!loc:@base_model/conv2d_6/kernel

7base_model/conv2d_6/kernel/Initializer/truncated_normalAdd;base_model/conv2d_6/kernel/Initializer/truncated_normal/mul<base_model/conv2d_6/kernel/Initializer/truncated_normal/mean*
T0*'
_output_shapes
:@*-
_class#
!loc:@base_model/conv2d_6/kernel
Č
base_model/conv2d_6/kernelVarHandleOp*
shape:@*
_output_shapes
: *+
shared_namebase_model/conv2d_6/kernel*
dtype0*-
_class#
!loc:@base_model/conv2d_6/kernel

;base_model/conv2d_6/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_6/kernel*
_output_shapes
: 

!base_model/conv2d_6/kernel/AssignAssignVariableOpbase_model/conv2d_6/kernel7base_model/conv2d_6/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_6/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_6/kernel*
dtype0*'
_output_shapes
:@
r
!base_model/conv2d_6/dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      

)base_model/conv2d_6/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_6/kernel*'
_output_shapes
:@*
dtype0
×
base_model/conv2d_6/Conv2DConv2Dbase_model/Pad_1)base_model/conv2d_6/Conv2D/ReadVariableOp*
paddingVALID*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
strides

Á
7base_model/batch_normalization_6/gamma/Initializer/onesConst*9
_class/
-+loc:@base_model/batch_normalization_6/gamma*
_output_shapes	
:*
dtype0*
valueB*  ?
ŕ
&base_model/batch_normalization_6/gammaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_6/gamma*
shape:*7
shared_name(&base_model/batch_normalization_6/gamma*
_output_shapes
: *
dtype0

Gbase_model/batch_normalization_6/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_6/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_6/gamma/AssignAssignVariableOp&base_model/batch_normalization_6/gamma7base_model/batch_normalization_6/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_6/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_6/gamma*
dtype0*
_output_shapes	
:
Ŕ
7base_model/batch_normalization_6/beta/Initializer/zerosConst*
_output_shapes	
:*
dtype0*8
_class.
,*loc:@base_model/batch_normalization_6/beta*
valueB*    
Ý
%base_model/batch_normalization_6/betaVarHandleOp*
shape:*6
shared_name'%base_model/batch_normalization_6/beta*8
_class.
,*loc:@base_model/batch_normalization_6/beta*
dtype0*
_output_shapes
: 

Fbase_model/batch_normalization_6/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_6/beta*
_output_shapes
: 
­
,base_model/batch_normalization_6/beta/AssignAssignVariableOp%base_model/batch_normalization_6/beta7base_model/batch_normalization_6/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_6/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_6/beta*
dtype0*
_output_shapes	
:
Î
>base_model/batch_normalization_6/moving_mean/Initializer/zerosConst*
valueB*    *?
_class5
31loc:@base_model/batch_normalization_6/moving_mean*
_output_shapes	
:*
dtype0
ň
,base_model/batch_normalization_6/moving_meanVarHandleOp*
_output_shapes
: *=
shared_name.,base_model/batch_normalization_6/moving_mean*
dtype0*?
_class5
31loc:@base_model/batch_normalization_6/moving_mean*
shape:
Š
Mbase_model/batch_normalization_6/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_6/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_6/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_6/moving_mean>base_model/batch_normalization_6/moving_mean/Initializer/zeros*
dtype0
Ş
@base_model/batch_normalization_6/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_6/moving_mean*
dtype0*
_output_shapes	
:
Ő
Abase_model/batch_normalization_6/moving_variance/Initializer/onesConst*C
_class9
75loc:@base_model/batch_normalization_6/moving_variance*
dtype0*
valueB*  ?*
_output_shapes	
:
ţ
0base_model/batch_normalization_6/moving_varianceVarHandleOp*
shape:*
_output_shapes
: *
dtype0*A
shared_name20base_model/batch_normalization_6/moving_variance*C
_class9
75loc:@base_model/batch_normalization_6/moving_variance
ą
Qbase_model/batch_normalization_6/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_6/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_6/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_6/moving_varianceAbase_model/batch_normalization_6/moving_variance/Initializer/ones*
dtype0
˛
Dbase_model/batch_normalization_6/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_6/moving_variance*
_output_shapes	
:*
dtype0

/base_model/batch_normalization_6/ReadVariableOpReadVariableOp&base_model/batch_normalization_6/gamma*
dtype0*
_output_shapes	
:

1base_model/batch_normalization_6/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_6/beta*
_output_shapes	
:*
dtype0
i
&base_model/batch_normalization_6/ConstConst*
dtype0*
valueB *
_output_shapes
: 
k
(base_model/batch_normalization_6/Const_1Const*
_output_shapes
: *
valueB *
dtype0

1base_model/batch_normalization_6/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_6/Conv2D/base_model/batch_normalization_6/ReadVariableOp1base_model/batch_normalization_6/ReadVariableOp_1&base_model/batch_normalization_6/Const(base_model/batch_normalization_6/Const_1*
U0*
epsilon%đ'7*
T0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
m
(base_model/batch_normalization_6/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *fff?
ź
6base_model/batch_normalization_6/AssignMovingAvg/sub/xConst*?
_class5
31loc:@base_model/batch_normalization_6/moving_mean*
valueB
 *  ?*
dtype0*
_output_shapes
: 
˙
4base_model/batch_normalization_6/AssignMovingAvg/subSub6base_model/batch_normalization_6/AssignMovingAvg/sub/x(base_model/batch_normalization_6/Const_2*
T0*?
_class5
31loc:@base_model/batch_normalization_6/moving_mean*
_output_shapes
: 
Š
?base_model/batch_normalization_6/AssignMovingAvg/ReadVariableOpReadVariableOp,base_model/batch_normalization_6/moving_mean*
dtype0*
_output_shapes	
:

6base_model/batch_normalization_6/AssignMovingAvg/sub_1Sub?base_model/batch_normalization_6/AssignMovingAvg/ReadVariableOp3base_model/batch_normalization_6/FusedBatchNormV3:1*?
_class5
31loc:@base_model/batch_normalization_6/moving_mean*
T0*
_output_shapes	
:

4base_model/batch_normalization_6/AssignMovingAvg/mulMul6base_model/batch_normalization_6/AssignMovingAvg/sub_14base_model/batch_normalization_6/AssignMovingAvg/sub*
_output_shapes	
:*
T0*?
_class5
31loc:@base_model/batch_normalization_6/moving_mean

Dbase_model/batch_normalization_6/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp,base_model/batch_normalization_6/moving_mean4base_model/batch_normalization_6/AssignMovingAvg/mul*?
_class5
31loc:@base_model/batch_normalization_6/moving_mean*
dtype0
ł
Abase_model/batch_normalization_6/AssignMovingAvg/ReadVariableOp_1ReadVariableOp,base_model/batch_normalization_6/moving_meanE^base_model/batch_normalization_6/AssignMovingAvg/AssignSubVariableOp*
_output_shapes	
:*?
_class5
31loc:@base_model/batch_normalization_6/moving_mean*
dtype0
Â
8base_model/batch_normalization_6/AssignMovingAvg_1/sub/xConst*
_output_shapes
: *C
_class9
75loc:@base_model/batch_normalization_6/moving_variance*
valueB
 *  ?*
dtype0

6base_model/batch_normalization_6/AssignMovingAvg_1/subSub8base_model/batch_normalization_6/AssignMovingAvg_1/sub/x(base_model/batch_normalization_6/Const_2*
T0*C
_class9
75loc:@base_model/batch_normalization_6/moving_variance*
_output_shapes
: 
Ż
Abase_model/batch_normalization_6/AssignMovingAvg_1/ReadVariableOpReadVariableOp0base_model/batch_normalization_6/moving_variance*
dtype0*
_output_shapes	
:
˘
8base_model/batch_normalization_6/AssignMovingAvg_1/sub_1SubAbase_model/batch_normalization_6/AssignMovingAvg_1/ReadVariableOp3base_model/batch_normalization_6/FusedBatchNormV3:2*
T0*C
_class9
75loc:@base_model/batch_normalization_6/moving_variance*
_output_shapes	
:

6base_model/batch_normalization_6/AssignMovingAvg_1/mulMul8base_model/batch_normalization_6/AssignMovingAvg_1/sub_16base_model/batch_normalization_6/AssignMovingAvg_1/sub*C
_class9
75loc:@base_model/batch_normalization_6/moving_variance*
T0*
_output_shapes	
:

Fbase_model/batch_normalization_6/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0base_model/batch_normalization_6/moving_variance6base_model/batch_normalization_6/AssignMovingAvg_1/mul*
dtype0*C
_class9
75loc:@base_model/batch_normalization_6/moving_variance
ż
Cbase_model/batch_normalization_6/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_6/moving_varianceG^base_model/batch_normalization_6/AssignMovingAvg_1/AssignSubVariableOp*
_output_shapes	
:*
dtype0*C
_class9
75loc:@base_model/batch_normalization_6/moving_variance

base_model/Pad_2/paddingsConst*9
value0B."                             *
_output_shapes

:*
dtype0

base_model/Pad_2Padbase_model/block_group1base_model/Pad_2/paddings*A
_output_shapes/
-:+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@*
T0
Ĺ
=base_model/conv2d_7/kernel/Initializer/truncated_normal/shapeConst*-
_class#
!loc:@base_model/conv2d_7/kernel*%
valueB"      @      *
dtype0*
_output_shapes
:
°
<base_model/conv2d_7/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
_output_shapes
: *
dtype0*-
_class#
!loc:@base_model/conv2d_7/kernel
˛
>base_model/conv2d_7/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
valueB
 *B=*
dtype0*-
_class#
!loc:@base_model/conv2d_7/kernel

Gbase_model/conv2d_7/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_7/kernel/Initializer/truncated_normal/shape*
dtype0*'
_output_shapes
:@*-
_class#
!loc:@base_model/conv2d_7/kernel*
T0
Ź
;base_model/conv2d_7/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_7/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_7/kernel/Initializer/truncated_normal/stddev*
T0*'
_output_shapes
:@*-
_class#
!loc:@base_model/conv2d_7/kernel

7base_model/conv2d_7/kernel/Initializer/truncated_normalAdd;base_model/conv2d_7/kernel/Initializer/truncated_normal/mul<base_model/conv2d_7/kernel/Initializer/truncated_normal/mean*
T0*-
_class#
!loc:@base_model/conv2d_7/kernel*'
_output_shapes
:@
Č
base_model/conv2d_7/kernelVarHandleOp*+
shared_namebase_model/conv2d_7/kernel*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_7/kernel*
shape:@*
dtype0

;base_model/conv2d_7/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_7/kernel*
_output_shapes
: 

!base_model/conv2d_7/kernel/AssignAssignVariableOpbase_model/conv2d_7/kernel7base_model/conv2d_7/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_7/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_7/kernel*
dtype0*'
_output_shapes
:@
r
!base_model/conv2d_7/dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      

)base_model/conv2d_7/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_7/kernel*
dtype0*'
_output_shapes
:@
×
base_model/conv2d_7/Conv2DConv2Dbase_model/Pad_2)base_model/conv2d_7/Conv2D/ReadVariableOp*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingVALID*
T0
Á
7base_model/batch_normalization_7/gamma/Initializer/onesConst*
dtype0*
valueB*  ?*
_output_shapes	
:*9
_class/
-+loc:@base_model/batch_normalization_7/gamma
ŕ
&base_model/batch_normalization_7/gammaVarHandleOp*7
shared_name(&base_model/batch_normalization_7/gamma*
dtype0*
_output_shapes
: *
shape:*9
_class/
-+loc:@base_model/batch_normalization_7/gamma

Gbase_model/batch_normalization_7/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_7/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_7/gamma/AssignAssignVariableOp&base_model/batch_normalization_7/gamma7base_model/batch_normalization_7/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_7/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_7/gamma*
_output_shapes	
:*
dtype0
Ŕ
7base_model/batch_normalization_7/beta/Initializer/zerosConst*
_output_shapes	
:*
valueB*    *
dtype0*8
_class.
,*loc:@base_model/batch_normalization_7/beta
Ý
%base_model/batch_normalization_7/betaVarHandleOp*8
_class.
,*loc:@base_model/batch_normalization_7/beta*
_output_shapes
: *
shape:*
dtype0*6
shared_name'%base_model/batch_normalization_7/beta

Fbase_model/batch_normalization_7/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_7/beta*
_output_shapes
: 
­
,base_model/batch_normalization_7/beta/AssignAssignVariableOp%base_model/batch_normalization_7/beta7base_model/batch_normalization_7/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_7/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_7/beta*
dtype0*
_output_shapes	
:
Î
>base_model/batch_normalization_7/moving_mean/Initializer/zerosConst*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean*
valueB*    *
_output_shapes	
:*
dtype0
ň
,base_model/batch_normalization_7/moving_meanVarHandleOp*
shape:*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean*=
shared_name.,base_model/batch_normalization_7/moving_mean*
dtype0*
_output_shapes
: 
Š
Mbase_model/batch_normalization_7/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_7/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_7/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_7/moving_mean>base_model/batch_normalization_7/moving_mean/Initializer/zeros*
dtype0
Ş
@base_model/batch_normalization_7/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_7/moving_mean*
_output_shapes	
:*
dtype0
Ő
Abase_model/batch_normalization_7/moving_variance/Initializer/onesConst*
_output_shapes	
:*
dtype0*
valueB*  ?*C
_class9
75loc:@base_model/batch_normalization_7/moving_variance
ţ
0base_model/batch_normalization_7/moving_varianceVarHandleOp*
_output_shapes
: *
shape:*A
shared_name20base_model/batch_normalization_7/moving_variance*
dtype0*C
_class9
75loc:@base_model/batch_normalization_7/moving_variance
ą
Qbase_model/batch_normalization_7/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_7/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_7/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_7/moving_varianceAbase_model/batch_normalization_7/moving_variance/Initializer/ones*
dtype0
˛
Dbase_model/batch_normalization_7/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_7/moving_variance*
dtype0*
_output_shapes	
:

/base_model/batch_normalization_7/ReadVariableOpReadVariableOp&base_model/batch_normalization_7/gamma*
_output_shapes	
:*
dtype0

1base_model/batch_normalization_7/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_7/beta*
dtype0*
_output_shapes	
:
i
&base_model/batch_normalization_7/ConstConst*
dtype0*
_output_shapes
: *
valueB 
k
(base_model/batch_normalization_7/Const_1Const*
valueB *
_output_shapes
: *
dtype0

1base_model/batch_normalization_7/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_7/Conv2D/base_model/batch_normalization_7/ReadVariableOp1base_model/batch_normalization_7/ReadVariableOp_1&base_model/batch_normalization_7/Const(base_model/batch_normalization_7/Const_1*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
epsilon%đ'7*
U0*
T0
m
(base_model/batch_normalization_7/Const_2Const*
_output_shapes
: *
valueB
 *fff?*
dtype0
ź
6base_model/batch_normalization_7/AssignMovingAvg/sub/xConst*
valueB
 *  ?*
_output_shapes
: *
dtype0*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean
˙
4base_model/batch_normalization_7/AssignMovingAvg/subSub6base_model/batch_normalization_7/AssignMovingAvg/sub/x(base_model/batch_normalization_7/Const_2*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean*
_output_shapes
: *
T0
Š
?base_model/batch_normalization_7/AssignMovingAvg/ReadVariableOpReadVariableOp,base_model/batch_normalization_7/moving_mean*
_output_shapes	
:*
dtype0

6base_model/batch_normalization_7/AssignMovingAvg/sub_1Sub?base_model/batch_normalization_7/AssignMovingAvg/ReadVariableOp3base_model/batch_normalization_7/FusedBatchNormV3:1*
T0*
_output_shapes	
:*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean

4base_model/batch_normalization_7/AssignMovingAvg/mulMul6base_model/batch_normalization_7/AssignMovingAvg/sub_14base_model/batch_normalization_7/AssignMovingAvg/sub*
_output_shapes	
:*
T0*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean

Dbase_model/batch_normalization_7/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp,base_model/batch_normalization_7/moving_mean4base_model/batch_normalization_7/AssignMovingAvg/mul*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean*
dtype0
ł
Abase_model/batch_normalization_7/AssignMovingAvg/ReadVariableOp_1ReadVariableOp,base_model/batch_normalization_7/moving_meanE^base_model/batch_normalization_7/AssignMovingAvg/AssignSubVariableOp*?
_class5
31loc:@base_model/batch_normalization_7/moving_mean*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_7/AssignMovingAvg_1/sub/xConst*
valueB
 *  ?*
dtype0*
_output_shapes
: *C
_class9
75loc:@base_model/batch_normalization_7/moving_variance

6base_model/batch_normalization_7/AssignMovingAvg_1/subSub8base_model/batch_normalization_7/AssignMovingAvg_1/sub/x(base_model/batch_normalization_7/Const_2*C
_class9
75loc:@base_model/batch_normalization_7/moving_variance*
_output_shapes
: *
T0
Ż
Abase_model/batch_normalization_7/AssignMovingAvg_1/ReadVariableOpReadVariableOp0base_model/batch_normalization_7/moving_variance*
_output_shapes	
:*
dtype0
˘
8base_model/batch_normalization_7/AssignMovingAvg_1/sub_1SubAbase_model/batch_normalization_7/AssignMovingAvg_1/ReadVariableOp3base_model/batch_normalization_7/FusedBatchNormV3:2*C
_class9
75loc:@base_model/batch_normalization_7/moving_variance*
_output_shapes	
:*
T0

6base_model/batch_normalization_7/AssignMovingAvg_1/mulMul8base_model/batch_normalization_7/AssignMovingAvg_1/sub_16base_model/batch_normalization_7/AssignMovingAvg_1/sub*
T0*
_output_shapes	
:*C
_class9
75loc:@base_model/batch_normalization_7/moving_variance

Fbase_model/batch_normalization_7/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0base_model/batch_normalization_7/moving_variance6base_model/batch_normalization_7/AssignMovingAvg_1/mul*C
_class9
75loc:@base_model/batch_normalization_7/moving_variance*
dtype0
ż
Cbase_model/batch_normalization_7/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_7/moving_varianceG^base_model/batch_normalization_7/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*
_output_shapes	
:*C
_class9
75loc:@base_model/batch_normalization_7/moving_variance

base_model/Relu_5Relu1base_model/batch_normalization_7/FusedBatchNormV3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ĺ
=base_model/conv2d_8/kernel/Initializer/truncated_normal/shapeConst*
dtype0*
_output_shapes
:*%
valueB"            *-
_class#
!loc:@base_model/conv2d_8/kernel
°
<base_model/conv2d_8/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *
dtype0*
valueB
 *    *-
_class#
!loc:@base_model/conv2d_8/kernel
˛
>base_model/conv2d_8/kernel/Initializer/truncated_normal/stddevConst*
dtype0*
_output_shapes
: *
valueB
 *¸1	=*-
_class#
!loc:@base_model/conv2d_8/kernel

Gbase_model/conv2d_8/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_8/kernel/Initializer/truncated_normal/shape*
T0*-
_class#
!loc:@base_model/conv2d_8/kernel*
dtype0*(
_output_shapes
:
­
;base_model/conv2d_8/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_8/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_8/kernel/Initializer/truncated_normal/stddev*
T0*-
_class#
!loc:@base_model/conv2d_8/kernel*(
_output_shapes
:

7base_model/conv2d_8/kernel/Initializer/truncated_normalAdd;base_model/conv2d_8/kernel/Initializer/truncated_normal/mul<base_model/conv2d_8/kernel/Initializer/truncated_normal/mean*(
_output_shapes
:*
T0*-
_class#
!loc:@base_model/conv2d_8/kernel
É
base_model/conv2d_8/kernelVarHandleOp*+
shared_namebase_model/conv2d_8/kernel*
_output_shapes
: *-
_class#
!loc:@base_model/conv2d_8/kernel*
shape:*
dtype0

;base_model/conv2d_8/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_8/kernel*
_output_shapes
: 

!base_model/conv2d_8/kernel/AssignAssignVariableOpbase_model/conv2d_8/kernel7base_model/conv2d_8/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_8/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_8/kernel*
dtype0*(
_output_shapes
:
r
!base_model/conv2d_8/dilation_rateConst*
dtype0*
valueB"      *
_output_shapes
:

)base_model/conv2d_8/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_8/kernel*(
_output_shapes
:*
dtype0
×
base_model/conv2d_8/Conv2DConv2Dbase_model/Relu_5)base_model/conv2d_8/Conv2D/ReadVariableOp*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
strides
*
paddingSAME
Â
8base_model/batch_normalization_8/gamma/Initializer/zerosConst*9
_class/
-+loc:@base_model/batch_normalization_8/gamma*
_output_shapes	
:*
valueB*    *
dtype0
ŕ
&base_model/batch_normalization_8/gammaVarHandleOp*7
shared_name(&base_model/batch_normalization_8/gamma*9
_class/
-+loc:@base_model/batch_normalization_8/gamma*
dtype0*
shape:*
_output_shapes
: 

Gbase_model/batch_normalization_8/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_8/gamma*
_output_shapes
: 
°
-base_model/batch_normalization_8/gamma/AssignAssignVariableOp&base_model/batch_normalization_8/gamma8base_model/batch_normalization_8/gamma/Initializer/zeros*
dtype0

:base_model/batch_normalization_8/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_8/gamma*
_output_shapes	
:*
dtype0
Ŕ
7base_model/batch_normalization_8/beta/Initializer/zerosConst*
valueB*    *
dtype0*8
_class.
,*loc:@base_model/batch_normalization_8/beta*
_output_shapes	
:
Ý
%base_model/batch_normalization_8/betaVarHandleOp*8
_class.
,*loc:@base_model/batch_normalization_8/beta*
shape:*6
shared_name'%base_model/batch_normalization_8/beta*
dtype0*
_output_shapes
: 

Fbase_model/batch_normalization_8/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_8/beta*
_output_shapes
: 
­
,base_model/batch_normalization_8/beta/AssignAssignVariableOp%base_model/batch_normalization_8/beta7base_model/batch_normalization_8/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_8/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_8/beta*
_output_shapes	
:*
dtype0
Î
>base_model/batch_normalization_8/moving_mean/Initializer/zerosConst*
_output_shapes	
:*
valueB*    *
dtype0*?
_class5
31loc:@base_model/batch_normalization_8/moving_mean
ň
,base_model/batch_normalization_8/moving_meanVarHandleOp*?
_class5
31loc:@base_model/batch_normalization_8/moving_mean*=
shared_name.,base_model/batch_normalization_8/moving_mean*
shape:*
_output_shapes
: *
dtype0
Š
Mbase_model/batch_normalization_8/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_8/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_8/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_8/moving_mean>base_model/batch_normalization_8/moving_mean/Initializer/zeros*
dtype0
Ş
@base_model/batch_normalization_8/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_8/moving_mean*
dtype0*
_output_shapes	
:
Ő
Abase_model/batch_normalization_8/moving_variance/Initializer/onesConst*C
_class9
75loc:@base_model/batch_normalization_8/moving_variance*
_output_shapes	
:*
dtype0*
valueB*  ?
ţ
0base_model/batch_normalization_8/moving_varianceVarHandleOp*
dtype0*A
shared_name20base_model/batch_normalization_8/moving_variance*
shape:*
_output_shapes
: *C
_class9
75loc:@base_model/batch_normalization_8/moving_variance
ą
Qbase_model/batch_normalization_8/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_8/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_8/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_8/moving_varianceAbase_model/batch_normalization_8/moving_variance/Initializer/ones*
dtype0
˛
Dbase_model/batch_normalization_8/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_8/moving_variance*
_output_shapes	
:*
dtype0

/base_model/batch_normalization_8/ReadVariableOpReadVariableOp&base_model/batch_normalization_8/gamma*
_output_shapes	
:*
dtype0

1base_model/batch_normalization_8/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_8/beta*
_output_shapes	
:*
dtype0
i
&base_model/batch_normalization_8/ConstConst*
dtype0*
_output_shapes
: *
valueB 
k
(base_model/batch_normalization_8/Const_1Const*
valueB *
_output_shapes
: *
dtype0

1base_model/batch_normalization_8/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_8/Conv2D/base_model/batch_normalization_8/ReadVariableOp1base_model/batch_normalization_8/ReadVariableOp_1&base_model/batch_normalization_8/Const(base_model/batch_normalization_8/Const_1*
U0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
epsilon%đ'7*
T0
m
(base_model/batch_normalization_8/Const_2Const*
valueB
 *fff?*
dtype0*
_output_shapes
: 
ź
6base_model/batch_normalization_8/AssignMovingAvg/sub/xConst*?
_class5
31loc:@base_model/batch_normalization_8/moving_mean*
_output_shapes
: *
valueB
 *  ?*
dtype0
˙
4base_model/batch_normalization_8/AssignMovingAvg/subSub6base_model/batch_normalization_8/AssignMovingAvg/sub/x(base_model/batch_normalization_8/Const_2*
T0*
_output_shapes
: *?
_class5
31loc:@base_model/batch_normalization_8/moving_mean
Š
?base_model/batch_normalization_8/AssignMovingAvg/ReadVariableOpReadVariableOp,base_model/batch_normalization_8/moving_mean*
_output_shapes	
:*
dtype0

6base_model/batch_normalization_8/AssignMovingAvg/sub_1Sub?base_model/batch_normalization_8/AssignMovingAvg/ReadVariableOp3base_model/batch_normalization_8/FusedBatchNormV3:1*
_output_shapes	
:*?
_class5
31loc:@base_model/batch_normalization_8/moving_mean*
T0

4base_model/batch_normalization_8/AssignMovingAvg/mulMul6base_model/batch_normalization_8/AssignMovingAvg/sub_14base_model/batch_normalization_8/AssignMovingAvg/sub*
T0*
_output_shapes	
:*?
_class5
31loc:@base_model/batch_normalization_8/moving_mean

Dbase_model/batch_normalization_8/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp,base_model/batch_normalization_8/moving_mean4base_model/batch_normalization_8/AssignMovingAvg/mul*?
_class5
31loc:@base_model/batch_normalization_8/moving_mean*
dtype0
ł
Abase_model/batch_normalization_8/AssignMovingAvg/ReadVariableOp_1ReadVariableOp,base_model/batch_normalization_8/moving_meanE^base_model/batch_normalization_8/AssignMovingAvg/AssignSubVariableOp*?
_class5
31loc:@base_model/batch_normalization_8/moving_mean*
dtype0*
_output_shapes	
:
Â
8base_model/batch_normalization_8/AssignMovingAvg_1/sub/xConst*
valueB
 *  ?*C
_class9
75loc:@base_model/batch_normalization_8/moving_variance*
dtype0*
_output_shapes
: 

6base_model/batch_normalization_8/AssignMovingAvg_1/subSub8base_model/batch_normalization_8/AssignMovingAvg_1/sub/x(base_model/batch_normalization_8/Const_2*
_output_shapes
: *
T0*C
_class9
75loc:@base_model/batch_normalization_8/moving_variance
Ż
Abase_model/batch_normalization_8/AssignMovingAvg_1/ReadVariableOpReadVariableOp0base_model/batch_normalization_8/moving_variance*
_output_shapes	
:*
dtype0
˘
8base_model/batch_normalization_8/AssignMovingAvg_1/sub_1SubAbase_model/batch_normalization_8/AssignMovingAvg_1/ReadVariableOp3base_model/batch_normalization_8/FusedBatchNormV3:2*
_output_shapes	
:*C
_class9
75loc:@base_model/batch_normalization_8/moving_variance*
T0

6base_model/batch_normalization_8/AssignMovingAvg_1/mulMul8base_model/batch_normalization_8/AssignMovingAvg_1/sub_16base_model/batch_normalization_8/AssignMovingAvg_1/sub*
T0*
_output_shapes	
:*C
_class9
75loc:@base_model/batch_normalization_8/moving_variance

Fbase_model/batch_normalization_8/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0base_model/batch_normalization_8/moving_variance6base_model/batch_normalization_8/AssignMovingAvg_1/mul*
dtype0*C
_class9
75loc:@base_model/batch_normalization_8/moving_variance
ż
Cbase_model/batch_normalization_8/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_8/moving_varianceG^base_model/batch_normalization_8/AssignMovingAvg_1/AssignSubVariableOp*C
_class9
75loc:@base_model/batch_normalization_8/moving_variance*
dtype0*
_output_shapes	
:
Ě
base_model/add_2AddV21base_model/batch_normalization_8/FusedBatchNormV31base_model/batch_normalization_6/FusedBatchNormV3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
x
base_model/Relu_6Relubase_model/add_2*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ĺ
=base_model/conv2d_9/kernel/Initializer/truncated_normal/shapeConst*-
_class#
!loc:@base_model/conv2d_9/kernel*
dtype0*
_output_shapes
:*%
valueB"            
°
<base_model/conv2d_9/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
_output_shapes
: *
dtype0*-
_class#
!loc:@base_model/conv2d_9/kernel
˛
>base_model/conv2d_9/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
dtype0*-
_class#
!loc:@base_model/conv2d_9/kernel*
valueB
 *¸1	=

Gbase_model/conv2d_9/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal=base_model/conv2d_9/kernel/Initializer/truncated_normal/shape*
T0*
dtype0*(
_output_shapes
:*-
_class#
!loc:@base_model/conv2d_9/kernel
­
;base_model/conv2d_9/kernel/Initializer/truncated_normal/mulMulGbase_model/conv2d_9/kernel/Initializer/truncated_normal/TruncatedNormal>base_model/conv2d_9/kernel/Initializer/truncated_normal/stddev*-
_class#
!loc:@base_model/conv2d_9/kernel*(
_output_shapes
:*
T0

7base_model/conv2d_9/kernel/Initializer/truncated_normalAdd;base_model/conv2d_9/kernel/Initializer/truncated_normal/mul<base_model/conv2d_9/kernel/Initializer/truncated_normal/mean*
T0*-
_class#
!loc:@base_model/conv2d_9/kernel*(
_output_shapes
:
É
base_model/conv2d_9/kernelVarHandleOp*-
_class#
!loc:@base_model/conv2d_9/kernel*+
shared_namebase_model/conv2d_9/kernel*
_output_shapes
: *
dtype0*
shape:

;base_model/conv2d_9/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_9/kernel*
_output_shapes
: 

!base_model/conv2d_9/kernel/AssignAssignVariableOpbase_model/conv2d_9/kernel7base_model/conv2d_9/kernel/Initializer/truncated_normal*
dtype0

.base_model/conv2d_9/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_9/kernel*(
_output_shapes
:*
dtype0
r
!base_model/conv2d_9/dilation_rateConst*
valueB"      *
dtype0*
_output_shapes
:

)base_model/conv2d_9/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_9/kernel*
dtype0*(
_output_shapes
:
×
base_model/conv2d_9/Conv2DConv2Dbase_model/Relu_6)base_model/conv2d_9/Conv2D/ReadVariableOp*
paddingSAME*
T0*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Á
7base_model/batch_normalization_9/gamma/Initializer/onesConst*9
_class/
-+loc:@base_model/batch_normalization_9/gamma*
_output_shapes	
:*
valueB*  ?*
dtype0
ŕ
&base_model/batch_normalization_9/gammaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_9/gamma*
_output_shapes
: *
shape:*
dtype0*7
shared_name(&base_model/batch_normalization_9/gamma

Gbase_model/batch_normalization_9/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_9/gamma*
_output_shapes
: 
Ż
-base_model/batch_normalization_9/gamma/AssignAssignVariableOp&base_model/batch_normalization_9/gamma7base_model/batch_normalization_9/gamma/Initializer/ones*
dtype0

:base_model/batch_normalization_9/gamma/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_9/gamma*
dtype0*
_output_shapes	
:
Ŕ
7base_model/batch_normalization_9/beta/Initializer/zerosConst*
valueB*    *
dtype0*8
_class.
,*loc:@base_model/batch_normalization_9/beta*
_output_shapes	
:
Ý
%base_model/batch_normalization_9/betaVarHandleOp*
shape:*
_output_shapes
: *6
shared_name'%base_model/batch_normalization_9/beta*
dtype0*8
_class.
,*loc:@base_model/batch_normalization_9/beta

Fbase_model/batch_normalization_9/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp%base_model/batch_normalization_9/beta*
_output_shapes
: 
­
,base_model/batch_normalization_9/beta/AssignAssignVariableOp%base_model/batch_normalization_9/beta7base_model/batch_normalization_9/beta/Initializer/zeros*
dtype0

9base_model/batch_normalization_9/beta/Read/ReadVariableOpReadVariableOp%base_model/batch_normalization_9/beta*
_output_shapes	
:*
dtype0
Î
>base_model/batch_normalization_9/moving_mean/Initializer/zerosConst*?
_class5
31loc:@base_model/batch_normalization_9/moving_mean*
dtype0*
valueB*    *
_output_shapes	
:
ň
,base_model/batch_normalization_9/moving_meanVarHandleOp*
dtype0*?
_class5
31loc:@base_model/batch_normalization_9/moving_mean*
shape:*
_output_shapes
: *=
shared_name.,base_model/batch_normalization_9/moving_mean
Š
Mbase_model/batch_normalization_9/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp,base_model/batch_normalization_9/moving_mean*
_output_shapes
: 
Â
3base_model/batch_normalization_9/moving_mean/AssignAssignVariableOp,base_model/batch_normalization_9/moving_mean>base_model/batch_normalization_9/moving_mean/Initializer/zeros*
dtype0
Ş
@base_model/batch_normalization_9/moving_mean/Read/ReadVariableOpReadVariableOp,base_model/batch_normalization_9/moving_mean*
dtype0*
_output_shapes	
:
Ő
Abase_model/batch_normalization_9/moving_variance/Initializer/onesConst*C
_class9
75loc:@base_model/batch_normalization_9/moving_variance*
valueB*  ?*
_output_shapes	
:*
dtype0
ţ
0base_model/batch_normalization_9/moving_varianceVarHandleOp*
dtype0*
shape:*A
shared_name20base_model/batch_normalization_9/moving_variance*C
_class9
75loc:@base_model/batch_normalization_9/moving_variance*
_output_shapes
: 
ą
Qbase_model/batch_normalization_9/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp0base_model/batch_normalization_9/moving_variance*
_output_shapes
: 
Í
7base_model/batch_normalization_9/moving_variance/AssignAssignVariableOp0base_model/batch_normalization_9/moving_varianceAbase_model/batch_normalization_9/moving_variance/Initializer/ones*
dtype0
˛
Dbase_model/batch_normalization_9/moving_variance/Read/ReadVariableOpReadVariableOp0base_model/batch_normalization_9/moving_variance*
_output_shapes	
:*
dtype0

/base_model/batch_normalization_9/ReadVariableOpReadVariableOp&base_model/batch_normalization_9/gamma*
dtype0*
_output_shapes	
:

1base_model/batch_normalization_9/ReadVariableOp_1ReadVariableOp%base_model/batch_normalization_9/beta*
dtype0*
_output_shapes	
:
i
&base_model/batch_normalization_9/ConstConst*
dtype0*
_output_shapes
: *
valueB 
k
(base_model/batch_normalization_9/Const_1Const*
_output_shapes
: *
dtype0*
valueB 

1base_model/batch_normalization_9/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_9/Conv2D/base_model/batch_normalization_9/ReadVariableOp1base_model/batch_normalization_9/ReadVariableOp_1&base_model/batch_normalization_9/Const(base_model/batch_normalization_9/Const_1*
T0*
U0*
epsilon%đ'7*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
m
(base_model/batch_normalization_9/Const_2Const*
valueB
 *fff?*
dtype0*
_output_shapes
: 
ź
6base_model/batch_normalization_9/AssignMovingAvg/sub/xConst*
dtype0*
_output_shapes
: *?
_class5
31loc:@base_model/batch_normalization_9/moving_mean*
valueB
 *  ?
˙
4base_model/batch_normalization_9/AssignMovingAvg/subSub6base_model/batch_normalization_9/AssignMovingAvg/sub/x(base_model/batch_normalization_9/Const_2*?
_class5
31loc:@base_model/batch_normalization_9/moving_mean*
T0*
_output_shapes
: 
Š
?base_model/batch_normalization_9/AssignMovingAvg/ReadVariableOpReadVariableOp,base_model/batch_normalization_9/moving_mean*
dtype0*
_output_shapes	
:

6base_model/batch_normalization_9/AssignMovingAvg/sub_1Sub?base_model/batch_normalization_9/AssignMovingAvg/ReadVariableOp3base_model/batch_normalization_9/FusedBatchNormV3:1*
_output_shapes	
:*?
_class5
31loc:@base_model/batch_normalization_9/moving_mean*
T0

4base_model/batch_normalization_9/AssignMovingAvg/mulMul6base_model/batch_normalization_9/AssignMovingAvg/sub_14base_model/batch_normalization_9/AssignMovingAvg/sub*
_output_shapes	
:*?
_class5
31loc:@base_model/batch_normalization_9/moving_mean*
T0

Dbase_model/batch_normalization_9/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp,base_model/batch_normalization_9/moving_mean4base_model/batch_normalization_9/AssignMovingAvg/mul*?
_class5
31loc:@base_model/batch_normalization_9/moving_mean*
dtype0
ł
Abase_model/batch_normalization_9/AssignMovingAvg/ReadVariableOp_1ReadVariableOp,base_model/batch_normalization_9/moving_meanE^base_model/batch_normalization_9/AssignMovingAvg/AssignSubVariableOp*
dtype0*
_output_shapes	
:*?
_class5
31loc:@base_model/batch_normalization_9/moving_mean
Â
8base_model/batch_normalization_9/AssignMovingAvg_1/sub/xConst*
valueB
 *  ?*C
_class9
75loc:@base_model/batch_normalization_9/moving_variance*
_output_shapes
: *
dtype0

6base_model/batch_normalization_9/AssignMovingAvg_1/subSub8base_model/batch_normalization_9/AssignMovingAvg_1/sub/x(base_model/batch_normalization_9/Const_2*
T0*
_output_shapes
: *C
_class9
75loc:@base_model/batch_normalization_9/moving_variance
Ż
Abase_model/batch_normalization_9/AssignMovingAvg_1/ReadVariableOpReadVariableOp0base_model/batch_normalization_9/moving_variance*
dtype0*
_output_shapes	
:
˘
8base_model/batch_normalization_9/AssignMovingAvg_1/sub_1SubAbase_model/batch_normalization_9/AssignMovingAvg_1/ReadVariableOp3base_model/batch_normalization_9/FusedBatchNormV3:2*C
_class9
75loc:@base_model/batch_normalization_9/moving_variance*
_output_shapes	
:*
T0

6base_model/batch_normalization_9/AssignMovingAvg_1/mulMul8base_model/batch_normalization_9/AssignMovingAvg_1/sub_16base_model/batch_normalization_9/AssignMovingAvg_1/sub*
_output_shapes	
:*
T0*C
_class9
75loc:@base_model/batch_normalization_9/moving_variance

Fbase_model/batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp0base_model/batch_normalization_9/moving_variance6base_model/batch_normalization_9/AssignMovingAvg_1/mul*C
_class9
75loc:@base_model/batch_normalization_9/moving_variance*
dtype0
ż
Cbase_model/batch_normalization_9/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp0base_model/batch_normalization_9/moving_varianceG^base_model/batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*
_output_shapes	
:*C
_class9
75loc:@base_model/batch_normalization_9/moving_variance

base_model/Relu_7Relu1base_model/batch_normalization_9/FusedBatchNormV3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_10/kernel/Initializer/truncated_normal/shapeConst*
dtype0*%
valueB"            *
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_10/kernel
˛
=base_model/conv2d_10/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *
dtype0*.
_class$
" loc:@base_model/conv2d_10/kernel*
valueB
 *    
´
?base_model/conv2d_10/kernel/Initializer/truncated_normal/stddevConst*
dtype0*
valueB
 *¸1	=*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_10/kernel

Hbase_model/conv2d_10/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_10/kernel/Initializer/truncated_normal/shape*.
_class$
" loc:@base_model/conv2d_10/kernel*
T0*(
_output_shapes
:*
dtype0
ą
<base_model/conv2d_10/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_10/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_10/kernel/Initializer/truncated_normal/stddev*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_10/kernel

8base_model/conv2d_10/kernel/Initializer/truncated_normalAdd<base_model/conv2d_10/kernel/Initializer/truncated_normal/mul=base_model/conv2d_10/kernel/Initializer/truncated_normal/mean*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_10/kernel
Ě
base_model/conv2d_10/kernelVarHandleOp*
dtype0*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_10/kernel*
shape:*,
shared_namebase_model/conv2d_10/kernel

<base_model/conv2d_10/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_10/kernel*
_output_shapes
: 

"base_model/conv2d_10/kernel/AssignAssignVariableOpbase_model/conv2d_10/kernel8base_model/conv2d_10/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_10/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_10/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_10/dilation_rateConst*
valueB"      *
dtype0*
_output_shapes
:

*base_model/conv2d_10/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_10/kernel*
dtype0*(
_output_shapes
:
Ů
base_model/conv2d_10/Conv2DConv2Dbase_model/Relu_7*base_model/conv2d_10/Conv2D/ReadVariableOp*
paddingSAME*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
strides
*
T0
Ä
9base_model/batch_normalization_10/gamma/Initializer/zerosConst*:
_class0
.,loc:@base_model/batch_normalization_10/gamma*
dtype0*
valueB*    *
_output_shapes	
:
ă
'base_model/batch_normalization_10/gammaVarHandleOp*
_output_shapes
: *:
_class0
.,loc:@base_model/batch_normalization_10/gamma*
shape:*8
shared_name)'base_model/batch_normalization_10/gamma*
dtype0

Hbase_model/batch_normalization_10/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_10/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_10/gamma/AssignAssignVariableOp'base_model/batch_normalization_10/gamma9base_model/batch_normalization_10/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_10/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_10/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_10/beta/Initializer/zerosConst*
_output_shapes	
:*9
_class/
-+loc:@base_model/batch_normalization_10/beta*
valueB*    *
dtype0
ŕ
&base_model/batch_normalization_10/betaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_10/beta*
shape:*
_output_shapes
: *
dtype0*7
shared_name(&base_model/batch_normalization_10/beta

Gbase_model/batch_normalization_10/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_10/beta*
_output_shapes
: 
°
-base_model/batch_normalization_10/beta/AssignAssignVariableOp&base_model/batch_normalization_10/beta8base_model/batch_normalization_10/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_10/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_10/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_10/moving_mean/Initializer/zerosConst*@
_class6
42loc:@base_model/batch_normalization_10/moving_mean*
_output_shapes	
:*
dtype0*
valueB*    
ő
-base_model/batch_normalization_10/moving_meanVarHandleOp*@
_class6
42loc:@base_model/batch_normalization_10/moving_mean*>
shared_name/-base_model/batch_normalization_10/moving_mean*
_output_shapes
: *
shape:*
dtype0
Ť
Nbase_model/batch_normalization_10/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_10/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_10/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_10/moving_mean?base_model/batch_normalization_10/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_10/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_10/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_10/moving_variance/Initializer/onesConst*D
_class:
86loc:@base_model/batch_normalization_10/moving_variance*
dtype0*
valueB*  ?*
_output_shapes	
:

1base_model/batch_normalization_10/moving_varianceVarHandleOp*
_output_shapes
: *
dtype0*B
shared_name31base_model/batch_normalization_10/moving_variance*D
_class:
86loc:@base_model/batch_normalization_10/moving_variance*
shape:
ł
Rbase_model/batch_normalization_10/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_10/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_10/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_10/moving_varianceBbase_model/batch_normalization_10/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_10/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_10/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_10/ReadVariableOpReadVariableOp'base_model/batch_normalization_10/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_10/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_10/beta*
_output_shapes	
:*
dtype0
j
'base_model/batch_normalization_10/ConstConst*
valueB *
dtype0*
_output_shapes
: 
l
)base_model/batch_normalization_10/Const_1Const*
dtype0*
_output_shapes
: *
valueB 
Ľ
2base_model/batch_normalization_10/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_10/Conv2D0base_model/batch_normalization_10/ReadVariableOp2base_model/batch_normalization_10/ReadVariableOp_1'base_model/batch_normalization_10/Const)base_model/batch_normalization_10/Const_1*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
epsilon%đ'7*
U0*
T0
n
)base_model/batch_normalization_10/Const_2Const*
dtype0*
valueB
 *fff?*
_output_shapes
: 
ž
7base_model/batch_normalization_10/AssignMovingAvg/sub/xConst*@
_class6
42loc:@base_model/batch_normalization_10/moving_mean*
valueB
 *  ?*
dtype0*
_output_shapes
: 

5base_model/batch_normalization_10/AssignMovingAvg/subSub7base_model/batch_normalization_10/AssignMovingAvg/sub/x)base_model/batch_normalization_10/Const_2*
_output_shapes
: *
T0*@
_class6
42loc:@base_model/batch_normalization_10/moving_mean
Ť
@base_model/batch_normalization_10/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_10/moving_mean*
_output_shapes	
:*
dtype0

7base_model/batch_normalization_10/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_10/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_10/FusedBatchNormV3:1*
_output_shapes	
:*
T0*@
_class6
42loc:@base_model/batch_normalization_10/moving_mean

5base_model/batch_normalization_10/AssignMovingAvg/mulMul7base_model/batch_normalization_10/AssignMovingAvg/sub_15base_model/batch_normalization_10/AssignMovingAvg/sub*
T0*@
_class6
42loc:@base_model/batch_normalization_10/moving_mean*
_output_shapes	
:

Ebase_model/batch_normalization_10/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_10/moving_mean5base_model/batch_normalization_10/AssignMovingAvg/mul*@
_class6
42loc:@base_model/batch_normalization_10/moving_mean*
dtype0
ˇ
Bbase_model/batch_normalization_10/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_10/moving_meanF^base_model/batch_normalization_10/AssignMovingAvg/AssignSubVariableOp*
dtype0*@
_class6
42loc:@base_model/batch_normalization_10/moving_mean*
_output_shapes	
:
Ä
9base_model/batch_normalization_10/AssignMovingAvg_1/sub/xConst*
_output_shapes
: *
valueB
 *  ?*
dtype0*D
_class:
86loc:@base_model/batch_normalization_10/moving_variance

7base_model/batch_normalization_10/AssignMovingAvg_1/subSub9base_model/batch_normalization_10/AssignMovingAvg_1/sub/x)base_model/batch_normalization_10/Const_2*D
_class:
86loc:@base_model/batch_normalization_10/moving_variance*
T0*
_output_shapes
: 
ą
Bbase_model/batch_normalization_10/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_10/moving_variance*
dtype0*
_output_shapes	
:
Ś
9base_model/batch_normalization_10/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_10/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_10/FusedBatchNormV3:2*
T0*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_10/moving_variance

7base_model/batch_normalization_10/AssignMovingAvg_1/mulMul9base_model/batch_normalization_10/AssignMovingAvg_1/sub_17base_model/batch_normalization_10/AssignMovingAvg_1/sub*
T0*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_10/moving_variance

Gbase_model/batch_normalization_10/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_10/moving_variance7base_model/batch_normalization_10/AssignMovingAvg_1/mul*
dtype0*D
_class:
86loc:@base_model/batch_normalization_10/moving_variance
Ă
Dbase_model/batch_normalization_10/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_10/moving_varianceH^base_model/batch_normalization_10/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*D
_class:
86loc:@base_model/batch_normalization_10/moving_variance*
_output_shapes	
:
­
base_model/add_3AddV22base_model/batch_normalization_10/FusedBatchNormV3base_model/Relu_6*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
x
base_model/Relu_8Relubase_model/add_3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

base_model/block_group2Identitybase_model/Relu_8*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

base_model/Pad_3/paddingsConst*
_output_shapes

:*9
value0B."                                 *
dtype0

base_model/Pad_3Padbase_model/block_group2base_model/Pad_3/paddings*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ç
>base_model/conv2d_11/kernel/Initializer/truncated_normal/shapeConst*%
valueB"            *.
_class$
" loc:@base_model/conv2d_11/kernel*
dtype0*
_output_shapes
:
˛
=base_model/conv2d_11/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *.
_class$
" loc:@base_model/conv2d_11/kernel*
dtype0*
_output_shapes
: 
´
?base_model/conv2d_11/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_11/kernel*
valueB
 *ĘÍ=*
dtype0

Hbase_model/conv2d_11/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_11/kernel/Initializer/truncated_normal/shape*(
_output_shapes
:*
dtype0*
T0*.
_class$
" loc:@base_model/conv2d_11/kernel
ą
<base_model/conv2d_11/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_11/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_11/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_11/kernel*
T0*(
_output_shapes
:

8base_model/conv2d_11/kernel/Initializer/truncated_normalAdd<base_model/conv2d_11/kernel/Initializer/truncated_normal/mul=base_model/conv2d_11/kernel/Initializer/truncated_normal/mean*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_11/kernel
Ě
base_model/conv2d_11/kernelVarHandleOp*
shape:*.
_class$
" loc:@base_model/conv2d_11/kernel*
dtype0*
_output_shapes
: *,
shared_namebase_model/conv2d_11/kernel

<base_model/conv2d_11/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_11/kernel*
_output_shapes
: 

"base_model/conv2d_11/kernel/AssignAssignVariableOpbase_model/conv2d_11/kernel8base_model/conv2d_11/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_11/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_11/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_11/dilation_rateConst*
dtype0*
valueB"      *
_output_shapes
:

*base_model/conv2d_11/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_11/kernel*(
_output_shapes
:*
dtype0
Ů
base_model/conv2d_11/Conv2DConv2Dbase_model/Pad_3*base_model/conv2d_11/Conv2D/ReadVariableOp*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0*
strides
*
paddingVALID
Ă
8base_model/batch_normalization_11/gamma/Initializer/onesConst*
valueB*  ?*
dtype0*:
_class0
.,loc:@base_model/batch_normalization_11/gamma*
_output_shapes	
:
ă
'base_model/batch_normalization_11/gammaVarHandleOp*
_output_shapes
: *8
shared_name)'base_model/batch_normalization_11/gamma*
shape:*:
_class0
.,loc:@base_model/batch_normalization_11/gamma*
dtype0

Hbase_model/batch_normalization_11/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_11/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_11/gamma/AssignAssignVariableOp'base_model/batch_normalization_11/gamma8base_model/batch_normalization_11/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_11/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_11/gamma*
dtype0*
_output_shapes	
:
Â
8base_model/batch_normalization_11/beta/Initializer/zerosConst*9
_class/
-+loc:@base_model/batch_normalization_11/beta*
_output_shapes	
:*
valueB*    *
dtype0
ŕ
&base_model/batch_normalization_11/betaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_11/beta*
_output_shapes
: *
shape:*7
shared_name(&base_model/batch_normalization_11/beta*
dtype0

Gbase_model/batch_normalization_11/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_11/beta*
_output_shapes
: 
°
-base_model/batch_normalization_11/beta/AssignAssignVariableOp&base_model/batch_normalization_11/beta8base_model/batch_normalization_11/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_11/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_11/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_11/moving_mean/Initializer/zerosConst*@
_class6
42loc:@base_model/batch_normalization_11/moving_mean*
valueB*    *
_output_shapes	
:*
dtype0
ő
-base_model/batch_normalization_11/moving_meanVarHandleOp*>
shared_name/-base_model/batch_normalization_11/moving_mean*
dtype0*
_output_shapes
: *@
_class6
42loc:@base_model/batch_normalization_11/moving_mean*
shape:
Ť
Nbase_model/batch_normalization_11/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_11/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_11/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_11/moving_mean?base_model/batch_normalization_11/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_11/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_11/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_11/moving_variance/Initializer/onesConst*
_output_shapes	
:*
valueB*  ?*
dtype0*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance

1base_model/batch_normalization_11/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_11/moving_variance*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance*
shape:*
dtype0*
_output_shapes
: 
ł
Rbase_model/batch_normalization_11/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_11/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_11/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_11/moving_varianceBbase_model/batch_normalization_11/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_11/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_11/moving_variance*
_output_shapes	
:*
dtype0

0base_model/batch_normalization_11/ReadVariableOpReadVariableOp'base_model/batch_normalization_11/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_11/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_11/beta*
_output_shapes	
:*
dtype0
j
'base_model/batch_normalization_11/ConstConst*
dtype0*
_output_shapes
: *
valueB 
l
)base_model/batch_normalization_11/Const_1Const*
valueB *
dtype0*
_output_shapes
: 
Ľ
2base_model/batch_normalization_11/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_11/Conv2D0base_model/batch_normalization_11/ReadVariableOp2base_model/batch_normalization_11/ReadVariableOp_1'base_model/batch_normalization_11/Const)base_model/batch_normalization_11/Const_1*
epsilon%đ'7*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
T0*
U0
n
)base_model/batch_normalization_11/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *fff?
ž
7base_model/batch_normalization_11/AssignMovingAvg/sub/xConst*@
_class6
42loc:@base_model/batch_normalization_11/moving_mean*
dtype0*
valueB
 *  ?*
_output_shapes
: 

5base_model/batch_normalization_11/AssignMovingAvg/subSub7base_model/batch_normalization_11/AssignMovingAvg/sub/x)base_model/batch_normalization_11/Const_2*
_output_shapes
: *@
_class6
42loc:@base_model/batch_normalization_11/moving_mean*
T0
Ť
@base_model/batch_normalization_11/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_11/moving_mean*
dtype0*
_output_shapes	
:

7base_model/batch_normalization_11/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_11/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_11/FusedBatchNormV3:1*
_output_shapes	
:*
T0*@
_class6
42loc:@base_model/batch_normalization_11/moving_mean

5base_model/batch_normalization_11/AssignMovingAvg/mulMul7base_model/batch_normalization_11/AssignMovingAvg/sub_15base_model/batch_normalization_11/AssignMovingAvg/sub*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_11/moving_mean*
T0

Ebase_model/batch_normalization_11/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_11/moving_mean5base_model/batch_normalization_11/AssignMovingAvg/mul*
dtype0*@
_class6
42loc:@base_model/batch_normalization_11/moving_mean
ˇ
Bbase_model/batch_normalization_11/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_11/moving_meanF^base_model/batch_normalization_11/AssignMovingAvg/AssignSubVariableOp*
dtype0*@
_class6
42loc:@base_model/batch_normalization_11/moving_mean*
_output_shapes	
:
Ä
9base_model/batch_normalization_11/AssignMovingAvg_1/sub/xConst*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance*
_output_shapes
: *
valueB
 *  ?*
dtype0

7base_model/batch_normalization_11/AssignMovingAvg_1/subSub9base_model/batch_normalization_11/AssignMovingAvg_1/sub/x)base_model/batch_normalization_11/Const_2*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance*
T0*
_output_shapes
: 
ą
Bbase_model/batch_normalization_11/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_11/moving_variance*
dtype0*
_output_shapes	
:
Ś
9base_model/batch_normalization_11/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_11/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_11/FusedBatchNormV3:2*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance*
T0*
_output_shapes	
:

7base_model/batch_normalization_11/AssignMovingAvg_1/mulMul9base_model/batch_normalization_11/AssignMovingAvg_1/sub_17base_model/batch_normalization_11/AssignMovingAvg_1/sub*
T0*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance*
_output_shapes	
:

Gbase_model/batch_normalization_11/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_11/moving_variance7base_model/batch_normalization_11/AssignMovingAvg_1/mul*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance*
dtype0
Ă
Dbase_model/batch_normalization_11/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_11/moving_varianceH^base_model/batch_normalization_11/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_11/moving_variance

base_model/Pad_4/paddingsConst*9
value0B."                             *
_output_shapes

:*
dtype0

base_model/Pad_4Padbase_model/block_group2base_model/Pad_4/paddings*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ç
>base_model/conv2d_12/kernel/Initializer/truncated_normal/shapeConst*
dtype0*
_output_shapes
:*%
valueB"            *.
_class$
" loc:@base_model/conv2d_12/kernel
˛
=base_model/conv2d_12/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
dtype0*.
_class$
" loc:@base_model/conv2d_12/kernel*
_output_shapes
: 
´
?base_model/conv2d_12/kernel/Initializer/truncated_normal/stddevConst*
dtype0*
_output_shapes
: *
valueB
 *¸1	=*.
_class$
" loc:@base_model/conv2d_12/kernel

Hbase_model/conv2d_12/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_12/kernel/Initializer/truncated_normal/shape*
T0*(
_output_shapes
:*
dtype0*.
_class$
" loc:@base_model/conv2d_12/kernel
ą
<base_model/conv2d_12/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_12/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_12/kernel/Initializer/truncated_normal/stddev*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_12/kernel

8base_model/conv2d_12/kernel/Initializer/truncated_normalAdd<base_model/conv2d_12/kernel/Initializer/truncated_normal/mul=base_model/conv2d_12/kernel/Initializer/truncated_normal/mean*.
_class$
" loc:@base_model/conv2d_12/kernel*(
_output_shapes
:*
T0
Ě
base_model/conv2d_12/kernelVarHandleOp*
shape:*.
_class$
" loc:@base_model/conv2d_12/kernel*,
shared_namebase_model/conv2d_12/kernel*
dtype0*
_output_shapes
: 

<base_model/conv2d_12/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_12/kernel*
_output_shapes
: 

"base_model/conv2d_12/kernel/AssignAssignVariableOpbase_model/conv2d_12/kernel8base_model/conv2d_12/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_12/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_12/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_12/dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      

*base_model/conv2d_12/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_12/kernel*(
_output_shapes
:*
dtype0
Ů
base_model/conv2d_12/Conv2DConv2Dbase_model/Pad_4*base_model/conv2d_12/Conv2D/ReadVariableOp*
strides
*
T0*
paddingVALID*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ă
8base_model/batch_normalization_12/gamma/Initializer/onesConst*
dtype0*:
_class0
.,loc:@base_model/batch_normalization_12/gamma*
_output_shapes	
:*
valueB*  ?
ă
'base_model/batch_normalization_12/gammaVarHandleOp*8
shared_name)'base_model/batch_normalization_12/gamma*:
_class0
.,loc:@base_model/batch_normalization_12/gamma*
shape:*
_output_shapes
: *
dtype0

Hbase_model/batch_normalization_12/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_12/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_12/gamma/AssignAssignVariableOp'base_model/batch_normalization_12/gamma8base_model/batch_normalization_12/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_12/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_12/gamma*
dtype0*
_output_shapes	
:
Â
8base_model/batch_normalization_12/beta/Initializer/zerosConst*
valueB*    *9
_class/
-+loc:@base_model/batch_normalization_12/beta*
_output_shapes	
:*
dtype0
ŕ
&base_model/batch_normalization_12/betaVarHandleOp*
_output_shapes
: *
dtype0*7
shared_name(&base_model/batch_normalization_12/beta*9
_class/
-+loc:@base_model/batch_normalization_12/beta*
shape:

Gbase_model/batch_normalization_12/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_12/beta*
_output_shapes
: 
°
-base_model/batch_normalization_12/beta/AssignAssignVariableOp&base_model/batch_normalization_12/beta8base_model/batch_normalization_12/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_12/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_12/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_12/moving_mean/Initializer/zerosConst*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean*
valueB*    *
_output_shapes	
:*
dtype0
ő
-base_model/batch_normalization_12/moving_meanVarHandleOp*
dtype0*>
shared_name/-base_model/batch_normalization_12/moving_mean*
shape:*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean*
_output_shapes
: 
Ť
Nbase_model/batch_normalization_12/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_12/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_12/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_12/moving_mean?base_model/batch_normalization_12/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_12/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_12/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_12/moving_variance/Initializer/onesConst*
dtype0*
valueB*  ?*D
_class:
86loc:@base_model/batch_normalization_12/moving_variance*
_output_shapes	
:

1base_model/batch_normalization_12/moving_varianceVarHandleOp*
shape:*B
shared_name31base_model/batch_normalization_12/moving_variance*
_output_shapes
: *
dtype0*D
_class:
86loc:@base_model/batch_normalization_12/moving_variance
ł
Rbase_model/batch_normalization_12/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_12/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_12/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_12/moving_varianceBbase_model/batch_normalization_12/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_12/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_12/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_12/ReadVariableOpReadVariableOp'base_model/batch_normalization_12/gamma*
_output_shapes	
:*
dtype0

2base_model/batch_normalization_12/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_12/beta*
_output_shapes	
:*
dtype0
j
'base_model/batch_normalization_12/ConstConst*
dtype0*
_output_shapes
: *
valueB 
l
)base_model/batch_normalization_12/Const_1Const*
dtype0*
_output_shapes
: *
valueB 
Ľ
2base_model/batch_normalization_12/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_12/Conv2D0base_model/batch_normalization_12/ReadVariableOp2base_model/batch_normalization_12/ReadVariableOp_1'base_model/batch_normalization_12/Const)base_model/batch_normalization_12/Const_1*
U0*
epsilon%đ'7*
T0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
n
)base_model/batch_normalization_12/Const_2Const*
dtype0*
_output_shapes
: *
valueB
 *fff?
ž
7base_model/batch_normalization_12/AssignMovingAvg/sub/xConst*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean*
valueB
 *  ?*
dtype0*
_output_shapes
: 

5base_model/batch_normalization_12/AssignMovingAvg/subSub7base_model/batch_normalization_12/AssignMovingAvg/sub/x)base_model/batch_normalization_12/Const_2*
_output_shapes
: *
T0*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean
Ť
@base_model/batch_normalization_12/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_12/moving_mean*
dtype0*
_output_shapes	
:

7base_model/batch_normalization_12/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_12/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_12/FusedBatchNormV3:1*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean*
_output_shapes	
:*
T0

5base_model/batch_normalization_12/AssignMovingAvg/mulMul7base_model/batch_normalization_12/AssignMovingAvg/sub_15base_model/batch_normalization_12/AssignMovingAvg/sub*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean*
_output_shapes	
:*
T0

Ebase_model/batch_normalization_12/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_12/moving_mean5base_model/batch_normalization_12/AssignMovingAvg/mul*
dtype0*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean
ˇ
Bbase_model/batch_normalization_12/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_12/moving_meanF^base_model/batch_normalization_12/AssignMovingAvg/AssignSubVariableOp*@
_class6
42loc:@base_model/batch_normalization_12/moving_mean*
dtype0*
_output_shapes	
:
Ä
9base_model/batch_normalization_12/AssignMovingAvg_1/sub/xConst*D
_class:
86loc:@base_model/batch_normalization_12/moving_variance*
dtype0*
valueB
 *  ?*
_output_shapes
: 

7base_model/batch_normalization_12/AssignMovingAvg_1/subSub9base_model/batch_normalization_12/AssignMovingAvg_1/sub/x)base_model/batch_normalization_12/Const_2*
T0*
_output_shapes
: *D
_class:
86loc:@base_model/batch_normalization_12/moving_variance
ą
Bbase_model/batch_normalization_12/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_12/moving_variance*
_output_shapes	
:*
dtype0
Ś
9base_model/batch_normalization_12/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_12/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_12/FusedBatchNormV3:2*
_output_shapes	
:*
T0*D
_class:
86loc:@base_model/batch_normalization_12/moving_variance

7base_model/batch_normalization_12/AssignMovingAvg_1/mulMul9base_model/batch_normalization_12/AssignMovingAvg_1/sub_17base_model/batch_normalization_12/AssignMovingAvg_1/sub*
_output_shapes	
:*
T0*D
_class:
86loc:@base_model/batch_normalization_12/moving_variance

Gbase_model/batch_normalization_12/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_12/moving_variance7base_model/batch_normalization_12/AssignMovingAvg_1/mul*
dtype0*D
_class:
86loc:@base_model/batch_normalization_12/moving_variance
Ă
Dbase_model/batch_normalization_12/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_12/moving_varianceH^base_model/batch_normalization_12/AssignMovingAvg_1/AssignSubVariableOp*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_12/moving_variance*
dtype0

base_model/Relu_9Relu2base_model/batch_normalization_12/FusedBatchNormV3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_13/kernel/Initializer/truncated_normal/shapeConst*%
valueB"            *
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_13/kernel*
dtype0
˛
=base_model/conv2d_13/kernel/Initializer/truncated_normal/meanConst*.
_class$
" loc:@base_model/conv2d_13/kernel*
valueB
 *    *
dtype0*
_output_shapes
: 
´
?base_model/conv2d_13/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
valueB
 *Â<*
dtype0*.
_class$
" loc:@base_model/conv2d_13/kernel

Hbase_model/conv2d_13/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_13/kernel/Initializer/truncated_normal/shape*
T0*
dtype0*.
_class$
" loc:@base_model/conv2d_13/kernel*(
_output_shapes
:
ą
<base_model/conv2d_13/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_13/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_13/kernel/Initializer/truncated_normal/stddev*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_13/kernel

8base_model/conv2d_13/kernel/Initializer/truncated_normalAdd<base_model/conv2d_13/kernel/Initializer/truncated_normal/mul=base_model/conv2d_13/kernel/Initializer/truncated_normal/mean*.
_class$
" loc:@base_model/conv2d_13/kernel*(
_output_shapes
:*
T0
Ě
base_model/conv2d_13/kernelVarHandleOp*.
_class$
" loc:@base_model/conv2d_13/kernel*
dtype0*,
shared_namebase_model/conv2d_13/kernel*
_output_shapes
: *
shape:

<base_model/conv2d_13/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_13/kernel*
_output_shapes
: 

"base_model/conv2d_13/kernel/AssignAssignVariableOpbase_model/conv2d_13/kernel8base_model/conv2d_13/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_13/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_13/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_13/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB"      

*base_model/conv2d_13/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_13/kernel*
dtype0*(
_output_shapes
:
Ů
base_model/conv2d_13/Conv2DConv2Dbase_model/Relu_9*base_model/conv2d_13/Conv2D/ReadVariableOp*
paddingSAME*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ä
9base_model/batch_normalization_13/gamma/Initializer/zerosConst*
valueB*    *
dtype0*
_output_shapes	
:*:
_class0
.,loc:@base_model/batch_normalization_13/gamma
ă
'base_model/batch_normalization_13/gammaVarHandleOp*8
shared_name)'base_model/batch_normalization_13/gamma*
_output_shapes
: *:
_class0
.,loc:@base_model/batch_normalization_13/gamma*
shape:*
dtype0

Hbase_model/batch_normalization_13/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_13/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_13/gamma/AssignAssignVariableOp'base_model/batch_normalization_13/gamma9base_model/batch_normalization_13/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_13/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_13/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_13/beta/Initializer/zerosConst*9
_class/
-+loc:@base_model/batch_normalization_13/beta*
_output_shapes	
:*
dtype0*
valueB*    
ŕ
&base_model/batch_normalization_13/betaVarHandleOp*
dtype0*
_output_shapes
: *7
shared_name(&base_model/batch_normalization_13/beta*
shape:*9
_class/
-+loc:@base_model/batch_normalization_13/beta

Gbase_model/batch_normalization_13/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_13/beta*
_output_shapes
: 
°
-base_model/batch_normalization_13/beta/AssignAssignVariableOp&base_model/batch_normalization_13/beta8base_model/batch_normalization_13/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_13/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_13/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_13/moving_mean/Initializer/zerosConst*@
_class6
42loc:@base_model/batch_normalization_13/moving_mean*
dtype0*
_output_shapes	
:*
valueB*    
ő
-base_model/batch_normalization_13/moving_meanVarHandleOp*
_output_shapes
: *>
shared_name/-base_model/batch_normalization_13/moving_mean*
shape:*
dtype0*@
_class6
42loc:@base_model/batch_normalization_13/moving_mean
Ť
Nbase_model/batch_normalization_13/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_13/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_13/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_13/moving_mean?base_model/batch_normalization_13/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_13/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_13/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_13/moving_variance/Initializer/onesConst*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance*
valueB*  ?*
dtype0*
_output_shapes	
:

1base_model/batch_normalization_13/moving_varianceVarHandleOp*
shape:*
_output_shapes
: *B
shared_name31base_model/batch_normalization_13/moving_variance*
dtype0*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance
ł
Rbase_model/batch_normalization_13/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_13/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_13/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_13/moving_varianceBbase_model/batch_normalization_13/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_13/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_13/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_13/ReadVariableOpReadVariableOp'base_model/batch_normalization_13/gamma*
_output_shapes	
:*
dtype0

2base_model/batch_normalization_13/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_13/beta*
dtype0*
_output_shapes	
:
j
'base_model/batch_normalization_13/ConstConst*
_output_shapes
: *
valueB *
dtype0
l
)base_model/batch_normalization_13/Const_1Const*
dtype0*
valueB *
_output_shapes
: 
Ľ
2base_model/batch_normalization_13/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_13/Conv2D0base_model/batch_normalization_13/ReadVariableOp2base_model/batch_normalization_13/ReadVariableOp_1'base_model/batch_normalization_13/Const)base_model/batch_normalization_13/Const_1*
U0*
T0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
epsilon%đ'7
n
)base_model/batch_normalization_13/Const_2Const*
dtype0*
_output_shapes
: *
valueB
 *fff?
ž
7base_model/batch_normalization_13/AssignMovingAvg/sub/xConst*
dtype0*@
_class6
42loc:@base_model/batch_normalization_13/moving_mean*
valueB
 *  ?*
_output_shapes
: 

5base_model/batch_normalization_13/AssignMovingAvg/subSub7base_model/batch_normalization_13/AssignMovingAvg/sub/x)base_model/batch_normalization_13/Const_2*
_output_shapes
: *@
_class6
42loc:@base_model/batch_normalization_13/moving_mean*
T0
Ť
@base_model/batch_normalization_13/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_13/moving_mean*
_output_shapes	
:*
dtype0

7base_model/batch_normalization_13/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_13/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_13/FusedBatchNormV3:1*@
_class6
42loc:@base_model/batch_normalization_13/moving_mean*
T0*
_output_shapes	
:

5base_model/batch_normalization_13/AssignMovingAvg/mulMul7base_model/batch_normalization_13/AssignMovingAvg/sub_15base_model/batch_normalization_13/AssignMovingAvg/sub*
T0*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_13/moving_mean

Ebase_model/batch_normalization_13/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_13/moving_mean5base_model/batch_normalization_13/AssignMovingAvg/mul*@
_class6
42loc:@base_model/batch_normalization_13/moving_mean*
dtype0
ˇ
Bbase_model/batch_normalization_13/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_13/moving_meanF^base_model/batch_normalization_13/AssignMovingAvg/AssignSubVariableOp*
dtype0*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_13/moving_mean
Ä
9base_model/batch_normalization_13/AssignMovingAvg_1/sub/xConst*
dtype0*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance*
_output_shapes
: *
valueB
 *  ?

7base_model/batch_normalization_13/AssignMovingAvg_1/subSub9base_model/batch_normalization_13/AssignMovingAvg_1/sub/x)base_model/batch_normalization_13/Const_2*
T0*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance*
_output_shapes
: 
ą
Bbase_model/batch_normalization_13/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_13/moving_variance*
_output_shapes	
:*
dtype0
Ś
9base_model/batch_normalization_13/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_13/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_13/FusedBatchNormV3:2*
T0*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance*
_output_shapes	
:

7base_model/batch_normalization_13/AssignMovingAvg_1/mulMul9base_model/batch_normalization_13/AssignMovingAvg_1/sub_17base_model/batch_normalization_13/AssignMovingAvg_1/sub*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance*
T0*
_output_shapes	
:

Gbase_model/batch_normalization_13/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_13/moving_variance7base_model/batch_normalization_13/AssignMovingAvg_1/mul*
dtype0*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance
Ă
Dbase_model/batch_normalization_13/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_13/moving_varianceH^base_model/batch_normalization_13/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_13/moving_variance
Î
base_model/add_4AddV22base_model/batch_normalization_13/FusedBatchNormV32base_model/batch_normalization_11/FusedBatchNormV3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
y
base_model/Relu_10Relubase_model/add_4*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_14/kernel/Initializer/truncated_normal/shapeConst*.
_class$
" loc:@base_model/conv2d_14/kernel*%
valueB"            *
_output_shapes
:*
dtype0
˛
=base_model/conv2d_14/kernel/Initializer/truncated_normal/meanConst*
dtype0*.
_class$
" loc:@base_model/conv2d_14/kernel*
valueB
 *    *
_output_shapes
: 
´
?base_model/conv2d_14/kernel/Initializer/truncated_normal/stddevConst*
valueB
 *Â<*
dtype0*.
_class$
" loc:@base_model/conv2d_14/kernel*
_output_shapes
: 

Hbase_model/conv2d_14/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_14/kernel/Initializer/truncated_normal/shape*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_14/kernel*
dtype0*
T0
ą
<base_model/conv2d_14/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_14/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_14/kernel/Initializer/truncated_normal/stddev*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_14/kernel*
T0

8base_model/conv2d_14/kernel/Initializer/truncated_normalAdd<base_model/conv2d_14/kernel/Initializer/truncated_normal/mul=base_model/conv2d_14/kernel/Initializer/truncated_normal/mean*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_14/kernel*
T0
Ě
base_model/conv2d_14/kernelVarHandleOp*.
_class$
" loc:@base_model/conv2d_14/kernel*
shape:*,
shared_namebase_model/conv2d_14/kernel*
_output_shapes
: *
dtype0

<base_model/conv2d_14/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_14/kernel*
_output_shapes
: 

"base_model/conv2d_14/kernel/AssignAssignVariableOpbase_model/conv2d_14/kernel8base_model/conv2d_14/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_14/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_14/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_14/dilation_rateConst*
dtype0*
valueB"      *
_output_shapes
:

*base_model/conv2d_14/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_14/kernel*
dtype0*(
_output_shapes
:
Ú
base_model/conv2d_14/Conv2DConv2Dbase_model/Relu_10*base_model/conv2d_14/Conv2D/ReadVariableOp*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
strides
*
paddingSAME*
T0
Ă
8base_model/batch_normalization_14/gamma/Initializer/onesConst*:
_class0
.,loc:@base_model/batch_normalization_14/gamma*
valueB*  ?*
_output_shapes	
:*
dtype0
ă
'base_model/batch_normalization_14/gammaVarHandleOp*:
_class0
.,loc:@base_model/batch_normalization_14/gamma*8
shared_name)'base_model/batch_normalization_14/gamma*
dtype0*
_output_shapes
: *
shape:

Hbase_model/batch_normalization_14/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_14/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_14/gamma/AssignAssignVariableOp'base_model/batch_normalization_14/gamma8base_model/batch_normalization_14/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_14/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_14/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_14/beta/Initializer/zerosConst*
_output_shapes	
:*
dtype0*
valueB*    *9
_class/
-+loc:@base_model/batch_normalization_14/beta
ŕ
&base_model/batch_normalization_14/betaVarHandleOp*
dtype0*7
shared_name(&base_model/batch_normalization_14/beta*9
_class/
-+loc:@base_model/batch_normalization_14/beta*
_output_shapes
: *
shape:

Gbase_model/batch_normalization_14/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_14/beta*
_output_shapes
: 
°
-base_model/batch_normalization_14/beta/AssignAssignVariableOp&base_model/batch_normalization_14/beta8base_model/batch_normalization_14/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_14/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_14/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_14/moving_mean/Initializer/zerosConst*@
_class6
42loc:@base_model/batch_normalization_14/moving_mean*
_output_shapes	
:*
dtype0*
valueB*    
ő
-base_model/batch_normalization_14/moving_meanVarHandleOp*
shape:*
dtype0*>
shared_name/-base_model/batch_normalization_14/moving_mean*@
_class6
42loc:@base_model/batch_normalization_14/moving_mean*
_output_shapes
: 
Ť
Nbase_model/batch_normalization_14/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_14/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_14/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_14/moving_mean?base_model/batch_normalization_14/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_14/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_14/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_14/moving_variance/Initializer/onesConst*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
valueB*  ?*
dtype0

1base_model/batch_normalization_14/moving_varianceVarHandleOp*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
dtype0*
shape:*
_output_shapes
: *B
shared_name31base_model/batch_normalization_14/moving_variance
ł
Rbase_model/batch_normalization_14/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_14/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_14/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_14/moving_varianceBbase_model/batch_normalization_14/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_14/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_14/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_14/ReadVariableOpReadVariableOp'base_model/batch_normalization_14/gamma*
_output_shapes	
:*
dtype0

2base_model/batch_normalization_14/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_14/beta*
_output_shapes	
:*
dtype0
j
'base_model/batch_normalization_14/ConstConst*
dtype0*
valueB *
_output_shapes
: 
l
)base_model/batch_normalization_14/Const_1Const*
dtype0*
valueB *
_output_shapes
: 
Ľ
2base_model/batch_normalization_14/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_14/Conv2D0base_model/batch_normalization_14/ReadVariableOp2base_model/batch_normalization_14/ReadVariableOp_1'base_model/batch_normalization_14/Const)base_model/batch_normalization_14/Const_1*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
U0*
epsilon%đ'7*
T0
n
)base_model/batch_normalization_14/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *fff?
ž
7base_model/batch_normalization_14/AssignMovingAvg/sub/xConst*
dtype0*
_output_shapes
: *
valueB
 *  ?*@
_class6
42loc:@base_model/batch_normalization_14/moving_mean

5base_model/batch_normalization_14/AssignMovingAvg/subSub7base_model/batch_normalization_14/AssignMovingAvg/sub/x)base_model/batch_normalization_14/Const_2*@
_class6
42loc:@base_model/batch_normalization_14/moving_mean*
_output_shapes
: *
T0
Ť
@base_model/batch_normalization_14/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_14/moving_mean*
_output_shapes	
:*
dtype0

7base_model/batch_normalization_14/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_14/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_14/FusedBatchNormV3:1*@
_class6
42loc:@base_model/batch_normalization_14/moving_mean*
T0*
_output_shapes	
:

5base_model/batch_normalization_14/AssignMovingAvg/mulMul7base_model/batch_normalization_14/AssignMovingAvg/sub_15base_model/batch_normalization_14/AssignMovingAvg/sub*
_output_shapes	
:*
T0*@
_class6
42loc:@base_model/batch_normalization_14/moving_mean

Ebase_model/batch_normalization_14/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_14/moving_mean5base_model/batch_normalization_14/AssignMovingAvg/mul*
dtype0*@
_class6
42loc:@base_model/batch_normalization_14/moving_mean
ˇ
Bbase_model/batch_normalization_14/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_14/moving_meanF^base_model/batch_normalization_14/AssignMovingAvg/AssignSubVariableOp*
_output_shapes	
:*
dtype0*@
_class6
42loc:@base_model/batch_normalization_14/moving_mean
Ä
9base_model/batch_normalization_14/AssignMovingAvg_1/sub/xConst*
valueB
 *  ?*
dtype0*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
_output_shapes
: 

7base_model/batch_normalization_14/AssignMovingAvg_1/subSub9base_model/batch_normalization_14/AssignMovingAvg_1/sub/x)base_model/batch_normalization_14/Const_2*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
T0*
_output_shapes
: 
ą
Bbase_model/batch_normalization_14/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_14/moving_variance*
dtype0*
_output_shapes	
:
Ś
9base_model/batch_normalization_14/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_14/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_14/FusedBatchNormV3:2*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
T0*
_output_shapes	
:

7base_model/batch_normalization_14/AssignMovingAvg_1/mulMul9base_model/batch_normalization_14/AssignMovingAvg_1/sub_17base_model/batch_normalization_14/AssignMovingAvg_1/sub*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
_output_shapes	
:*
T0

Gbase_model/batch_normalization_14/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_14/moving_variance7base_model/batch_normalization_14/AssignMovingAvg_1/mul*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
dtype0
Ă
Dbase_model/batch_normalization_14/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_14/moving_varianceH^base_model/batch_normalization_14/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*D
_class:
86loc:@base_model/batch_normalization_14/moving_variance*
_output_shapes	
:

base_model/Relu_11Relu2base_model/batch_normalization_14/FusedBatchNormV3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ç
>base_model/conv2d_15/kernel/Initializer/truncated_normal/shapeConst*
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_15/kernel*
dtype0*%
valueB"            
˛
=base_model/conv2d_15/kernel/Initializer/truncated_normal/meanConst*
dtype0*.
_class$
" loc:@base_model/conv2d_15/kernel*
valueB
 *    *
_output_shapes
: 
´
?base_model/conv2d_15/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
dtype0*.
_class$
" loc:@base_model/conv2d_15/kernel*
valueB
 *Â<

Hbase_model/conv2d_15/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_15/kernel/Initializer/truncated_normal/shape*
dtype0*.
_class$
" loc:@base_model/conv2d_15/kernel*(
_output_shapes
:*
T0
ą
<base_model/conv2d_15/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_15/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_15/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_15/kernel*(
_output_shapes
:*
T0

8base_model/conv2d_15/kernel/Initializer/truncated_normalAdd<base_model/conv2d_15/kernel/Initializer/truncated_normal/mul=base_model/conv2d_15/kernel/Initializer/truncated_normal/mean*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_15/kernel
Ě
base_model/conv2d_15/kernelVarHandleOp*,
shared_namebase_model/conv2d_15/kernel*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_15/kernel*
dtype0*
shape:

<base_model/conv2d_15/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_15/kernel*
_output_shapes
: 

"base_model/conv2d_15/kernel/AssignAssignVariableOpbase_model/conv2d_15/kernel8base_model/conv2d_15/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_15/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_15/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_15/dilation_rateConst*
_output_shapes
:*
dtype0*
valueB"      

*base_model/conv2d_15/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_15/kernel*
dtype0*(
_output_shapes
:
Ú
base_model/conv2d_15/Conv2DConv2Dbase_model/Relu_11*base_model/conv2d_15/Conv2D/ReadVariableOp*
T0*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingSAME
Ä
9base_model/batch_normalization_15/gamma/Initializer/zerosConst*
dtype0*
_output_shapes	
:*:
_class0
.,loc:@base_model/batch_normalization_15/gamma*
valueB*    
ă
'base_model/batch_normalization_15/gammaVarHandleOp*:
_class0
.,loc:@base_model/batch_normalization_15/gamma*8
shared_name)'base_model/batch_normalization_15/gamma*
dtype0*
shape:*
_output_shapes
: 

Hbase_model/batch_normalization_15/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_15/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_15/gamma/AssignAssignVariableOp'base_model/batch_normalization_15/gamma9base_model/batch_normalization_15/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_15/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_15/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_15/beta/Initializer/zerosConst*9
_class/
-+loc:@base_model/batch_normalization_15/beta*
valueB*    *
dtype0*
_output_shapes	
:
ŕ
&base_model/batch_normalization_15/betaVarHandleOp*9
_class/
-+loc:@base_model/batch_normalization_15/beta*7
shared_name(&base_model/batch_normalization_15/beta*
_output_shapes
: *
dtype0*
shape:

Gbase_model/batch_normalization_15/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_15/beta*
_output_shapes
: 
°
-base_model/batch_normalization_15/beta/AssignAssignVariableOp&base_model/batch_normalization_15/beta8base_model/batch_normalization_15/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_15/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_15/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_15/moving_mean/Initializer/zerosConst*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean*
dtype0*
valueB*    
ő
-base_model/batch_normalization_15/moving_meanVarHandleOp*
dtype0*>
shared_name/-base_model/batch_normalization_15/moving_mean*
_output_shapes
: *
shape:*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean
Ť
Nbase_model/batch_normalization_15/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_15/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_15/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_15/moving_mean?base_model/batch_normalization_15/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_15/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_15/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_15/moving_variance/Initializer/onesConst*
dtype0*
valueB*  ?*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_15/moving_variance

1base_model/batch_normalization_15/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_15/moving_variance*
shape:*D
_class:
86loc:@base_model/batch_normalization_15/moving_variance*
_output_shapes
: *
dtype0
ł
Rbase_model/batch_normalization_15/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_15/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_15/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_15/moving_varianceBbase_model/batch_normalization_15/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_15/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_15/moving_variance*
_output_shapes	
:*
dtype0

0base_model/batch_normalization_15/ReadVariableOpReadVariableOp'base_model/batch_normalization_15/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_15/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_15/beta*
dtype0*
_output_shapes	
:
j
'base_model/batch_normalization_15/ConstConst*
_output_shapes
: *
valueB *
dtype0
l
)base_model/batch_normalization_15/Const_1Const*
dtype0*
valueB *
_output_shapes
: 
Ľ
2base_model/batch_normalization_15/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_15/Conv2D0base_model/batch_normalization_15/ReadVariableOp2base_model/batch_normalization_15/ReadVariableOp_1'base_model/batch_normalization_15/Const)base_model/batch_normalization_15/Const_1*
U0*
epsilon%đ'7*
T0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
n
)base_model/batch_normalization_15/Const_2Const*
dtype0*
valueB
 *fff?*
_output_shapes
: 
ž
7base_model/batch_normalization_15/AssignMovingAvg/sub/xConst*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean*
_output_shapes
: *
dtype0*
valueB
 *  ?

5base_model/batch_normalization_15/AssignMovingAvg/subSub7base_model/batch_normalization_15/AssignMovingAvg/sub/x)base_model/batch_normalization_15/Const_2*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean*
_output_shapes
: *
T0
Ť
@base_model/batch_normalization_15/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_15/moving_mean*
dtype0*
_output_shapes	
:

7base_model/batch_normalization_15/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_15/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_15/FusedBatchNormV3:1*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean*
T0*
_output_shapes	
:

5base_model/batch_normalization_15/AssignMovingAvg/mulMul7base_model/batch_normalization_15/AssignMovingAvg/sub_15base_model/batch_normalization_15/AssignMovingAvg/sub*
T0*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean*
_output_shapes	
:

Ebase_model/batch_normalization_15/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_15/moving_mean5base_model/batch_normalization_15/AssignMovingAvg/mul*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean*
dtype0
ˇ
Bbase_model/batch_normalization_15/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_15/moving_meanF^base_model/batch_normalization_15/AssignMovingAvg/AssignSubVariableOp*@
_class6
42loc:@base_model/batch_normalization_15/moving_mean*
_output_shapes	
:*
dtype0
Ä
9base_model/batch_normalization_15/AssignMovingAvg_1/sub/xConst*
_output_shapes
: *
valueB
 *  ?*
dtype0*D
_class:
86loc:@base_model/batch_normalization_15/moving_variance

7base_model/batch_normalization_15/AssignMovingAvg_1/subSub9base_model/batch_normalization_15/AssignMovingAvg_1/sub/x)base_model/batch_normalization_15/Const_2*
_output_shapes
: *D
_class:
86loc:@base_model/batch_normalization_15/moving_variance*
T0
ą
Bbase_model/batch_normalization_15/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_15/moving_variance*
_output_shapes	
:*
dtype0
Ś
9base_model/batch_normalization_15/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_15/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_15/FusedBatchNormV3:2*
T0*D
_class:
86loc:@base_model/batch_normalization_15/moving_variance*
_output_shapes	
:

7base_model/batch_normalization_15/AssignMovingAvg_1/mulMul9base_model/batch_normalization_15/AssignMovingAvg_1/sub_17base_model/batch_normalization_15/AssignMovingAvg_1/sub*D
_class:
86loc:@base_model/batch_normalization_15/moving_variance*
T0*
_output_shapes	
:

Gbase_model/batch_normalization_15/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_15/moving_variance7base_model/batch_normalization_15/AssignMovingAvg_1/mul*
dtype0*D
_class:
86loc:@base_model/batch_normalization_15/moving_variance
Ă
Dbase_model/batch_normalization_15/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_15/moving_varianceH^base_model/batch_normalization_15/AssignMovingAvg_1/AssignSubVariableOp*D
_class:
86loc:@base_model/batch_normalization_15/moving_variance*
dtype0*
_output_shapes	
:
Ž
base_model/add_5AddV22base_model/batch_normalization_15/FusedBatchNormV3base_model/Relu_10*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
y
base_model/Relu_12Relubase_model/add_5*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

base_model/block_group3Identitybase_model/Relu_12*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

base_model/Pad_5/paddingsConst*
dtype0*
_output_shapes

:*9
value0B."                                 

base_model/Pad_5Padbase_model/block_group3base_model/Pad_5/paddings*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_16/kernel/Initializer/truncated_normal/shapeConst*.
_class$
" loc:@base_model/conv2d_16/kernel*%
valueB"            *
_output_shapes
:*
dtype0
˛
=base_model/conv2d_16/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
dtype0*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_16/kernel
´
?base_model/conv2d_16/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
dtype0*
valueB
 *6=*.
_class$
" loc:@base_model/conv2d_16/kernel

Hbase_model/conv2d_16/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_16/kernel/Initializer/truncated_normal/shape*.
_class$
" loc:@base_model/conv2d_16/kernel*
dtype0*(
_output_shapes
:*
T0
ą
<base_model/conv2d_16/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_16/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_16/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_16/kernel*(
_output_shapes
:*
T0

8base_model/conv2d_16/kernel/Initializer/truncated_normalAdd<base_model/conv2d_16/kernel/Initializer/truncated_normal/mul=base_model/conv2d_16/kernel/Initializer/truncated_normal/mean*.
_class$
" loc:@base_model/conv2d_16/kernel*(
_output_shapes
:*
T0
Ě
base_model/conv2d_16/kernelVarHandleOp*
dtype0*
shape:*.
_class$
" loc:@base_model/conv2d_16/kernel*,
shared_namebase_model/conv2d_16/kernel*
_output_shapes
: 

<base_model/conv2d_16/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_16/kernel*
_output_shapes
: 

"base_model/conv2d_16/kernel/AssignAssignVariableOpbase_model/conv2d_16/kernel8base_model/conv2d_16/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_16/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_16/kernel*
dtype0*(
_output_shapes
:
s
"base_model/conv2d_16/dilation_rateConst*
dtype0*
valueB"      *
_output_shapes
:

*base_model/conv2d_16/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_16/kernel*(
_output_shapes
:*
dtype0
Ů
base_model/conv2d_16/Conv2DConv2Dbase_model/Pad_5*base_model/conv2d_16/Conv2D/ReadVariableOp*
strides
*
T0*
paddingVALID*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ă
8base_model/batch_normalization_16/gamma/Initializer/onesConst*
valueB*  ?*:
_class0
.,loc:@base_model/batch_normalization_16/gamma*
dtype0*
_output_shapes	
:
ă
'base_model/batch_normalization_16/gammaVarHandleOp*:
_class0
.,loc:@base_model/batch_normalization_16/gamma*
shape:*
dtype0*8
shared_name)'base_model/batch_normalization_16/gamma*
_output_shapes
: 

Hbase_model/batch_normalization_16/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_16/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_16/gamma/AssignAssignVariableOp'base_model/batch_normalization_16/gamma8base_model/batch_normalization_16/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_16/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_16/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_16/beta/Initializer/zerosConst*
dtype0*
valueB*    *9
_class/
-+loc:@base_model/batch_normalization_16/beta*
_output_shapes	
:
ŕ
&base_model/batch_normalization_16/betaVarHandleOp*
shape:*7
shared_name(&base_model/batch_normalization_16/beta*
_output_shapes
: *
dtype0*9
_class/
-+loc:@base_model/batch_normalization_16/beta

Gbase_model/batch_normalization_16/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_16/beta*
_output_shapes
: 
°
-base_model/batch_normalization_16/beta/AssignAssignVariableOp&base_model/batch_normalization_16/beta8base_model/batch_normalization_16/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_16/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_16/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_16/moving_mean/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@base_model/batch_normalization_16/moving_mean*
_output_shapes	
:*
dtype0
ő
-base_model/batch_normalization_16/moving_meanVarHandleOp*@
_class6
42loc:@base_model/batch_normalization_16/moving_mean*
_output_shapes
: *>
shared_name/-base_model/batch_normalization_16/moving_mean*
dtype0*
shape:
Ť
Nbase_model/batch_normalization_16/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_16/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_16/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_16/moving_mean?base_model/batch_normalization_16/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_16/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_16/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_16/moving_variance/Initializer/onesConst*
_output_shapes	
:*
valueB*  ?*D
_class:
86loc:@base_model/batch_normalization_16/moving_variance*
dtype0

1base_model/batch_normalization_16/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_16/moving_variance*
shape:*
dtype0*D
_class:
86loc:@base_model/batch_normalization_16/moving_variance*
_output_shapes
: 
ł
Rbase_model/batch_normalization_16/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_16/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_16/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_16/moving_varianceBbase_model/batch_normalization_16/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_16/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_16/moving_variance*
_output_shapes	
:*
dtype0

0base_model/batch_normalization_16/ReadVariableOpReadVariableOp'base_model/batch_normalization_16/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_16/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_16/beta*
dtype0*
_output_shapes	
:
j
'base_model/batch_normalization_16/ConstConst*
dtype0*
valueB *
_output_shapes
: 
l
)base_model/batch_normalization_16/Const_1Const*
_output_shapes
: *
dtype0*
valueB 
Ľ
2base_model/batch_normalization_16/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_16/Conv2D0base_model/batch_normalization_16/ReadVariableOp2base_model/batch_normalization_16/ReadVariableOp_1'base_model/batch_normalization_16/Const)base_model/batch_normalization_16/Const_1*
U0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
T0*
epsilon%đ'7
n
)base_model/batch_normalization_16/Const_2Const*
dtype0*
_output_shapes
: *
valueB
 *fff?
ž
7base_model/batch_normalization_16/AssignMovingAvg/sub/xConst*
_output_shapes
: *
valueB
 *  ?*
dtype0*@
_class6
42loc:@base_model/batch_normalization_16/moving_mean

5base_model/batch_normalization_16/AssignMovingAvg/subSub7base_model/batch_normalization_16/AssignMovingAvg/sub/x)base_model/batch_normalization_16/Const_2*
_output_shapes
: *
T0*@
_class6
42loc:@base_model/batch_normalization_16/moving_mean
Ť
@base_model/batch_normalization_16/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_16/moving_mean*
dtype0*
_output_shapes	
:

7base_model/batch_normalization_16/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_16/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_16/FusedBatchNormV3:1*
_output_shapes	
:*
T0*@
_class6
42loc:@base_model/batch_normalization_16/moving_mean

5base_model/batch_normalization_16/AssignMovingAvg/mulMul7base_model/batch_normalization_16/AssignMovingAvg/sub_15base_model/batch_normalization_16/AssignMovingAvg/sub*@
_class6
42loc:@base_model/batch_normalization_16/moving_mean*
_output_shapes	
:*
T0

Ebase_model/batch_normalization_16/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_16/moving_mean5base_model/batch_normalization_16/AssignMovingAvg/mul*
dtype0*@
_class6
42loc:@base_model/batch_normalization_16/moving_mean
ˇ
Bbase_model/batch_normalization_16/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_16/moving_meanF^base_model/batch_normalization_16/AssignMovingAvg/AssignSubVariableOp*
dtype0*@
_class6
42loc:@base_model/batch_normalization_16/moving_mean*
_output_shapes	
:
Ä
9base_model/batch_normalization_16/AssignMovingAvg_1/sub/xConst*
_output_shapes
: *D
_class:
86loc:@base_model/batch_normalization_16/moving_variance*
valueB
 *  ?*
dtype0

7base_model/batch_normalization_16/AssignMovingAvg_1/subSub9base_model/batch_normalization_16/AssignMovingAvg_1/sub/x)base_model/batch_normalization_16/Const_2*D
_class:
86loc:@base_model/batch_normalization_16/moving_variance*
T0*
_output_shapes
: 
ą
Bbase_model/batch_normalization_16/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_16/moving_variance*
_output_shapes	
:*
dtype0
Ś
9base_model/batch_normalization_16/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_16/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_16/FusedBatchNormV3:2*D
_class:
86loc:@base_model/batch_normalization_16/moving_variance*
T0*
_output_shapes	
:

7base_model/batch_normalization_16/AssignMovingAvg_1/mulMul9base_model/batch_normalization_16/AssignMovingAvg_1/sub_17base_model/batch_normalization_16/AssignMovingAvg_1/sub*
_output_shapes	
:*
T0*D
_class:
86loc:@base_model/batch_normalization_16/moving_variance

Gbase_model/batch_normalization_16/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_16/moving_variance7base_model/batch_normalization_16/AssignMovingAvg_1/mul*D
_class:
86loc:@base_model/batch_normalization_16/moving_variance*
dtype0
Ă
Dbase_model/batch_normalization_16/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_16/moving_varianceH^base_model/batch_normalization_16/AssignMovingAvg_1/AssignSubVariableOp*D
_class:
86loc:@base_model/batch_normalization_16/moving_variance*
_output_shapes	
:*
dtype0

base_model/Pad_6/paddingsConst*
_output_shapes

:*
dtype0*9
value0B."                             

base_model/Pad_6Padbase_model/block_group3base_model/Pad_6/paddings*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_17/kernel/Initializer/truncated_normal/shapeConst*
_output_shapes
:*%
valueB"            *
dtype0*.
_class$
" loc:@base_model/conv2d_17/kernel
˛
=base_model/conv2d_17/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_17/kernel*
valueB
 *    *
dtype0
´
?base_model/conv2d_17/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *
valueB
 *Â<*
dtype0*.
_class$
" loc:@base_model/conv2d_17/kernel

Hbase_model/conv2d_17/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_17/kernel/Initializer/truncated_normal/shape*
T0*
dtype0*.
_class$
" loc:@base_model/conv2d_17/kernel*(
_output_shapes
:
ą
<base_model/conv2d_17/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_17/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_17/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_17/kernel*
T0*(
_output_shapes
:

8base_model/conv2d_17/kernel/Initializer/truncated_normalAdd<base_model/conv2d_17/kernel/Initializer/truncated_normal/mul=base_model/conv2d_17/kernel/Initializer/truncated_normal/mean*(
_output_shapes
:*
T0*.
_class$
" loc:@base_model/conv2d_17/kernel
Ě
base_model/conv2d_17/kernelVarHandleOp*
dtype0*
shape:*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_17/kernel*,
shared_namebase_model/conv2d_17/kernel

<base_model/conv2d_17/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_17/kernel*
_output_shapes
: 

"base_model/conv2d_17/kernel/AssignAssignVariableOpbase_model/conv2d_17/kernel8base_model/conv2d_17/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_17/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_17/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_17/dilation_rateConst*
valueB"      *
_output_shapes
:*
dtype0

*base_model/conv2d_17/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_17/kernel*(
_output_shapes
:*
dtype0
Ů
base_model/conv2d_17/Conv2DConv2Dbase_model/Pad_6*base_model/conv2d_17/Conv2D/ReadVariableOp*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
paddingVALID*
T0*
strides

Ă
8base_model/batch_normalization_17/gamma/Initializer/onesConst*:
_class0
.,loc:@base_model/batch_normalization_17/gamma*
valueB*  ?*
dtype0*
_output_shapes	
:
ă
'base_model/batch_normalization_17/gammaVarHandleOp*
shape:*8
shared_name)'base_model/batch_normalization_17/gamma*:
_class0
.,loc:@base_model/batch_normalization_17/gamma*
_output_shapes
: *
dtype0

Hbase_model/batch_normalization_17/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_17/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_17/gamma/AssignAssignVariableOp'base_model/batch_normalization_17/gamma8base_model/batch_normalization_17/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_17/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_17/gamma*
dtype0*
_output_shapes	
:
Â
8base_model/batch_normalization_17/beta/Initializer/zerosConst*
_output_shapes	
:*9
_class/
-+loc:@base_model/batch_normalization_17/beta*
valueB*    *
dtype0
ŕ
&base_model/batch_normalization_17/betaVarHandleOp*
dtype0*
shape:*7
shared_name(&base_model/batch_normalization_17/beta*9
_class/
-+loc:@base_model/batch_normalization_17/beta*
_output_shapes
: 

Gbase_model/batch_normalization_17/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_17/beta*
_output_shapes
: 
°
-base_model/batch_normalization_17/beta/AssignAssignVariableOp&base_model/batch_normalization_17/beta8base_model/batch_normalization_17/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_17/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_17/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_17/moving_mean/Initializer/zerosConst*
valueB*    *@
_class6
42loc:@base_model/batch_normalization_17/moving_mean*
_output_shapes	
:*
dtype0
ő
-base_model/batch_normalization_17/moving_meanVarHandleOp*>
shared_name/-base_model/batch_normalization_17/moving_mean*
_output_shapes
: *
dtype0*
shape:*@
_class6
42loc:@base_model/batch_normalization_17/moving_mean
Ť
Nbase_model/batch_normalization_17/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_17/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_17/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_17/moving_mean?base_model/batch_normalization_17/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_17/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_17/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_17/moving_variance/Initializer/onesConst*
dtype0*D
_class:
86loc:@base_model/batch_normalization_17/moving_variance*
valueB*  ?*
_output_shapes	
:

1base_model/batch_normalization_17/moving_varianceVarHandleOp*
_output_shapes
: *
shape:*B
shared_name31base_model/batch_normalization_17/moving_variance*D
_class:
86loc:@base_model/batch_normalization_17/moving_variance*
dtype0
ł
Rbase_model/batch_normalization_17/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_17/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_17/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_17/moving_varianceBbase_model/batch_normalization_17/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_17/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_17/moving_variance*
_output_shapes	
:*
dtype0

0base_model/batch_normalization_17/ReadVariableOpReadVariableOp'base_model/batch_normalization_17/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_17/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_17/beta*
dtype0*
_output_shapes	
:
j
'base_model/batch_normalization_17/ConstConst*
valueB *
dtype0*
_output_shapes
: 
l
)base_model/batch_normalization_17/Const_1Const*
_output_shapes
: *
valueB *
dtype0
Ľ
2base_model/batch_normalization_17/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_17/Conv2D0base_model/batch_normalization_17/ReadVariableOp2base_model/batch_normalization_17/ReadVariableOp_1'base_model/batch_normalization_17/Const)base_model/batch_normalization_17/Const_1*
epsilon%đ'7*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
U0*
T0
n
)base_model/batch_normalization_17/Const_2Const*
dtype0*
valueB
 *fff?*
_output_shapes
: 
ž
7base_model/batch_normalization_17/AssignMovingAvg/sub/xConst*
valueB
 *  ?*@
_class6
42loc:@base_model/batch_normalization_17/moving_mean*
_output_shapes
: *
dtype0

5base_model/batch_normalization_17/AssignMovingAvg/subSub7base_model/batch_normalization_17/AssignMovingAvg/sub/x)base_model/batch_normalization_17/Const_2*
_output_shapes
: *
T0*@
_class6
42loc:@base_model/batch_normalization_17/moving_mean
Ť
@base_model/batch_normalization_17/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_17/moving_mean*
_output_shapes	
:*
dtype0

7base_model/batch_normalization_17/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_17/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_17/FusedBatchNormV3:1*
T0*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_17/moving_mean

5base_model/batch_normalization_17/AssignMovingAvg/mulMul7base_model/batch_normalization_17/AssignMovingAvg/sub_15base_model/batch_normalization_17/AssignMovingAvg/sub*
_output_shapes	
:*
T0*@
_class6
42loc:@base_model/batch_normalization_17/moving_mean

Ebase_model/batch_normalization_17/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_17/moving_mean5base_model/batch_normalization_17/AssignMovingAvg/mul*@
_class6
42loc:@base_model/batch_normalization_17/moving_mean*
dtype0
ˇ
Bbase_model/batch_normalization_17/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_17/moving_meanF^base_model/batch_normalization_17/AssignMovingAvg/AssignSubVariableOp*
_output_shapes	
:*
dtype0*@
_class6
42loc:@base_model/batch_normalization_17/moving_mean
Ä
9base_model/batch_normalization_17/AssignMovingAvg_1/sub/xConst*
_output_shapes
: *
valueB
 *  ?*
dtype0*D
_class:
86loc:@base_model/batch_normalization_17/moving_variance

7base_model/batch_normalization_17/AssignMovingAvg_1/subSub9base_model/batch_normalization_17/AssignMovingAvg_1/sub/x)base_model/batch_normalization_17/Const_2*
_output_shapes
: *D
_class:
86loc:@base_model/batch_normalization_17/moving_variance*
T0
ą
Bbase_model/batch_normalization_17/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_17/moving_variance*
dtype0*
_output_shapes	
:
Ś
9base_model/batch_normalization_17/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_17/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_17/FusedBatchNormV3:2*D
_class:
86loc:@base_model/batch_normalization_17/moving_variance*
_output_shapes	
:*
T0

7base_model/batch_normalization_17/AssignMovingAvg_1/mulMul9base_model/batch_normalization_17/AssignMovingAvg_1/sub_17base_model/batch_normalization_17/AssignMovingAvg_1/sub*
T0*D
_class:
86loc:@base_model/batch_normalization_17/moving_variance*
_output_shapes	
:

Gbase_model/batch_normalization_17/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_17/moving_variance7base_model/batch_normalization_17/AssignMovingAvg_1/mul*D
_class:
86loc:@base_model/batch_normalization_17/moving_variance*
dtype0
Ă
Dbase_model/batch_normalization_17/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_17/moving_varianceH^base_model/batch_normalization_17/AssignMovingAvg_1/AssignSubVariableOp*D
_class:
86loc:@base_model/batch_normalization_17/moving_variance*
dtype0*
_output_shapes	
:

base_model/Relu_13Relu2base_model/batch_normalization_17/FusedBatchNormV3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_18/kernel/Initializer/truncated_normal/shapeConst*.
_class$
" loc:@base_model/conv2d_18/kernel*%
valueB"            *
dtype0*
_output_shapes
:
˛
=base_model/conv2d_18/kernel/Initializer/truncated_normal/meanConst*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_18/kernel*
dtype0*
valueB
 *    
´
?base_model/conv2d_18/kernel/Initializer/truncated_normal/stddevConst*.
_class$
" loc:@base_model/conv2d_18/kernel*
_output_shapes
: *
dtype0*
valueB
 *¸1<

Hbase_model/conv2d_18/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_18/kernel/Initializer/truncated_normal/shape*
dtype0*.
_class$
" loc:@base_model/conv2d_18/kernel*
T0*(
_output_shapes
:
ą
<base_model/conv2d_18/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_18/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_18/kernel/Initializer/truncated_normal/stddev*.
_class$
" loc:@base_model/conv2d_18/kernel*(
_output_shapes
:*
T0

8base_model/conv2d_18/kernel/Initializer/truncated_normalAdd<base_model/conv2d_18/kernel/Initializer/truncated_normal/mul=base_model/conv2d_18/kernel/Initializer/truncated_normal/mean*(
_output_shapes
:*
T0*.
_class$
" loc:@base_model/conv2d_18/kernel
Ě
base_model/conv2d_18/kernelVarHandleOp*,
shared_namebase_model/conv2d_18/kernel*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_18/kernel*
dtype0*
shape:

<base_model/conv2d_18/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_18/kernel*
_output_shapes
: 

"base_model/conv2d_18/kernel/AssignAssignVariableOpbase_model/conv2d_18/kernel8base_model/conv2d_18/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_18/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_18/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_18/dilation_rateConst*
valueB"      *
_output_shapes
:*
dtype0

*base_model/conv2d_18/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_18/kernel*
dtype0*(
_output_shapes
:
Ú
base_model/conv2d_18/Conv2DConv2Dbase_model/Relu_13*base_model/conv2d_18/Conv2D/ReadVariableOp*
T0*
paddingSAME*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ä
9base_model/batch_normalization_18/gamma/Initializer/zerosConst*
dtype0*:
_class0
.,loc:@base_model/batch_normalization_18/gamma*
_output_shapes	
:*
valueB*    
ă
'base_model/batch_normalization_18/gammaVarHandleOp*
_output_shapes
: *
shape:*
dtype0*:
_class0
.,loc:@base_model/batch_normalization_18/gamma*8
shared_name)'base_model/batch_normalization_18/gamma

Hbase_model/batch_normalization_18/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_18/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_18/gamma/AssignAssignVariableOp'base_model/batch_normalization_18/gamma9base_model/batch_normalization_18/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_18/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_18/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_18/beta/Initializer/zerosConst*
dtype0*9
_class/
-+loc:@base_model/batch_normalization_18/beta*
valueB*    *
_output_shapes	
:
ŕ
&base_model/batch_normalization_18/betaVarHandleOp*
shape:*7
shared_name(&base_model/batch_normalization_18/beta*9
_class/
-+loc:@base_model/batch_normalization_18/beta*
dtype0*
_output_shapes
: 

Gbase_model/batch_normalization_18/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_18/beta*
_output_shapes
: 
°
-base_model/batch_normalization_18/beta/AssignAssignVariableOp&base_model/batch_normalization_18/beta8base_model/batch_normalization_18/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_18/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_18/beta*
_output_shapes	
:*
dtype0
Đ
?base_model/batch_normalization_18/moving_mean/Initializer/zerosConst*
_output_shapes	
:*
dtype0*
valueB*    *@
_class6
42loc:@base_model/batch_normalization_18/moving_mean
ő
-base_model/batch_normalization_18/moving_meanVarHandleOp*
_output_shapes
: *@
_class6
42loc:@base_model/batch_normalization_18/moving_mean*>
shared_name/-base_model/batch_normalization_18/moving_mean*
dtype0*
shape:
Ť
Nbase_model/batch_normalization_18/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_18/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_18/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_18/moving_mean?base_model/batch_normalization_18/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_18/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_18/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_18/moving_variance/Initializer/onesConst*
valueB*  ?*D
_class:
86loc:@base_model/batch_normalization_18/moving_variance*
dtype0*
_output_shapes	
:

1base_model/batch_normalization_18/moving_varianceVarHandleOp*
shape:*D
_class:
86loc:@base_model/batch_normalization_18/moving_variance*
dtype0*
_output_shapes
: *B
shared_name31base_model/batch_normalization_18/moving_variance
ł
Rbase_model/batch_normalization_18/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_18/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_18/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_18/moving_varianceBbase_model/batch_normalization_18/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_18/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_18/moving_variance*
_output_shapes	
:*
dtype0

0base_model/batch_normalization_18/ReadVariableOpReadVariableOp'base_model/batch_normalization_18/gamma*
_output_shapes	
:*
dtype0

2base_model/batch_normalization_18/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_18/beta*
_output_shapes	
:*
dtype0
j
'base_model/batch_normalization_18/ConstConst*
valueB *
_output_shapes
: *
dtype0
l
)base_model/batch_normalization_18/Const_1Const*
dtype0*
_output_shapes
: *
valueB 
Ľ
2base_model/batch_normalization_18/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_18/Conv2D0base_model/batch_normalization_18/ReadVariableOp2base_model/batch_normalization_18/ReadVariableOp_1'base_model/batch_normalization_18/Const)base_model/batch_normalization_18/Const_1*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::*
epsilon%đ'7*
T0*
U0
n
)base_model/batch_normalization_18/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *fff?
ž
7base_model/batch_normalization_18/AssignMovingAvg/sub/xConst*@
_class6
42loc:@base_model/batch_normalization_18/moving_mean*
valueB
 *  ?*
dtype0*
_output_shapes
: 

5base_model/batch_normalization_18/AssignMovingAvg/subSub7base_model/batch_normalization_18/AssignMovingAvg/sub/x)base_model/batch_normalization_18/Const_2*
T0*@
_class6
42loc:@base_model/batch_normalization_18/moving_mean*
_output_shapes
: 
Ť
@base_model/batch_normalization_18/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_18/moving_mean*
dtype0*
_output_shapes	
:

7base_model/batch_normalization_18/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_18/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_18/FusedBatchNormV3:1*
T0*@
_class6
42loc:@base_model/batch_normalization_18/moving_mean*
_output_shapes	
:

5base_model/batch_normalization_18/AssignMovingAvg/mulMul7base_model/batch_normalization_18/AssignMovingAvg/sub_15base_model/batch_normalization_18/AssignMovingAvg/sub*
_output_shapes	
:*
T0*@
_class6
42loc:@base_model/batch_normalization_18/moving_mean

Ebase_model/batch_normalization_18/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_18/moving_mean5base_model/batch_normalization_18/AssignMovingAvg/mul*
dtype0*@
_class6
42loc:@base_model/batch_normalization_18/moving_mean
ˇ
Bbase_model/batch_normalization_18/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_18/moving_meanF^base_model/batch_normalization_18/AssignMovingAvg/AssignSubVariableOp*
_output_shapes	
:*
dtype0*@
_class6
42loc:@base_model/batch_normalization_18/moving_mean
Ä
9base_model/batch_normalization_18/AssignMovingAvg_1/sub/xConst*
valueB
 *  ?*
_output_shapes
: *D
_class:
86loc:@base_model/batch_normalization_18/moving_variance*
dtype0

7base_model/batch_normalization_18/AssignMovingAvg_1/subSub9base_model/batch_normalization_18/AssignMovingAvg_1/sub/x)base_model/batch_normalization_18/Const_2*D
_class:
86loc:@base_model/batch_normalization_18/moving_variance*
_output_shapes
: *
T0
ą
Bbase_model/batch_normalization_18/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_18/moving_variance*
dtype0*
_output_shapes	
:
Ś
9base_model/batch_normalization_18/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_18/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_18/FusedBatchNormV3:2*
_output_shapes	
:*
T0*D
_class:
86loc:@base_model/batch_normalization_18/moving_variance

7base_model/batch_normalization_18/AssignMovingAvg_1/mulMul9base_model/batch_normalization_18/AssignMovingAvg_1/sub_17base_model/batch_normalization_18/AssignMovingAvg_1/sub*
T0*D
_class:
86loc:@base_model/batch_normalization_18/moving_variance*
_output_shapes	
:

Gbase_model/batch_normalization_18/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_18/moving_variance7base_model/batch_normalization_18/AssignMovingAvg_1/mul*D
_class:
86loc:@base_model/batch_normalization_18/moving_variance*
dtype0
Ă
Dbase_model/batch_normalization_18/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_18/moving_varianceH^base_model/batch_normalization_18/AssignMovingAvg_1/AssignSubVariableOp*
dtype0*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_18/moving_variance
Î
base_model/add_6AddV22base_model/batch_normalization_18/FusedBatchNormV32base_model/batch_normalization_16/FusedBatchNormV3*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
y
base_model/Relu_14Relubase_model/add_6*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙
Ç
>base_model/conv2d_19/kernel/Initializer/truncated_normal/shapeConst*
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_19/kernel*
dtype0*%
valueB"            
˛
=base_model/conv2d_19/kernel/Initializer/truncated_normal/meanConst*.
_class$
" loc:@base_model/conv2d_19/kernel*
_output_shapes
: *
dtype0*
valueB
 *    
´
?base_model/conv2d_19/kernel/Initializer/truncated_normal/stddevConst*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_19/kernel*
valueB
 *¸1<*
dtype0

Hbase_model/conv2d_19/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_19/kernel/Initializer/truncated_normal/shape*
dtype0*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_19/kernel
ą
<base_model/conv2d_19/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_19/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_19/kernel/Initializer/truncated_normal/stddev*(
_output_shapes
:*
T0*.
_class$
" loc:@base_model/conv2d_19/kernel

8base_model/conv2d_19/kernel/Initializer/truncated_normalAdd<base_model/conv2d_19/kernel/Initializer/truncated_normal/mul=base_model/conv2d_19/kernel/Initializer/truncated_normal/mean*.
_class$
" loc:@base_model/conv2d_19/kernel*(
_output_shapes
:*
T0
Ě
base_model/conv2d_19/kernelVarHandleOp*
shape:*,
shared_namebase_model/conv2d_19/kernel*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_19/kernel*
dtype0

<base_model/conv2d_19/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_19/kernel*
_output_shapes
: 

"base_model/conv2d_19/kernel/AssignAssignVariableOpbase_model/conv2d_19/kernel8base_model/conv2d_19/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_19/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_19/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_19/dilation_rateConst*
valueB"      *
_output_shapes
:*
dtype0

*base_model/conv2d_19/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_19/kernel*
dtype0*(
_output_shapes
:
Ú
base_model/conv2d_19/Conv2DConv2Dbase_model/Relu_14*base_model/conv2d_19/Conv2D/ReadVariableOp*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
strides
*
paddingSAME
Ă
8base_model/batch_normalization_19/gamma/Initializer/onesConst*:
_class0
.,loc:@base_model/batch_normalization_19/gamma*
valueB*  ?*
dtype0*
_output_shapes	
:
ă
'base_model/batch_normalization_19/gammaVarHandleOp*
_output_shapes
: *
shape:*:
_class0
.,loc:@base_model/batch_normalization_19/gamma*
dtype0*8
shared_name)'base_model/batch_normalization_19/gamma

Hbase_model/batch_normalization_19/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_19/gamma*
_output_shapes
: 
˛
.base_model/batch_normalization_19/gamma/AssignAssignVariableOp'base_model/batch_normalization_19/gamma8base_model/batch_normalization_19/gamma/Initializer/ones*
dtype0
 
;base_model/batch_normalization_19/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_19/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_19/beta/Initializer/zerosConst*9
_class/
-+loc:@base_model/batch_normalization_19/beta*
valueB*    *
_output_shapes	
:*
dtype0
ŕ
&base_model/batch_normalization_19/betaVarHandleOp*7
shared_name(&base_model/batch_normalization_19/beta*
dtype0*
shape:*9
_class/
-+loc:@base_model/batch_normalization_19/beta*
_output_shapes
: 

Gbase_model/batch_normalization_19/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_19/beta*
_output_shapes
: 
°
-base_model/batch_normalization_19/beta/AssignAssignVariableOp&base_model/batch_normalization_19/beta8base_model/batch_normalization_19/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_19/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_19/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_19/moving_mean/Initializer/zerosConst*
valueB*    *
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_19/moving_mean*
dtype0
ő
-base_model/batch_normalization_19/moving_meanVarHandleOp*
dtype0*>
shared_name/-base_model/batch_normalization_19/moving_mean*@
_class6
42loc:@base_model/batch_normalization_19/moving_mean*
_output_shapes
: *
shape:
Ť
Nbase_model/batch_normalization_19/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_19/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_19/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_19/moving_mean?base_model/batch_normalization_19/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_19/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_19/moving_mean*
_output_shapes	
:*
dtype0
×
Bbase_model/batch_normalization_19/moving_variance/Initializer/onesConst*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_19/moving_variance*
valueB*  ?*
dtype0

1base_model/batch_normalization_19/moving_varianceVarHandleOp*D
_class:
86loc:@base_model/batch_normalization_19/moving_variance*B
shared_name31base_model/batch_normalization_19/moving_variance*
shape:*
_output_shapes
: *
dtype0
ł
Rbase_model/batch_normalization_19/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_19/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_19/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_19/moving_varianceBbase_model/batch_normalization_19/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_19/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_19/moving_variance*
_output_shapes	
:*
dtype0

0base_model/batch_normalization_19/ReadVariableOpReadVariableOp'base_model/batch_normalization_19/gamma*
dtype0*
_output_shapes	
:

2base_model/batch_normalization_19/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_19/beta*
dtype0*
_output_shapes	
:
j
'base_model/batch_normalization_19/ConstConst*
valueB *
_output_shapes
: *
dtype0
l
)base_model/batch_normalization_19/Const_1Const*
valueB *
dtype0*
_output_shapes
: 
Ľ
2base_model/batch_normalization_19/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_19/Conv2D0base_model/batch_normalization_19/ReadVariableOp2base_model/batch_normalization_19/ReadVariableOp_1'base_model/batch_normalization_19/Const)base_model/batch_normalization_19/Const_1*
T0*
epsilon%đ'7*
U0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
n
)base_model/batch_normalization_19/Const_2Const*
valueB
 *fff?*
_output_shapes
: *
dtype0
ž
7base_model/batch_normalization_19/AssignMovingAvg/sub/xConst*@
_class6
42loc:@base_model/batch_normalization_19/moving_mean*
dtype0*
valueB
 *  ?*
_output_shapes
: 

5base_model/batch_normalization_19/AssignMovingAvg/subSub7base_model/batch_normalization_19/AssignMovingAvg/sub/x)base_model/batch_normalization_19/Const_2*
_output_shapes
: *
T0*@
_class6
42loc:@base_model/batch_normalization_19/moving_mean
Ť
@base_model/batch_normalization_19/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_19/moving_mean*
dtype0*
_output_shapes	
:

7base_model/batch_normalization_19/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_19/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_19/FusedBatchNormV3:1*
T0*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_19/moving_mean

5base_model/batch_normalization_19/AssignMovingAvg/mulMul7base_model/batch_normalization_19/AssignMovingAvg/sub_15base_model/batch_normalization_19/AssignMovingAvg/sub*@
_class6
42loc:@base_model/batch_normalization_19/moving_mean*
_output_shapes	
:*
T0

Ebase_model/batch_normalization_19/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_19/moving_mean5base_model/batch_normalization_19/AssignMovingAvg/mul*@
_class6
42loc:@base_model/batch_normalization_19/moving_mean*
dtype0
ˇ
Bbase_model/batch_normalization_19/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_19/moving_meanF^base_model/batch_normalization_19/AssignMovingAvg/AssignSubVariableOp*
_output_shapes	
:*
dtype0*@
_class6
42loc:@base_model/batch_normalization_19/moving_mean
Ä
9base_model/batch_normalization_19/AssignMovingAvg_1/sub/xConst*D
_class:
86loc:@base_model/batch_normalization_19/moving_variance*
dtype0*
_output_shapes
: *
valueB
 *  ?

7base_model/batch_normalization_19/AssignMovingAvg_1/subSub9base_model/batch_normalization_19/AssignMovingAvg_1/sub/x)base_model/batch_normalization_19/Const_2*
_output_shapes
: *D
_class:
86loc:@base_model/batch_normalization_19/moving_variance*
T0
ą
Bbase_model/batch_normalization_19/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_19/moving_variance*
_output_shapes	
:*
dtype0
Ś
9base_model/batch_normalization_19/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_19/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_19/FusedBatchNormV3:2*
T0*D
_class:
86loc:@base_model/batch_normalization_19/moving_variance*
_output_shapes	
:

7base_model/batch_normalization_19/AssignMovingAvg_1/mulMul9base_model/batch_normalization_19/AssignMovingAvg_1/sub_17base_model/batch_normalization_19/AssignMovingAvg_1/sub*
T0*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_19/moving_variance

Gbase_model/batch_normalization_19/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_19/moving_variance7base_model/batch_normalization_19/AssignMovingAvg_1/mul*
dtype0*D
_class:
86loc:@base_model/batch_normalization_19/moving_variance
Ă
Dbase_model/batch_normalization_19/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_19/moving_varianceH^base_model/batch_normalization_19/AssignMovingAvg_1/AssignSubVariableOp*
_output_shapes	
:*D
_class:
86loc:@base_model/batch_normalization_19/moving_variance*
dtype0

base_model/Relu_15Relu2base_model/batch_normalization_19/FusedBatchNormV3*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
Ç
>base_model/conv2d_20/kernel/Initializer/truncated_normal/shapeConst*
dtype0*.
_class$
" loc:@base_model/conv2d_20/kernel*%
valueB"            *
_output_shapes
:
˛
=base_model/conv2d_20/kernel/Initializer/truncated_normal/meanConst*
valueB
 *    *
dtype0*.
_class$
" loc:@base_model/conv2d_20/kernel*
_output_shapes
: 
´
?base_model/conv2d_20/kernel/Initializer/truncated_normal/stddevConst*
dtype0*
valueB
 *¸1<*
_output_shapes
: *.
_class$
" loc:@base_model/conv2d_20/kernel

Hbase_model/conv2d_20/kernel/Initializer/truncated_normal/TruncatedNormalTruncatedNormal>base_model/conv2d_20/kernel/Initializer/truncated_normal/shape*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_20/kernel*
dtype0
ą
<base_model/conv2d_20/kernel/Initializer/truncated_normal/mulMulHbase_model/conv2d_20/kernel/Initializer/truncated_normal/TruncatedNormal?base_model/conv2d_20/kernel/Initializer/truncated_normal/stddev*(
_output_shapes
:*
T0*.
_class$
" loc:@base_model/conv2d_20/kernel

8base_model/conv2d_20/kernel/Initializer/truncated_normalAdd<base_model/conv2d_20/kernel/Initializer/truncated_normal/mul=base_model/conv2d_20/kernel/Initializer/truncated_normal/mean*
T0*(
_output_shapes
:*.
_class$
" loc:@base_model/conv2d_20/kernel
Ě
base_model/conv2d_20/kernelVarHandleOp*.
_class$
" loc:@base_model/conv2d_20/kernel*
_output_shapes
: *,
shared_namebase_model/conv2d_20/kernel*
dtype0*
shape:

<base_model/conv2d_20/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOpbase_model/conv2d_20/kernel*
_output_shapes
: 

"base_model/conv2d_20/kernel/AssignAssignVariableOpbase_model/conv2d_20/kernel8base_model/conv2d_20/kernel/Initializer/truncated_normal*
dtype0

/base_model/conv2d_20/kernel/Read/ReadVariableOpReadVariableOpbase_model/conv2d_20/kernel*(
_output_shapes
:*
dtype0
s
"base_model/conv2d_20/dilation_rateConst*
dtype0*
_output_shapes
:*
valueB"      

*base_model/conv2d_20/Conv2D/ReadVariableOpReadVariableOpbase_model/conv2d_20/kernel*
dtype0*(
_output_shapes
:
Ú
base_model/conv2d_20/Conv2DConv2Dbase_model/Relu_15*base_model/conv2d_20/Conv2D/ReadVariableOp*
strides
*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0*
paddingSAME
Ä
9base_model/batch_normalization_20/gamma/Initializer/zerosConst*
_output_shapes	
:*
dtype0*:
_class0
.,loc:@base_model/batch_normalization_20/gamma*
valueB*    
ă
'base_model/batch_normalization_20/gammaVarHandleOp*
_output_shapes
: *:
_class0
.,loc:@base_model/batch_normalization_20/gamma*8
shared_name)'base_model/batch_normalization_20/gamma*
shape:*
dtype0

Hbase_model/batch_normalization_20/gamma/IsInitialized/VarIsInitializedOpVarIsInitializedOp'base_model/batch_normalization_20/gamma*
_output_shapes
: 
ł
.base_model/batch_normalization_20/gamma/AssignAssignVariableOp'base_model/batch_normalization_20/gamma9base_model/batch_normalization_20/gamma/Initializer/zeros*
dtype0
 
;base_model/batch_normalization_20/gamma/Read/ReadVariableOpReadVariableOp'base_model/batch_normalization_20/gamma*
_output_shapes	
:*
dtype0
Â
8base_model/batch_normalization_20/beta/Initializer/zerosConst*
dtype0*
_output_shapes	
:*9
_class/
-+loc:@base_model/batch_normalization_20/beta*
valueB*    
ŕ
&base_model/batch_normalization_20/betaVarHandleOp*7
shared_name(&base_model/batch_normalization_20/beta*9
_class/
-+loc:@base_model/batch_normalization_20/beta*
_output_shapes
: *
shape:*
dtype0

Gbase_model/batch_normalization_20/beta/IsInitialized/VarIsInitializedOpVarIsInitializedOp&base_model/batch_normalization_20/beta*
_output_shapes
: 
°
-base_model/batch_normalization_20/beta/AssignAssignVariableOp&base_model/batch_normalization_20/beta8base_model/batch_normalization_20/beta/Initializer/zeros*
dtype0

:base_model/batch_normalization_20/beta/Read/ReadVariableOpReadVariableOp&base_model/batch_normalization_20/beta*
dtype0*
_output_shapes	
:
Đ
?base_model/batch_normalization_20/moving_mean/Initializer/zerosConst*
dtype0*@
_class6
42loc:@base_model/batch_normalization_20/moving_mean*
_output_shapes	
:*
valueB*    
ő
-base_model/batch_normalization_20/moving_meanVarHandleOp*
shape:*@
_class6
42loc:@base_model/batch_normalization_20/moving_mean*>
shared_name/-base_model/batch_normalization_20/moving_mean*
_output_shapes
: *
dtype0
Ť
Nbase_model/batch_normalization_20/moving_mean/IsInitialized/VarIsInitializedOpVarIsInitializedOp-base_model/batch_normalization_20/moving_mean*
_output_shapes
: 
Ĺ
4base_model/batch_normalization_20/moving_mean/AssignAssignVariableOp-base_model/batch_normalization_20/moving_mean?base_model/batch_normalization_20/moving_mean/Initializer/zeros*
dtype0
Ź
Abase_model/batch_normalization_20/moving_mean/Read/ReadVariableOpReadVariableOp-base_model/batch_normalization_20/moving_mean*
dtype0*
_output_shapes	
:
×
Bbase_model/batch_normalization_20/moving_variance/Initializer/onesConst*
valueB*  ?*
_output_shapes	
:*
dtype0*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance

1base_model/batch_normalization_20/moving_varianceVarHandleOp*B
shared_name31base_model/batch_normalization_20/moving_variance*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance*
shape:*
dtype0*
_output_shapes
: 
ł
Rbase_model/batch_normalization_20/moving_variance/IsInitialized/VarIsInitializedOpVarIsInitializedOp1base_model/batch_normalization_20/moving_variance*
_output_shapes
: 
Đ
8base_model/batch_normalization_20/moving_variance/AssignAssignVariableOp1base_model/batch_normalization_20/moving_varianceBbase_model/batch_normalization_20/moving_variance/Initializer/ones*
dtype0
´
Ebase_model/batch_normalization_20/moving_variance/Read/ReadVariableOpReadVariableOp1base_model/batch_normalization_20/moving_variance*
dtype0*
_output_shapes	
:

0base_model/batch_normalization_20/ReadVariableOpReadVariableOp'base_model/batch_normalization_20/gamma*
_output_shapes	
:*
dtype0

2base_model/batch_normalization_20/ReadVariableOp_1ReadVariableOp&base_model/batch_normalization_20/beta*
_output_shapes	
:*
dtype0
j
'base_model/batch_normalization_20/ConstConst*
valueB *
dtype0*
_output_shapes
: 
l
)base_model/batch_normalization_20/Const_1Const*
_output_shapes
: *
valueB *
dtype0
Ľ
2base_model/batch_normalization_20/FusedBatchNormV3FusedBatchNormV3base_model/conv2d_20/Conv2D0base_model/batch_normalization_20/ReadVariableOp2base_model/batch_normalization_20/ReadVariableOp_1'base_model/batch_normalization_20/Const)base_model/batch_normalization_20/Const_1*
U0*
epsilon%đ'7*
T0*b
_output_shapesP
N:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙:::::
n
)base_model/batch_normalization_20/Const_2Const*
_output_shapes
: *
dtype0*
valueB
 *fff?
ž
7base_model/batch_normalization_20/AssignMovingAvg/sub/xConst*
valueB
 *  ?*
_output_shapes
: *@
_class6
42loc:@base_model/batch_normalization_20/moving_mean*
dtype0

5base_model/batch_normalization_20/AssignMovingAvg/subSub7base_model/batch_normalization_20/AssignMovingAvg/sub/x)base_model/batch_normalization_20/Const_2*
T0*@
_class6
42loc:@base_model/batch_normalization_20/moving_mean*
_output_shapes
: 
Ť
@base_model/batch_normalization_20/AssignMovingAvg/ReadVariableOpReadVariableOp-base_model/batch_normalization_20/moving_mean*
_output_shapes	
:*
dtype0

7base_model/batch_normalization_20/AssignMovingAvg/sub_1Sub@base_model/batch_normalization_20/AssignMovingAvg/ReadVariableOp4base_model/batch_normalization_20/FusedBatchNormV3:1*
T0*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_20/moving_mean

5base_model/batch_normalization_20/AssignMovingAvg/mulMul7base_model/batch_normalization_20/AssignMovingAvg/sub_15base_model/batch_normalization_20/AssignMovingAvg/sub*@
_class6
42loc:@base_model/batch_normalization_20/moving_mean*
T0*
_output_shapes	
:

Ebase_model/batch_normalization_20/AssignMovingAvg/AssignSubVariableOpAssignSubVariableOp-base_model/batch_normalization_20/moving_mean5base_model/batch_normalization_20/AssignMovingAvg/mul*@
_class6
42loc:@base_model/batch_normalization_20/moving_mean*
dtype0
ˇ
Bbase_model/batch_normalization_20/AssignMovingAvg/ReadVariableOp_1ReadVariableOp-base_model/batch_normalization_20/moving_meanF^base_model/batch_normalization_20/AssignMovingAvg/AssignSubVariableOp*
_output_shapes	
:*@
_class6
42loc:@base_model/batch_normalization_20/moving_mean*
dtype0
Ä
9base_model/batch_normalization_20/AssignMovingAvg_1/sub/xConst*
_output_shapes
: *
dtype0*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance*
valueB
 *  ?

7base_model/batch_normalization_20/AssignMovingAvg_1/subSub9base_model/batch_normalization_20/AssignMovingAvg_1/sub/x)base_model/batch_normalization_20/Const_2*
_output_shapes
: *
T0*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance
ą
Bbase_model/batch_normalization_20/AssignMovingAvg_1/ReadVariableOpReadVariableOp1base_model/batch_normalization_20/moving_variance*
dtype0*
_output_shapes	
:
Ś
9base_model/batch_normalization_20/AssignMovingAvg_1/sub_1SubBbase_model/batch_normalization_20/AssignMovingAvg_1/ReadVariableOp4base_model/batch_normalization_20/FusedBatchNormV3:2*
_output_shapes	
:*
T0*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance

7base_model/batch_normalization_20/AssignMovingAvg_1/mulMul9base_model/batch_normalization_20/AssignMovingAvg_1/sub_17base_model/batch_normalization_20/AssignMovingAvg_1/sub*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance*
_output_shapes	
:*
T0

Gbase_model/batch_normalization_20/AssignMovingAvg_1/AssignSubVariableOpAssignSubVariableOp1base_model/batch_normalization_20/moving_variance7base_model/batch_normalization_20/AssignMovingAvg_1/mul*
dtype0*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance
Ă
Dbase_model/batch_normalization_20/AssignMovingAvg_1/ReadVariableOp_1ReadVariableOp1base_model/batch_normalization_20/moving_varianceH^base_model/batch_normalization_20/AssignMovingAvg_1/AssignSubVariableOp*D
_class:
86loc:@base_model/batch_normalization_20/moving_variance*
_output_shapes	
:*
dtype0
Ž
base_model/add_7AddV22base_model/batch_normalization_20/FusedBatchNormV3base_model/Relu_14*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
y
base_model/Relu_16Relubase_model/add_7*
T0*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙

base_model/block_group4Identitybase_model/Relu_16*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0

base_model/StopGradientStopGradientbase_model/block_group4*B
_output_shapes0
.:,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙*
T0
r
!base_model/Mean/reduction_indicesConst*
_output_shapes
:*
valueB"      *
dtype0

base_model/MeanMeanbase_model/StopGradient!base_model/Mean/reduction_indices*
T0*(
_output_shapes
:˙˙˙˙˙˙˙˙˙
i
base_model/final_avg_poolIdentitybase_model/Mean*(
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0
Ř
Ihead_supervised/linear_layer/dense/kernel/Initializer/random_normal/shapeConst*
dtype0*
_output_shapes
:*
valueB"      *<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel
Ë
Hhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/meanConst*
dtype0*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
valueB
 *    *
_output_shapes
: 
Í
Jhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/stddevConst*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
dtype0*
valueB
 *
×#<*
_output_shapes
: 
°
Xhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/RandomStandardNormalRandomStandardNormalIhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/shape*
dtype0*
_output_shapes
:	*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
T0
Ü
Ghead_supervised/linear_layer/dense/kernel/Initializer/random_normal/mulMulXhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/RandomStandardNormalJhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/stddev*
T0*
_output_shapes
:	*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel
Ĺ
Chead_supervised/linear_layer/dense/kernel/Initializer/random_normalAddGhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/mulHhead_supervised/linear_layer/dense/kernel/Initializer/random_normal/mean*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
_output_shapes
:	*
T0
í
)head_supervised/linear_layer/dense/kernelVarHandleOp*
_output_shapes
: *:
shared_name+)head_supervised/linear_layer/dense/kernel*
shape:	*<
_class2
0.loc:@head_supervised/linear_layer/dense/kernel*
dtype0
Ł
Jhead_supervised/linear_layer/dense/kernel/IsInitialized/VarIsInitializedOpVarIsInitializedOp)head_supervised/linear_layer/dense/kernel*
_output_shapes
: 
Á
0head_supervised/linear_layer/dense/kernel/AssignAssignVariableOp)head_supervised/linear_layer/dense/kernelChead_supervised/linear_layer/dense/kernel/Initializer/random_normal*
dtype0
¨
=head_supervised/linear_layer/dense/kernel/Read/ReadVariableOpReadVariableOp)head_supervised/linear_layer/dense/kernel*
_output_shapes
:	*
dtype0
Â
9head_supervised/linear_layer/dense/bias/Initializer/zerosConst*
_output_shapes
:*
dtype0*
valueB*    *:
_class0
.,loc:@head_supervised/linear_layer/dense/bias
â
'head_supervised/linear_layer/dense/biasVarHandleOp*8
shared_name)'head_supervised/linear_layer/dense/bias*:
_class0
.,loc:@head_supervised/linear_layer/dense/bias*
dtype0*
_output_shapes
: *
shape:

Hhead_supervised/linear_layer/dense/bias/IsInitialized/VarIsInitializedOpVarIsInitializedOp'head_supervised/linear_layer/dense/bias*
_output_shapes
: 
ł
.head_supervised/linear_layer/dense/bias/AssignAssignVariableOp'head_supervised/linear_layer/dense/bias9head_supervised/linear_layer/dense/bias/Initializer/zeros*
dtype0

;head_supervised/linear_layer/dense/bias/Read/ReadVariableOpReadVariableOp'head_supervised/linear_layer/dense/bias*
_output_shapes
:*
dtype0
Ł
8head_supervised/linear_layer/dense/MatMul/ReadVariableOpReadVariableOp)head_supervised/linear_layer/dense/kernel*
dtype0*
_output_shapes
:	
ş
)head_supervised/linear_layer/dense/MatMulMatMulbase_model/final_avg_pool8head_supervised/linear_layer/dense/MatMul/ReadVariableOp*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0

9head_supervised/linear_layer/dense/BiasAdd/ReadVariableOpReadVariableOp'head_supervised/linear_layer/dense/bias*
dtype0*
_output_shapes
:
Í
*head_supervised/linear_layer/dense/BiasAddBiasAdd)head_supervised/linear_layer/dense/MatMul9head_supervised/linear_layer/dense/BiasAdd/ReadVariableOp*
T0*'
_output_shapes
:˙˙˙˙˙˙˙˙˙

-head_supervised/linear_layer/linear_layer_outIdentity*head_supervised/linear_layer/dense/BiasAdd*'
_output_shapes
:˙˙˙˙˙˙˙˙˙*
T0""i
trainable_variables÷hôh
Ş
base_model/conv2d/kernel:0base_model/conv2d/kernel/Assign.base_model/conv2d/kernel/Read/ReadVariableOp:0(27base_model/conv2d/kernel/Initializer/truncated_normal:08
Î
&base_model/batch_normalization/gamma:0+base_model/batch_normalization/gamma/Assign:base_model/batch_normalization/gamma/Read/ReadVariableOp:0(27base_model/batch_normalization/gamma/Initializer/ones:08
Ë
%base_model/batch_normalization/beta:0*base_model/batch_normalization/beta/Assign9base_model/batch_normalization/beta/Read/ReadVariableOp:0(27base_model/batch_normalization/beta/Initializer/zeros:08
˛
base_model/conv2d_1/kernel:0!base_model/conv2d_1/kernel/Assign0base_model/conv2d_1/kernel/Read/ReadVariableOp:0(29base_model/conv2d_1/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_1/gamma:0-base_model/batch_normalization_1/gamma/Assign<base_model/batch_normalization_1/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_1/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_1/beta:0,base_model/batch_normalization_1/beta/Assign;base_model/batch_normalization_1/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_1/beta/Initializer/zeros:08
˛
base_model/conv2d_2/kernel:0!base_model/conv2d_2/kernel/Assign0base_model/conv2d_2/kernel/Read/ReadVariableOp:0(29base_model/conv2d_2/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_2/gamma:0-base_model/batch_normalization_2/gamma/Assign<base_model/batch_normalization_2/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_2/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_2/beta:0,base_model/batch_normalization_2/beta/Assign;base_model/batch_normalization_2/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_2/beta/Initializer/zeros:08
˛
base_model/conv2d_3/kernel:0!base_model/conv2d_3/kernel/Assign0base_model/conv2d_3/kernel/Read/ReadVariableOp:0(29base_model/conv2d_3/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_3/gamma:0-base_model/batch_normalization_3/gamma/Assign<base_model/batch_normalization_3/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_3/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_3/beta:0,base_model/batch_normalization_3/beta/Assign;base_model/batch_normalization_3/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_3/beta/Initializer/zeros:08
˛
base_model/conv2d_4/kernel:0!base_model/conv2d_4/kernel/Assign0base_model/conv2d_4/kernel/Read/ReadVariableOp:0(29base_model/conv2d_4/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_4/gamma:0-base_model/batch_normalization_4/gamma/Assign<base_model/batch_normalization_4/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_4/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_4/beta:0,base_model/batch_normalization_4/beta/Assign;base_model/batch_normalization_4/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_4/beta/Initializer/zeros:08
˛
base_model/conv2d_5/kernel:0!base_model/conv2d_5/kernel/Assign0base_model/conv2d_5/kernel/Read/ReadVariableOp:0(29base_model/conv2d_5/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_5/gamma:0-base_model/batch_normalization_5/gamma/Assign<base_model/batch_normalization_5/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_5/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_5/beta:0,base_model/batch_normalization_5/beta/Assign;base_model/batch_normalization_5/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_5/beta/Initializer/zeros:08
˛
base_model/conv2d_6/kernel:0!base_model/conv2d_6/kernel/Assign0base_model/conv2d_6/kernel/Read/ReadVariableOp:0(29base_model/conv2d_6/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_6/gamma:0-base_model/batch_normalization_6/gamma/Assign<base_model/batch_normalization_6/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_6/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_6/beta:0,base_model/batch_normalization_6/beta/Assign;base_model/batch_normalization_6/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_6/beta/Initializer/zeros:08
˛
base_model/conv2d_7/kernel:0!base_model/conv2d_7/kernel/Assign0base_model/conv2d_7/kernel/Read/ReadVariableOp:0(29base_model/conv2d_7/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_7/gamma:0-base_model/batch_normalization_7/gamma/Assign<base_model/batch_normalization_7/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_7/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_7/beta:0,base_model/batch_normalization_7/beta/Assign;base_model/batch_normalization_7/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_7/beta/Initializer/zeros:08
˛
base_model/conv2d_8/kernel:0!base_model/conv2d_8/kernel/Assign0base_model/conv2d_8/kernel/Read/ReadVariableOp:0(29base_model/conv2d_8/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_8/gamma:0-base_model/batch_normalization_8/gamma/Assign<base_model/batch_normalization_8/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_8/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_8/beta:0,base_model/batch_normalization_8/beta/Assign;base_model/batch_normalization_8/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_8/beta/Initializer/zeros:08
˛
base_model/conv2d_9/kernel:0!base_model/conv2d_9/kernel/Assign0base_model/conv2d_9/kernel/Read/ReadVariableOp:0(29base_model/conv2d_9/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_9/gamma:0-base_model/batch_normalization_9/gamma/Assign<base_model/batch_normalization_9/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_9/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_9/beta:0,base_model/batch_normalization_9/beta/Assign;base_model/batch_normalization_9/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_9/beta/Initializer/zeros:08
ś
base_model/conv2d_10/kernel:0"base_model/conv2d_10/kernel/Assign1base_model/conv2d_10/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_10/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_10/gamma:0.base_model/batch_normalization_10/gamma/Assign=base_model/batch_normalization_10/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_10/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_10/beta:0-base_model/batch_normalization_10/beta/Assign<base_model/batch_normalization_10/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_10/beta/Initializer/zeros:08
ś
base_model/conv2d_11/kernel:0"base_model/conv2d_11/kernel/Assign1base_model/conv2d_11/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_11/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_11/gamma:0.base_model/batch_normalization_11/gamma/Assign=base_model/batch_normalization_11/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_11/gamma/Initializer/ones:08
×
(base_model/batch_normalization_11/beta:0-base_model/batch_normalization_11/beta/Assign<base_model/batch_normalization_11/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_11/beta/Initializer/zeros:08
ś
base_model/conv2d_12/kernel:0"base_model/conv2d_12/kernel/Assign1base_model/conv2d_12/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_12/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_12/gamma:0.base_model/batch_normalization_12/gamma/Assign=base_model/batch_normalization_12/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_12/gamma/Initializer/ones:08
×
(base_model/batch_normalization_12/beta:0-base_model/batch_normalization_12/beta/Assign<base_model/batch_normalization_12/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_12/beta/Initializer/zeros:08
ś
base_model/conv2d_13/kernel:0"base_model/conv2d_13/kernel/Assign1base_model/conv2d_13/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_13/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_13/gamma:0.base_model/batch_normalization_13/gamma/Assign=base_model/batch_normalization_13/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_13/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_13/beta:0-base_model/batch_normalization_13/beta/Assign<base_model/batch_normalization_13/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_13/beta/Initializer/zeros:08
ś
base_model/conv2d_14/kernel:0"base_model/conv2d_14/kernel/Assign1base_model/conv2d_14/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_14/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_14/gamma:0.base_model/batch_normalization_14/gamma/Assign=base_model/batch_normalization_14/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_14/gamma/Initializer/ones:08
×
(base_model/batch_normalization_14/beta:0-base_model/batch_normalization_14/beta/Assign<base_model/batch_normalization_14/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_14/beta/Initializer/zeros:08
ś
base_model/conv2d_15/kernel:0"base_model/conv2d_15/kernel/Assign1base_model/conv2d_15/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_15/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_15/gamma:0.base_model/batch_normalization_15/gamma/Assign=base_model/batch_normalization_15/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_15/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_15/beta:0-base_model/batch_normalization_15/beta/Assign<base_model/batch_normalization_15/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_15/beta/Initializer/zeros:08
ś
base_model/conv2d_16/kernel:0"base_model/conv2d_16/kernel/Assign1base_model/conv2d_16/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_16/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_16/gamma:0.base_model/batch_normalization_16/gamma/Assign=base_model/batch_normalization_16/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_16/gamma/Initializer/ones:08
×
(base_model/batch_normalization_16/beta:0-base_model/batch_normalization_16/beta/Assign<base_model/batch_normalization_16/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_16/beta/Initializer/zeros:08
ś
base_model/conv2d_17/kernel:0"base_model/conv2d_17/kernel/Assign1base_model/conv2d_17/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_17/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_17/gamma:0.base_model/batch_normalization_17/gamma/Assign=base_model/batch_normalization_17/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_17/gamma/Initializer/ones:08
×
(base_model/batch_normalization_17/beta:0-base_model/batch_normalization_17/beta/Assign<base_model/batch_normalization_17/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_17/beta/Initializer/zeros:08
ś
base_model/conv2d_18/kernel:0"base_model/conv2d_18/kernel/Assign1base_model/conv2d_18/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_18/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_18/gamma:0.base_model/batch_normalization_18/gamma/Assign=base_model/batch_normalization_18/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_18/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_18/beta:0-base_model/batch_normalization_18/beta/Assign<base_model/batch_normalization_18/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_18/beta/Initializer/zeros:08
ś
base_model/conv2d_19/kernel:0"base_model/conv2d_19/kernel/Assign1base_model/conv2d_19/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_19/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_19/gamma:0.base_model/batch_normalization_19/gamma/Assign=base_model/batch_normalization_19/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_19/gamma/Initializer/ones:08
×
(base_model/batch_normalization_19/beta:0-base_model/batch_normalization_19/beta/Assign<base_model/batch_normalization_19/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_19/beta/Initializer/zeros:08
ś
base_model/conv2d_20/kernel:0"base_model/conv2d_20/kernel/Assign1base_model/conv2d_20/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_20/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_20/gamma:0.base_model/batch_normalization_20/gamma/Assign=base_model/batch_normalization_20/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_20/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_20/beta:0-base_model/batch_normalization_20/beta/Assign<base_model/batch_normalization_20/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_20/beta/Initializer/zeros:08
ë
+head_supervised/linear_layer/dense/kernel:00head_supervised/linear_layer/dense/kernel/Assign?head_supervised/linear_layer/dense/kernel/Read/ReadVariableOp:0(2Ehead_supervised/linear_layer/dense/kernel/Initializer/random_normal:08
Ű
)head_supervised/linear_layer/dense/bias:0.head_supervised/linear_layer/dense/bias/Assign=head_supervised/linear_layer/dense/bias/Read/ReadVariableOp:0(2;head_supervised/linear_layer/dense/bias/Initializer/zeros:08"ź
	variablesź˙ť
Ş
base_model/conv2d/kernel:0base_model/conv2d/kernel/Assign.base_model/conv2d/kernel/Read/ReadVariableOp:0(27base_model/conv2d/kernel/Initializer/truncated_normal:08
Î
&base_model/batch_normalization/gamma:0+base_model/batch_normalization/gamma/Assign:base_model/batch_normalization/gamma/Read/ReadVariableOp:0(27base_model/batch_normalization/gamma/Initializer/ones:08
Ë
%base_model/batch_normalization/beta:0*base_model/batch_normalization/beta/Assign9base_model/batch_normalization/beta/Read/ReadVariableOp:0(27base_model/batch_normalization/beta/Initializer/zeros:08
é
,base_model/batch_normalization/moving_mean:01base_model/batch_normalization/moving_mean/Assign@base_model/batch_normalization/moving_mean/Read/ReadVariableOp:0(2>base_model/batch_normalization/moving_mean/Initializer/zeros:0@H
ř
0base_model/batch_normalization/moving_variance:05base_model/batch_normalization/moving_variance/AssignDbase_model/batch_normalization/moving_variance/Read/ReadVariableOp:0(2Abase_model/batch_normalization/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_1/kernel:0!base_model/conv2d_1/kernel/Assign0base_model/conv2d_1/kernel/Read/ReadVariableOp:0(29base_model/conv2d_1/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_1/gamma:0-base_model/batch_normalization_1/gamma/Assign<base_model/batch_normalization_1/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_1/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_1/beta:0,base_model/batch_normalization_1/beta/Assign;base_model/batch_normalization_1/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_1/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_1/moving_mean:03base_model/batch_normalization_1/moving_mean/AssignBbase_model/batch_normalization_1/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_1/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_1/moving_variance:07base_model/batch_normalization_1/moving_variance/AssignFbase_model/batch_normalization_1/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_1/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_2/kernel:0!base_model/conv2d_2/kernel/Assign0base_model/conv2d_2/kernel/Read/ReadVariableOp:0(29base_model/conv2d_2/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_2/gamma:0-base_model/batch_normalization_2/gamma/Assign<base_model/batch_normalization_2/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_2/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_2/beta:0,base_model/batch_normalization_2/beta/Assign;base_model/batch_normalization_2/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_2/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_2/moving_mean:03base_model/batch_normalization_2/moving_mean/AssignBbase_model/batch_normalization_2/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_2/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_2/moving_variance:07base_model/batch_normalization_2/moving_variance/AssignFbase_model/batch_normalization_2/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_2/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_3/kernel:0!base_model/conv2d_3/kernel/Assign0base_model/conv2d_3/kernel/Read/ReadVariableOp:0(29base_model/conv2d_3/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_3/gamma:0-base_model/batch_normalization_3/gamma/Assign<base_model/batch_normalization_3/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_3/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_3/beta:0,base_model/batch_normalization_3/beta/Assign;base_model/batch_normalization_3/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_3/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_3/moving_mean:03base_model/batch_normalization_3/moving_mean/AssignBbase_model/batch_normalization_3/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_3/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_3/moving_variance:07base_model/batch_normalization_3/moving_variance/AssignFbase_model/batch_normalization_3/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_3/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_4/kernel:0!base_model/conv2d_4/kernel/Assign0base_model/conv2d_4/kernel/Read/ReadVariableOp:0(29base_model/conv2d_4/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_4/gamma:0-base_model/batch_normalization_4/gamma/Assign<base_model/batch_normalization_4/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_4/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_4/beta:0,base_model/batch_normalization_4/beta/Assign;base_model/batch_normalization_4/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_4/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_4/moving_mean:03base_model/batch_normalization_4/moving_mean/AssignBbase_model/batch_normalization_4/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_4/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_4/moving_variance:07base_model/batch_normalization_4/moving_variance/AssignFbase_model/batch_normalization_4/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_4/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_5/kernel:0!base_model/conv2d_5/kernel/Assign0base_model/conv2d_5/kernel/Read/ReadVariableOp:0(29base_model/conv2d_5/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_5/gamma:0-base_model/batch_normalization_5/gamma/Assign<base_model/batch_normalization_5/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_5/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_5/beta:0,base_model/batch_normalization_5/beta/Assign;base_model/batch_normalization_5/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_5/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_5/moving_mean:03base_model/batch_normalization_5/moving_mean/AssignBbase_model/batch_normalization_5/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_5/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_5/moving_variance:07base_model/batch_normalization_5/moving_variance/AssignFbase_model/batch_normalization_5/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_5/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_6/kernel:0!base_model/conv2d_6/kernel/Assign0base_model/conv2d_6/kernel/Read/ReadVariableOp:0(29base_model/conv2d_6/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_6/gamma:0-base_model/batch_normalization_6/gamma/Assign<base_model/batch_normalization_6/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_6/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_6/beta:0,base_model/batch_normalization_6/beta/Assign;base_model/batch_normalization_6/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_6/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_6/moving_mean:03base_model/batch_normalization_6/moving_mean/AssignBbase_model/batch_normalization_6/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_6/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_6/moving_variance:07base_model/batch_normalization_6/moving_variance/AssignFbase_model/batch_normalization_6/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_6/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_7/kernel:0!base_model/conv2d_7/kernel/Assign0base_model/conv2d_7/kernel/Read/ReadVariableOp:0(29base_model/conv2d_7/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_7/gamma:0-base_model/batch_normalization_7/gamma/Assign<base_model/batch_normalization_7/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_7/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_7/beta:0,base_model/batch_normalization_7/beta/Assign;base_model/batch_normalization_7/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_7/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_7/moving_mean:03base_model/batch_normalization_7/moving_mean/AssignBbase_model/batch_normalization_7/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_7/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_7/moving_variance:07base_model/batch_normalization_7/moving_variance/AssignFbase_model/batch_normalization_7/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_7/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_8/kernel:0!base_model/conv2d_8/kernel/Assign0base_model/conv2d_8/kernel/Read/ReadVariableOp:0(29base_model/conv2d_8/kernel/Initializer/truncated_normal:08
×
(base_model/batch_normalization_8/gamma:0-base_model/batch_normalization_8/gamma/Assign<base_model/batch_normalization_8/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_8/gamma/Initializer/zeros:08
Ó
'base_model/batch_normalization_8/beta:0,base_model/batch_normalization_8/beta/Assign;base_model/batch_normalization_8/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_8/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_8/moving_mean:03base_model/batch_normalization_8/moving_mean/AssignBbase_model/batch_normalization_8/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_8/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_8/moving_variance:07base_model/batch_normalization_8/moving_variance/AssignFbase_model/batch_normalization_8/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_8/moving_variance/Initializer/ones:0@H
˛
base_model/conv2d_9/kernel:0!base_model/conv2d_9/kernel/Assign0base_model/conv2d_9/kernel/Read/ReadVariableOp:0(29base_model/conv2d_9/kernel/Initializer/truncated_normal:08
Ö
(base_model/batch_normalization_9/gamma:0-base_model/batch_normalization_9/gamma/Assign<base_model/batch_normalization_9/gamma/Read/ReadVariableOp:0(29base_model/batch_normalization_9/gamma/Initializer/ones:08
Ó
'base_model/batch_normalization_9/beta:0,base_model/batch_normalization_9/beta/Assign;base_model/batch_normalization_9/beta/Read/ReadVariableOp:0(29base_model/batch_normalization_9/beta/Initializer/zeros:08
ń
.base_model/batch_normalization_9/moving_mean:03base_model/batch_normalization_9/moving_mean/AssignBbase_model/batch_normalization_9/moving_mean/Read/ReadVariableOp:0(2@base_model/batch_normalization_9/moving_mean/Initializer/zeros:0@H

2base_model/batch_normalization_9/moving_variance:07base_model/batch_normalization_9/moving_variance/AssignFbase_model/batch_normalization_9/moving_variance/Read/ReadVariableOp:0(2Cbase_model/batch_normalization_9/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_10/kernel:0"base_model/conv2d_10/kernel/Assign1base_model/conv2d_10/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_10/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_10/gamma:0.base_model/batch_normalization_10/gamma/Assign=base_model/batch_normalization_10/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_10/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_10/beta:0-base_model/batch_normalization_10/beta/Assign<base_model/batch_normalization_10/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_10/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_10/moving_mean:04base_model/batch_normalization_10/moving_mean/AssignCbase_model/batch_normalization_10/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_10/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_10/moving_variance:08base_model/batch_normalization_10/moving_variance/AssignGbase_model/batch_normalization_10/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_10/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_11/kernel:0"base_model/conv2d_11/kernel/Assign1base_model/conv2d_11/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_11/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_11/gamma:0.base_model/batch_normalization_11/gamma/Assign=base_model/batch_normalization_11/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_11/gamma/Initializer/ones:08
×
(base_model/batch_normalization_11/beta:0-base_model/batch_normalization_11/beta/Assign<base_model/batch_normalization_11/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_11/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_11/moving_mean:04base_model/batch_normalization_11/moving_mean/AssignCbase_model/batch_normalization_11/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_11/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_11/moving_variance:08base_model/batch_normalization_11/moving_variance/AssignGbase_model/batch_normalization_11/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_11/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_12/kernel:0"base_model/conv2d_12/kernel/Assign1base_model/conv2d_12/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_12/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_12/gamma:0.base_model/batch_normalization_12/gamma/Assign=base_model/batch_normalization_12/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_12/gamma/Initializer/ones:08
×
(base_model/batch_normalization_12/beta:0-base_model/batch_normalization_12/beta/Assign<base_model/batch_normalization_12/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_12/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_12/moving_mean:04base_model/batch_normalization_12/moving_mean/AssignCbase_model/batch_normalization_12/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_12/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_12/moving_variance:08base_model/batch_normalization_12/moving_variance/AssignGbase_model/batch_normalization_12/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_12/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_13/kernel:0"base_model/conv2d_13/kernel/Assign1base_model/conv2d_13/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_13/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_13/gamma:0.base_model/batch_normalization_13/gamma/Assign=base_model/batch_normalization_13/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_13/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_13/beta:0-base_model/batch_normalization_13/beta/Assign<base_model/batch_normalization_13/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_13/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_13/moving_mean:04base_model/batch_normalization_13/moving_mean/AssignCbase_model/batch_normalization_13/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_13/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_13/moving_variance:08base_model/batch_normalization_13/moving_variance/AssignGbase_model/batch_normalization_13/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_13/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_14/kernel:0"base_model/conv2d_14/kernel/Assign1base_model/conv2d_14/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_14/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_14/gamma:0.base_model/batch_normalization_14/gamma/Assign=base_model/batch_normalization_14/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_14/gamma/Initializer/ones:08
×
(base_model/batch_normalization_14/beta:0-base_model/batch_normalization_14/beta/Assign<base_model/batch_normalization_14/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_14/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_14/moving_mean:04base_model/batch_normalization_14/moving_mean/AssignCbase_model/batch_normalization_14/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_14/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_14/moving_variance:08base_model/batch_normalization_14/moving_variance/AssignGbase_model/batch_normalization_14/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_14/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_15/kernel:0"base_model/conv2d_15/kernel/Assign1base_model/conv2d_15/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_15/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_15/gamma:0.base_model/batch_normalization_15/gamma/Assign=base_model/batch_normalization_15/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_15/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_15/beta:0-base_model/batch_normalization_15/beta/Assign<base_model/batch_normalization_15/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_15/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_15/moving_mean:04base_model/batch_normalization_15/moving_mean/AssignCbase_model/batch_normalization_15/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_15/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_15/moving_variance:08base_model/batch_normalization_15/moving_variance/AssignGbase_model/batch_normalization_15/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_15/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_16/kernel:0"base_model/conv2d_16/kernel/Assign1base_model/conv2d_16/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_16/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_16/gamma:0.base_model/batch_normalization_16/gamma/Assign=base_model/batch_normalization_16/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_16/gamma/Initializer/ones:08
×
(base_model/batch_normalization_16/beta:0-base_model/batch_normalization_16/beta/Assign<base_model/batch_normalization_16/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_16/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_16/moving_mean:04base_model/batch_normalization_16/moving_mean/AssignCbase_model/batch_normalization_16/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_16/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_16/moving_variance:08base_model/batch_normalization_16/moving_variance/AssignGbase_model/batch_normalization_16/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_16/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_17/kernel:0"base_model/conv2d_17/kernel/Assign1base_model/conv2d_17/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_17/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_17/gamma:0.base_model/batch_normalization_17/gamma/Assign=base_model/batch_normalization_17/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_17/gamma/Initializer/ones:08
×
(base_model/batch_normalization_17/beta:0-base_model/batch_normalization_17/beta/Assign<base_model/batch_normalization_17/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_17/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_17/moving_mean:04base_model/batch_normalization_17/moving_mean/AssignCbase_model/batch_normalization_17/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_17/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_17/moving_variance:08base_model/batch_normalization_17/moving_variance/AssignGbase_model/batch_normalization_17/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_17/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_18/kernel:0"base_model/conv2d_18/kernel/Assign1base_model/conv2d_18/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_18/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_18/gamma:0.base_model/batch_normalization_18/gamma/Assign=base_model/batch_normalization_18/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_18/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_18/beta:0-base_model/batch_normalization_18/beta/Assign<base_model/batch_normalization_18/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_18/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_18/moving_mean:04base_model/batch_normalization_18/moving_mean/AssignCbase_model/batch_normalization_18/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_18/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_18/moving_variance:08base_model/batch_normalization_18/moving_variance/AssignGbase_model/batch_normalization_18/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_18/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_19/kernel:0"base_model/conv2d_19/kernel/Assign1base_model/conv2d_19/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_19/kernel/Initializer/truncated_normal:08
Ú
)base_model/batch_normalization_19/gamma:0.base_model/batch_normalization_19/gamma/Assign=base_model/batch_normalization_19/gamma/Read/ReadVariableOp:0(2:base_model/batch_normalization_19/gamma/Initializer/ones:08
×
(base_model/batch_normalization_19/beta:0-base_model/batch_normalization_19/beta/Assign<base_model/batch_normalization_19/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_19/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_19/moving_mean:04base_model/batch_normalization_19/moving_mean/AssignCbase_model/batch_normalization_19/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_19/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_19/moving_variance:08base_model/batch_normalization_19/moving_variance/AssignGbase_model/batch_normalization_19/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_19/moving_variance/Initializer/ones:0@H
ś
base_model/conv2d_20/kernel:0"base_model/conv2d_20/kernel/Assign1base_model/conv2d_20/kernel/Read/ReadVariableOp:0(2:base_model/conv2d_20/kernel/Initializer/truncated_normal:08
Ű
)base_model/batch_normalization_20/gamma:0.base_model/batch_normalization_20/gamma/Assign=base_model/batch_normalization_20/gamma/Read/ReadVariableOp:0(2;base_model/batch_normalization_20/gamma/Initializer/zeros:08
×
(base_model/batch_normalization_20/beta:0-base_model/batch_normalization_20/beta/Assign<base_model/batch_normalization_20/beta/Read/ReadVariableOp:0(2:base_model/batch_normalization_20/beta/Initializer/zeros:08
ő
/base_model/batch_normalization_20/moving_mean:04base_model/batch_normalization_20/moving_mean/AssignCbase_model/batch_normalization_20/moving_mean/Read/ReadVariableOp:0(2Abase_model/batch_normalization_20/moving_mean/Initializer/zeros:0@H

3base_model/batch_normalization_20/moving_variance:08base_model/batch_normalization_20/moving_variance/AssignGbase_model/batch_normalization_20/moving_variance/Read/ReadVariableOp:0(2Dbase_model/batch_normalization_20/moving_variance/Initializer/ones:0@H
ë
+head_supervised/linear_layer/dense/kernel:00head_supervised/linear_layer/dense/kernel/Assign?head_supervised/linear_layer/dense/kernel/Read/ReadVariableOp:0(2Ehead_supervised/linear_layer/dense/kernel/Initializer/random_normal:08
Ű
)head_supervised/linear_layer/dense/bias:0.head_supervised/linear_layer/dense/bias/Assign=head_supervised/linear_layer/dense/bias/Read/ReadVariableOp:0(2;head_supervised/linear_layer/dense/bias/Initializer/zeros:08"Ę

update_opsť
¸
Bbase_model/batch_normalization/AssignMovingAvg/AssignSubVariableOp
Dbase_model/batch_normalization/AssignMovingAvg_1/AssignSubVariableOp
Dbase_model/batch_normalization_1/AssignMovingAvg/AssignSubVariableOp
Fbase_model/batch_normalization_1/AssignMovingAvg_1/AssignSubVariableOp
Dbase_model/batch_normalization_2/AssignMovingAvg/AssignSubVariableOp
Fbase_model/batch_normalization_2/AssignMovingAvg_1/AssignSubVariableOp
Dbase_model/batch_normalization_3/AssignMovingAvg/AssignSubVariableOp
Fbase_model/batch_normalization_3/AssignMovingAvg_1/AssignSubVariableOp
Dbase_model/batch_normalization_4/AssignMovingAvg/AssignSubVariableOp
Fbase_model/batch_normalization_4/AssignMovingAvg_1/AssignSubVariableOp
Dbase_model/batch_normalization_5/AssignMovingAvg/AssignSubVariableOp
Fbase_model/batch_normalization_5/AssignMovingAvg_1/AssignSubVariableOp
Dbase_model/batch_normalization_6/AssignMovingAvg/AssignSubVariableOp
Fbase_model/batch_normalization_6/AssignMovingAvg_1/AssignSubVariableOp
Dbase_model/batch_normalization_7/AssignMovingAvg/AssignSubVariableOp
Fbase_model/batch_normalization_7/AssignMovingAvg_1/AssignSubVariableOp
Dbase_model/batch_normalization_8/AssignMovingAvg/AssignSubVariableOp
Fbase_model/batch_normalization_8/AssignMovingAvg_1/AssignSubVariableOp
Dbase_model/batch_normalization_9/AssignMovingAvg/AssignSubVariableOp
Fbase_model/batch_normalization_9/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_10/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_10/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_11/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_11/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_12/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_12/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_13/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_13/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_14/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_14/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_15/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_15/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_16/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_16/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_17/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_17/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_18/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_18/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_19/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_19/AssignMovingAvg_1/AssignSubVariableOp
Ebase_model/batch_normalization_20/AssignMovingAvg/AssignSubVariableOp
Gbase_model/batch_normalization_20/AssignMovingAvg_1/AssignSubVariableOp*ć
defaultÚ
H
images>
Placeholder:0+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙>
default3
base_model/final_avg_pool:0˙˙˙˙˙˙˙˙˙b
initial_max_poolN
base_model/initial_max_pool:0+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@Z
block_group1J
base_model/block_group1:0+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@E
final_avg_pool3
base_model/final_avg_pool:0˙˙˙˙˙˙˙˙˙[
block_group3K
base_model/block_group3:0,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙[
block_group2K
base_model/block_group2:0,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙T

logits_supF
/head_supervised/linear_layer/linear_layer_out:0˙˙˙˙˙˙˙˙˙Z
initial_convJ
base_model/initial_conv:0+˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙@[
block_group4K
base_model/block_group4:0,˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙˙